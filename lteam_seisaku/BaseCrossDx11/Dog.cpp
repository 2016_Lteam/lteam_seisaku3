/*!
@file Dog.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class Dog : public GameObject;
	//	用途: 犬
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Dog::Dog(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),	//大きさ
		m_Rot(Rotation),//回転
		m_Pos(Position)	//位置
	{}

	//初期化
	void Dog::OnCreate() {
		//初期位置などの設定
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(m_Scale);
		Ptr->SetRotation(m_Rot);
		Ptr->SetPosition(m_Pos);


		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.7f, 0.7f, 0.7f),
			Vector3(0.0f, 0, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
		);

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();
		//反発係数は0.5（半分）
		PtrRedid->SetReflection(0.5f);
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();

		//最下地点
		PtrGravity->SetBaseY(0.0f);
		//衝突判定をつける
		//auto PtrCol = AddComponent<CollisionSphere>();
		//横部分のみ反発
		//PtrCol->SetIsHitAction(IsHitAction::AutoOnObjectRepel);

		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"DOG_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTBoneModelDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"DOG_MESH");
		PtrDraw->AddAnimation(L"Default", 0, 21, true, 40.0f);
		PtrDraw->AddAnimation(L"Walk", 30, 40, true, 40.0f);
		PtrDraw->ChangeCurrentAnimation(L"Default");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);

		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		//透明処理
		SetAlphaActive(true);
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Dog> >(GetThis<Dog>());
		//最初のステートをD_DefaultStateに設定
		m_StateMachine->ChangeState(D_DefaultState::Instance());

		SetDrawLayer(1);

		wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Dog_naku.wav";
		App::GetApp()->RegisterWav(L"DOG_NAKU", strMusic);


	}


	//更新
	void Dog::OnUpdate() {
		auto PtrDraw = GetComponent<PNTBoneModelDraw>();
		float ElapsedTime = App::GetApp()->GetElapsedTime();

		PtrDraw->UpdateAnimation(ElapsedTime);


		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();
	}

	void Dog::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		//スパークの放出
		auto PtrSpark = GetStage()->GetSharedGameObject<MultiSpark>(L"MultiSpark", false);
		if (PtrSpark) {
			PtrSpark->InsertSpark(GetComponent<Transform>()->GetPosition());
		}
		if (GetStateMachine()->GetCurrentState() == SearchState::Instance()) {
			GetStateMachine()->ChangeState(D_DefaultState::Instance());
		}
	}

	//ターンの最終更新時
	void Dog::OnLastUpdate() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");

		auto m_Pos = GetComponent<Transform>()->GetPosition();
		if (Motion_Change) {
			if (PlayerPtr->GetWOD()) {
				if (m_Pos == m_Dog_BeforePos) {
					if (!walkanim) {
						auto PtrDraw = GetComponent<PNTBoneModelDraw>();
						PtrDraw->ChangeCurrentAnimation(L"Default");
						Motion_Change = false;
					}
				}
				m_Dog_BeforePos = m_Pos;
			}
		}
	}

	//playerの前座標に犬が移動する
	void Dog::SetPlayer_BeforePos(Vector3 Player_Pos) {
		//auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		//auto P_Move_Direction = PlayerPtr->m_Dog_MoveAngle;
		//auto PTrans = PlayerPtr->GetComponent<Transform>();
		//auto P_Pos = PTrans->GetPosition();
		//auto Move_Speed = PlayerPtr->Move_Speed;
		//auto D_Trans = GetComponent<Transform>();
		//auto D_Pos = D_Trans->GetPosition();

		////D_Trans->SetPosition(Player_Pos);

		//switch (P_Move_Direction)
		//{
		//case 0:
		//	m_Move_Direction = 0;
		//	break;
		//case 1:
		//	m_Move_Direction = 1;
		//	break;
		//case 2:
		//	m_Move_Direction = 2;
		//	break;
		//case 3:
		//	m_Move_Direction = 3;
		//	break;
		//}
		//m_Dog_Walk = true;
		//auto PtrDraw = GetComponent<PNTBoneModelDraw>();
		//PtrDraw->ChangeCurrentAnimation(L"Walk");
	}

	//playerの前座標に犬が移動する
	void Dog::SetPlayer_BeforePosVec(Vector3 Player_Pos) {
		int Move_Angle = 0;
		auto p_Pos = Player_Pos;
		if (CurrentDefault) {
			if (m_PlayerPosHistory.size() > 0) {
				auto d_Pos = m_PlayerPosHistory.back();
				if (round(d_Pos.x) < round(p_Pos.x)) {
					Move_Angle = 3;
				}
				if (round(d_Pos.x) > round(p_Pos.x)) {
					Move_Angle = 2;
				}
				if (round(d_Pos.z) > round(p_Pos.z)) {
					Move_Angle = 1;
				}
				if (round(d_Pos.z) < round(p_Pos.z)) {
					Move_Angle = 0;
				}
			}
			else {
				auto D_Trans = GetComponent<Transform>();
				auto d_Pos = D_Trans->GetPosition();
				if (round(d_Pos.x) < round(p_Pos.x)) {
					Move_Angle = 3;
				}
				if (round(d_Pos.x) > round(p_Pos.x)) {
					Move_Angle = 2;
				}
				if (round(d_Pos.z) > round(p_Pos.z)) {
					Move_Angle = 1;
				}
				if (round(d_Pos.z) < round(p_Pos.z)) {
					Move_Angle = 0;
				}
			}
			m_PlayerPosHistory.push_back(Player_Pos);
			m_PlayerAngleHistory.push_back(Move_Angle);

			m_Dog_Walk = true;

			if (!Motion_Change) {
				auto PtrDraw = GetComponent<PNTBoneModelDraw>();
				PtrDraw->ChangeCurrentAnimation(L"Walk");
				Motion_Change = true;
			}
		}
	}

	void Dog::Dog_Move_Now(int MD) {
		//犬カメラの情報を呼び出す
		//auto PtrCamera = dynamic_pointer_cast<Tutorial>(GetStage())->PtrSubCamera;
		//プレイヤーの情報を呼び出す
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PTrans = PlayerPtr->GetComponent<Transform>();
		auto Move_Speed = PlayerPtr->Move_Speed;
		//プレゼントボックスグループを呼び出す
		auto pbgPtr = GetStage()->GetSharedObjectGroup(L"PresentBoxGroup");
		//スイッチグループを呼び出す
		auto sgPtr = GetStage()->GetSharedObjectGroup(L"SwitchGroup");

		//犬の上に表示するスプライト
		//auto dsprite = GetStage()->GetSharedObjectGroup(L"DogSpriteGroup")->at(0);
		auto dsprite = GetStage()->GetSharedGameObject<WallSprite>(L"DogSprite");
		auto Button_Push = GetStage()->GetSharedGameObject<WallSprite>(L"Button_Push");
		auto Face = GetStage()->GetSharedGameObject<DogSerif>(L"DogSerif");

		//犬のTransformを取得する
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto d_Trans = GetComponent<Transform>();
		auto d_Pos = d_Trans->GetPosition();
		//MDに応じて移動方向を変化させる
		switch (MD)
		{
		case 0:
			Obstacle_Decision(0);

			if (m_Dog_Walk || m_Dog_SWalk) {
				d_Trans->SetRotation(0, 3.14f, 0);
				d_Trans->SetPosition(Vector3(d_Pos.x, d_Pos.y, d_Pos.z + Move_Speed));
				dsprite->SetDrawActive(false);
				Button_Push->SetDrawActive(false);
				Face->SetDrawActive(true);
				//PtrCamera->SetDogRot(0);
				m_Move_Distance += Move_Speed;
			}
			break;
		case 1:
			Obstacle_Decision(1);

			if (m_Dog_Walk || m_Dog_SWalk) {
				d_Trans->SetRotation(0, 0, 0);
				d_Trans->SetPosition(Vector3(d_Pos.x, d_Pos.y, d_Pos.z - Move_Speed));
				dsprite->SetDrawActive(false);
				Button_Push->SetDrawActive(false);
				Face->SetDrawActive(true);
				//PtrCamera->SetDogRot(3);
				m_Move_Distance += Move_Speed;
			}
			break;
		case 2:
			Obstacle_Decision(2);

			if (m_Dog_Walk || m_Dog_SWalk) {
				d_Trans->SetRotation(0, XM_PI / 2, 0);
				d_Trans->SetPosition(Vector3(d_Pos.x - Move_Speed, d_Pos.y, d_Pos.z));
				dsprite->SetDrawActive(false);
				Button_Push->SetDrawActive(false);
				Face->SetDrawActive(true);
				//PtrCamera->SetDogRot(2);
				m_Move_Distance += Move_Speed;
			}
			break;
		case 3:

			Obstacle_Decision(3);

			if (m_Dog_Walk || m_Dog_SWalk) {
				d_Trans->SetRotation(0, -3.14f / 2, 0);
				d_Trans->SetPosition(Vector3(d_Pos.x + Move_Speed, d_Pos.y, d_Pos.z));
				dsprite->SetDrawActive(false);
				Button_Push->SetDrawActive(false);
				Face->SetDrawActive(true);
				//PtrCamera->SetDogRot(1);
				m_Move_Distance += Move_Speed;
			}
			break;
		}
		//m_MoveDistanceにMove_Speedを毎ターン格納する
		//m_Move_Distanceが1.0f(1マスの移動を終えたら)
		if (m_Move_Distance >= 1.0f) {
			m_Move_Distance = 0;
			d_Trans->SetPosition(Vector3(roundf(d_Pos.x), d_Pos.y, roundf(d_Pos.z)));
			for (auto p : pbgPtr->GetGroupVector()) {
				auto p_ptr = dynamic_pointer_cast<PresentBoxModel>(p.lock());
				if (!p_ptr->m_OpenBox) {
					p_ptr->Decision_OpenBox();
				}
			}
			for (auto s : sgPtr->GetGroupVector()) {
				dynamic_pointer_cast<SwitchModel>(s.lock())->Decision_OpenDoor();
			}

			if (m_PlayerAngleHistory.size() > 0) {
				m_PlayerAngleHistory.erase(m_PlayerAngleHistory.begin());
			}
			if (m_PlayerAngleHistory.size() < 1) {
				m_Dog_Walk = false;
			}

			m_Dog_SWalk = false;
			//auto PtrDraw = GetComponent<PNTBoneModelDraw>();
			//PtrDraw->ChangeCurrentAnimation(L"Default");
			//カメラの回転量を把握するための表示

			auto OG = GetStage()->GetSharedObjectGroup(L"OdorGroup");
			for (auto v : OG->GetGroupVector()) {
				auto od = dynamic_pointer_cast<OdorModel>(v.lock());
				if (dog_state == 1) {
					if (od->Return_Smell(GetComponent<Transform>()->GetPosition())) {
						smell_dog = true;
						state_UIChange();
					}
					else {
						smell_dog = false;
						if (!LocatePresent) {
							state_UIChange();
						}
					}
				}
			}
		}
		auto RPGptr = GetStage()->GetSharedObjectGroup(L"RoadPathGroup");

	}


	//自分の次に進む方向に障害物があるか判断
	void Dog::Obstacle_Decision(int angleNo) {
		//プレイヤーの情報を呼び出す
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PTrans = PlayerPtr->GetComponent<Transform>();
		auto p_Pos = PTrans->GetPosition();
		auto Move_Speed = PlayerPtr->Move_Speed;

		//ゲームオブジェクトグループを呼び出す
		auto ogoPtr = GetStage()->GetSharedObjectGroup(L"Other_GameObject");
		//ドアグループを呼び出す
		auto dgPtr = GetStage()->GetSharedObjectGroup(L"DoorGroup");
		//スイッチグループを呼び出す
		auto sgPtr = GetStage()->GetSharedObjectGroup(L"SwitchGroup");

		//犬の上に表示するスプライト
		//auto dsprite = GetStage()->GetSharedObjectGroup(L"DogSpriteGroup")->at(0);
		auto dsprite = GetStage()->GetSharedGameObject<WallSprite>(L"DogSprite");
		auto Button_Push = GetStage()->GetSharedGameObject<WallSprite>(L"Button_Push");


		//犬のTransformを取得する
		auto d_Trans = GetComponent<Transform>();
		auto d_Pos = d_Trans->GetPosition();

		int x = 0;
		int z = 0;
		switch (angleNo) {
		case 0:
			z = 1;
			break;
		case 1:
			z = -1;
			break;
		case 2:
			x = -1;
			break;
		case 3:
			x = 1;
			break;
		}
		for (auto v : ogoPtr->GetGroupVector()) {
			auto goptr = v.lock();
			auto goTrans = goptr->GetComponent<Transform>();
			auto goPos = goTrans->GetPosition();
			if (goPos.x == d_Pos.x + x && goPos.z == d_Pos.z + z) {
				m_Dog_Walk = false;
				m_Dog_SWalk = false;
				m_DogOnthePath = false;
				break;
			}
		}
		if (m_Dog_Walk || m_Dog_SWalk) {
			for (auto d : dgPtr->GetGroupVector()) {
				auto dptr = dynamic_pointer_cast<DoorModel>(d.lock());
				auto dTrans = dptr->GetComponent<Transform>();
				auto dPos = dTrans->GetPosition();
				if (dPos.x == d_Pos.x + x && dPos.z == d_Pos.z + z  && dptr->m_Open == false) {
					for (auto s : sgPtr->GetGroupVector()) {
						auto sptr = dynamic_pointer_cast<SwitchModel>(s.lock());
						if (sptr->m_SwitchNum == dptr->m_DoorNum) {
							auto stx = sptr->GetComponent<PNTStaticDraw>()->GetTextureResource();
							auto Face = GetStage()->GetSharedGameObject<DogSerif>(L"DogSerif");
							dsprite->GetComponent<PCTSpriteDraw>()->SetTextureResource(stx);
							dsprite->SetDrawActive(true);
							Button_Push->SetDrawActive(true);
							Face->SetDrawActive(false);
					
					}
					if (sptr->m_SwitchNum == dptr->m_DoorNum && !IsPushButton) {
						//オーディオの初期化
						m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
						m_AudioObjectPtr->AddAudioResource(L"DOG_NAKU");
						m_AudioObjectPtr->Start(L"DOG_NAKU", 0.5f);

						IsPushButton = true;
						}
					}
					m_Dog_Walk = false;
					m_Dog_SWalk = false;
					break;
				}
			}
		}
		if (p_Pos.x == d_Pos.x + x&&p_Pos.z == d_Pos.z + z) {
			auto Face = GetStage()->GetSharedGameObject<DogSerif>(L"DogSerif");

			Face->SetNum(4);
			m_Dog_Walk = false;
			m_Dog_SWalk = false;

		}
	}


	//一番近いプレゼントボックスを探す関数
	void Dog::Search_Treasure() {
		//距離判定用に使う変数
		float storage1 = 0;
		float storage2 = 0;
		auto d_Trans = GetComponent<Transform>();
		auto d_Pos = d_Trans->GetPosition();
		//PresentBoxGroupの情報を取得する
		auto PBGptr = GetStage()->GetSharedObjectGroup(L"PresentBoxGroup");
		for (auto v : PBGptr->GetGroupVector()) {
			auto pb = dynamic_pointer_cast<PresentBoxModel>(v.lock());
			if (!pb->m_OpenBox) {
				auto pbtrans = pb->GetComponent<Transform>();
				auto pb_Pos = pbtrans->GetPosition();
				if (storage1 == 0) {
					storage1 = sqrt((d_Pos.x - pb_Pos.x)*(d_Pos.x - pb_Pos.x) + (d_Pos.z - pb_Pos.z)*(d_Pos.z - pb_Pos.z));
					m_pb_storage = pb;
				}
				else {
					storage2 = sqrt((d_Pos.x - pb_Pos.x)*(d_Pos.x - pb_Pos.x) + (d_Pos.z - pb_Pos.z)*(d_Pos.z - pb_Pos.z));
					if (storage1 > storage2) {
						storage1 = storage2;
						m_pb_storage = pb;
					}
				}
			}
		}
		m_Path_History.clear();
		m_Path_History_Revert.clear();
		m_TargetTreasure = true;
	}

	//犬がいずれかのパスの座標にいるかを確認して、いずれのパスの座標にもいなかった場合
	//もしくはパスの上にのっている時、近くのパスの座標に移動するためパスの情報を格納する関数
	void Dog::Dog_SearchNearpath() {
		//距離判定用に使う変数
		float storage1 = 0;
		float storage2 = 0;
		auto d_Trans = GetComponent<Transform>();
		auto d_Pos = d_Trans->GetPosition();

		//RoadPathGroupの情報を取得する
		auto RPGptr = GetStage()->GetSharedObjectGroup(L"RoadPathGroup");
		for (auto v : RPGptr->GetGroupVector()) {
			auto rp = dynamic_pointer_cast<RoadPath>(v.lock());
			auto rptrans = rp->GetComponent<Transform>();
			auto rp_Pos = rptrans->GetPosition();
			////for文に使用
			//int Replacement_X_High = 0;
			//int Replacement_X_Low = 0;
			//int Replacement_Z_High = 0;
			//int Replacement_Z_Low = 0;
			//auto ogoPtr = GetStage()->GetSharedObjectGroup(L"Other_GameObject");
			////自分と検索したパス間にオブジェクトがないか判断
			if (m_phCounter > 0) {
				//考案1
				//if (d_Pos.z == rp_Pos.z) {
				//	for (int i = Replacement_X_Low; i <= Replacement_X_High; i++) {

				//		for (auto ogo : ogoPtr->GetGroupVector()) {
				//			auto go = ogo.lock();
				//			auto goTrans = go->GetComponent<Transform>();
				//			auto go_Pos = goTrans->GetPosition();
				//			if (go_Pos.x == i && go_Pos.z == d_Pos.z) {
				//				m_Hindrance = true;
				//				break;
				//			}
				//		}
				//	}
				//}
				//if (d_Pos.x == rp_Pos.x) {
				//	for (int j = Replacement_Z_Low; j <= Replacement_Z_High; j++) {
				//		for (auto ogo : ogoPtr->GetGroupVector()) {
				//			auto go = ogo.lock();
				//			auto goTrans = go->GetComponent<Transform>();
				//			auto go_Pos = goTrans->GetPosition();
				//			if (go_Pos.x == d_Pos.x && go_Pos.z == j) {
				//				m_Hindrance = true;
				//				break;
				//			}
				//		}
				//	}
				//}
				//考案2
				//d_Pos = m_rp_storage->GetComponent<Transform>()->GetPosition();
				//	if (d_Pos.x < rp_Pos.x) {
				//		Replacement_X_High = (int)rp_Pos.x;
				//		Replacement_X_Low = (int)d_Pos.x;
				//	}
				//	else {
				//		Replacement_X_High = (int)d_Pos.x;
				//		Replacement_X_Low = (int)rp_Pos.x;
				//	}
				//	if (d_Pos.z < rp_Pos.z) {
				//		Replacement_Z_High = (int)rp_Pos.z;
				//		Replacement_Z_Low = (int)d_Pos.z;
				//	}
				//	else {
				//		Replacement_Z_High = (int)d_Pos.z;
				//		Replacement_Z_Low = (int)rp_Pos.z;
				//	}
				//	for (int i = Replacement_X_Low; i < Replacement_X_High; i++) {
				//		for (int j = Replacement_Z_Low; j < Replacement_Z_High; j++) {
				//			for (auto ogo : ogoPtr->GetGroupVector()) {
				//				auto go = ogo.lock();
				//				auto goTrans = go->GetComponent<Transform>();
				//				auto go_Pos = goTrans->GetPosition();
				//				if (go_Pos.x == i && go_Pos.z == j) {
				//					m_Hindrance = true;
				//					break;
				//				}
				//			}
				//			if (m_Hindrance == true) {
				//				break;
				//			}
				//		}
				//		if (m_Hindrance == true) {
				//			break;
				//		}
				//	}
			}
			if (m_phCounter > 0) {
				for (int i = 0; i < m_phCounter; i++) {
					if (m_Path_History[i] == rp) {
						m_Hindrance = true;
					}
				}
			}
			if (m_phCounter2 > 0) {
				for (int i = 0; i < m_phCounter2; i++) {
					if (m_Path_History_Revert[i] == rp) {
						m_Hindrance = true;
					}
				}
			}
			if (!m_Hindrance) {
				auto pb_Pos = m_pb_storage->GetComponent<Transform>()->GetPosition();
				if (m_PresentBoxToPlayer) {
					auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
					auto PTrans = PlayerPtr->GetComponent<Transform>();
					auto p_Pos = PTrans->GetPosition();
					pb_Pos = p_Pos;
				}
				if (storage1 == 0) {
					//if (m_phCounter > 0) {
						if (!(d_Pos.x == rp_Pos.x && d_Pos.z == rp_Pos.z)) {
							if ((d_Pos.x + 1 == rp_Pos.x && d_Pos.z == rp_Pos.z) || (d_Pos.x == rp_Pos.x && d_Pos.z + 1 == rp_Pos.z) ||
								(d_Pos.x - 1 == rp_Pos.x && d_Pos.z == rp_Pos.z) || (d_Pos.x == rp_Pos.x &&d_Pos.z - 1 == rp_Pos.z)) {
								storage1 = sqrt((pb_Pos.x - rp_Pos.x)*(pb_Pos.x - rp_Pos.x) + (pb_Pos.z - rp_Pos.z)*(pb_Pos.z - rp_Pos.z));
								m_rp_storage = rp;
								//break;
							}
						}
					//}
				}
				else {
					storage2 = sqrt((pb_Pos.x - rp_Pos.x)*(pb_Pos.x - rp_Pos.x) + (pb_Pos.z - rp_Pos.z)*(pb_Pos.z - rp_Pos.z));
					if ((d_Pos.x + 1 == rp_Pos.x && d_Pos.z == rp_Pos.z) || (d_Pos.x == rp_Pos.x && d_Pos.z + 1 == rp_Pos.z) ||
						(d_Pos.x - 1 == rp_Pos.x && d_Pos.z == rp_Pos.z) || (d_Pos.x == rp_Pos.x &&d_Pos.z - 1 == rp_Pos.z)) {
						if (storage1 > storage2) {
							m_rp_storage = rp;
							storage1 = storage2;
						}
					}
				}
				//}
				//else {
				//	if (m_phCounter > 0) {
				//		if (d_Pos.x == rp_Pos.x || d_Pos.z == rp_Pos.z) {
				//			storage2 = sqrt((d_Pos.x - rp_Pos.x)*(d_Pos.x - rp_Pos.x) + (d_Pos.z - rp_Pos.z)*(d_Pos.z - rp_Pos.z));
				//			if (storage1 > storage2) {
				//				storage1 = storage2;
				//				m_rp_storage = rp;
				//			}
				//		}
				//	}
				//	else {
				//		storage2 = sqrt((d_Pos.x - rp_Pos.x)*(d_Pos.x - rp_Pos.x) + (d_Pos.z - rp_Pos.z)*(d_Pos.z - rp_Pos.z));
				//		if (storage1 > storage2) {
				//			storage1 = storage2;
				//			m_rp_storage = rp;
				//		}
				//	}
				//}

			}
			m_Hindrance = false;
		}
		if (dog_state == 1) {
			if (m_phCounter > 0) {
				if (m_rp_storage != m_Path_History.back()) {
					m_Path_History.push_back(m_rp_storage);
					m_phCounter++;
				}
				else {
					m_Path_History.pop_back();
					m_rp_storage = m_Path_History.back();
					m_Path_History_Revert.push_back(m_Path_History.back());
					m_phCounter2++;
					m_phCounter--;
				}
			}
			else {
				m_Path_History.push_back(m_rp_storage);
				m_phCounter++;
			}
		}
		else if (dog_state == 2) {
			if (m_Path_History.size()>0) {
				m_rp_storage = m_Path_History.back();
				m_Path_History.pop_back();
				//m_phCounter++;
			}
		}
		m_DogOnthePath = true;
		storage1 = 0;
	}
	//自分とパスの座標情報を取得し移動方向を決める(X軸とY軸を合わせる感じー)
	void Dog::DogToPath_MoveAngle() {
		//事前に取得しているパスの情報
		auto rp = m_rp_storage;
		auto rptrans = rp->GetComponent<Transform>();
		auto rp_Pos = rptrans->GetPosition();
		//事前に取得しているプレゼントボックスの情報
		auto pb = m_pb_storage;
		auto pbtrans = pb->GetComponent<Transform>();
		auto pb_Pos = pbtrans->GetPosition();
		//犬の位置情報を格納
		auto d_Trans = GetComponent<Transform>();
		auto d_Pos = d_Trans->GetPosition();
		auto d_Rot = d_Trans->GetRotation();
		//プレイヤーの情報を呼び出す
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PTrans = PlayerPtr->GetComponent<Transform>();
		auto p_Pos = PTrans->GetPosition();

		//ゲームオブジェクトグループを呼び出す
		auto ogoPtr = GetStage()->GetSharedObjectGroup(L"Other_GameObject");

		if (!m_PresentBoxToPlayer) {
			if (!m_PathToPresentBox) {
				if (rp_Pos.x == d_Pos.x && rp_Pos.z == d_Pos.z) {
					auto pb_Pos = m_pb_storage->GetComponent<Transform>()->GetPosition();
					if (pb_Pos.x == rp_Pos.x || pb_Pos.z == rp_Pos.z) {
						m_Hindrance = false;
						int Replacement_X_High;
						int Replacement_X_Low;
						int Replacement_Z_High;
						int Replacement_Z_Low;

						if (pb_Pos.x < rp_Pos.x) {
							Replacement_X_High = round(rp_Pos.x);
							Replacement_X_Low = round(pb_Pos.x);
						}
						else {
							Replacement_X_High = round(pb_Pos.x);
							Replacement_X_Low = round(rp_Pos.x);
						}
						if (pb_Pos.z < rp_Pos.z) {
							Replacement_Z_High = round(rp_Pos.z);
							Replacement_Z_Low = round(pb_Pos.z);
						}
						else {
							Replacement_Z_High = round(pb_Pos.z);
							Replacement_Z_Low = round(rp_Pos.z);
						}

						if (round(pb_Pos.z) == round(rp_Pos.z)) {
							for (int i = Replacement_X_Low; i <= Replacement_X_High; i++) {

								for (auto ogo : ogoPtr->GetGroupVector()) {
									auto go = ogo.lock();
									auto goTrans = go->GetComponent<Transform>();
									auto go_Pos = goTrans->GetPosition();
									if (round(go_Pos.x) == i && round(go_Pos.z) == round(pb_Pos.z)) {
										m_Hindrance = true;
										break;
									}
								}
							}
						}
						if (round(pb_Pos.x) == round(rp_Pos.x)) {
							for (int j = Replacement_Z_Low; j <= Replacement_Z_High; j++) {
								for (auto ogo : ogoPtr->GetGroupVector()) {
									auto go = ogo.lock();
									auto goTrans = go->GetComponent<Transform>();
									auto go_Pos = goTrans->GetPosition();
									if (round(go_Pos.x) == round(pb_Pos.x) &&round(go_Pos.z) == j) {
										m_Hindrance = true;
										break;
									}
								}
							}
						}
						if (!m_Hindrance) {
							m_PathToPresentBox = true;
						}
						else {
							m_DogOnthePath = false;
						}
					}
					else {
						m_DogOnthePath = false;
					}
				}
				else {
					if (rp_Pos.z > d_Pos.z) {
						m_Move_Direction = 0;
					}
					else if (rp_Pos.z < d_Pos.z) {
						m_Move_Direction = 1;
					}
					else if (rp_Pos.x < d_Pos.x) {
						m_Move_Direction = 2;
					}
					else if (rp_Pos.x > d_Pos.x) {
						m_Move_Direction = 3;
					}

					m_Dog_SWalk = true;
					if (!Motion_Change) {
						auto PtrDraw = GetComponent<PNTBoneModelDraw>();
						if (PtrDraw->GetCurrentAnimation() != L"Walk") {
							PtrDraw->ChangeCurrentAnimation(L"Walk");
							Motion_Change = true;
						}
					}
				}
			}
			else {
				if (pb_Pos.z > d_Pos.z) {
					m_Move_Direction = 0;
				}
				else if (pb_Pos.z < d_Pos.z) {
					m_Move_Direction = 1;
				}
				else if (pb_Pos.x < d_Pos.x) {
					m_Move_Direction = 2;
				}
				else if (pb_Pos.x > d_Pos.x) {
					m_Move_Direction = 3;
				}

				m_Dog_SWalk = true;
				if (!Motion_Change) {
					auto PtrDraw = GetComponent<PNTBoneModelDraw>();
					if (PtrDraw->GetCurrentAnimation() != L"Walk") {
						PtrDraw->ChangeCurrentAnimation(L"Walk");
						Motion_Change = true;
					}
				}
			}
		}
		else {
			//if (((int)sqrt((d_Pos.x - p_Pos.x)*(d_Pos.x - p_Pos.x) + (d_Pos.z - p_Pos.z)*(d_Pos.z - p_Pos.z)) == 1) && (d_Pos.x == p_Pos.x || d_Pos.z == p_Pos.z)) {
			if((d_Pos.x + 1 == p_Pos.x && d_Pos.z == p_Pos.z) || (d_Pos.x == p_Pos.x && d_Pos.z + 1 == p_Pos.z) ||
				(d_Pos.x - 1 == p_Pos.x && d_Pos.z == p_Pos.z) || (d_Pos.x == p_Pos.x &&d_Pos.z - 1 == p_Pos.z)){
				GetStateMachine()->ChangeState(D_DefaultState::Instance());
				m_PlayerPosHistory.clear();
			}
			else {
				if (!m_PathToPresentBox) {
					if (round(p_Pos.x) == round(d_Pos.x) || round(p_Pos.z) == round(d_Pos.z)) {
						m_Hindrance = false;
						int Replacement_X_High;
						int Replacement_X_Low;
						int Replacement_Z_High;
						int Replacement_Z_Low;

						if (p_Pos.x < d_Pos.x) {
							Replacement_X_High = round(d_Pos.x);
							Replacement_X_Low = round(p_Pos.x);
						}
						else {
							Replacement_X_High = round(p_Pos.x);
							Replacement_X_Low = round(d_Pos.x);
						}
						if (p_Pos.z < d_Pos.z) {
							Replacement_Z_High = round(d_Pos.z);
							Replacement_Z_Low = round(p_Pos.z);
						}
						else {
							Replacement_Z_High = round(p_Pos.z);
							Replacement_Z_Low = round(d_Pos.z);
						}

						if (round(p_Pos.z) == round(d_Pos.z)) {
							for (int i = Replacement_X_Low; i <= Replacement_X_High; i++) {

								for (auto ogo : ogoPtr->GetGroupVector()) {
									auto go = ogo.lock();
									auto goTrans = go->GetComponent<Transform>();
									auto go_Pos = goTrans->GetPosition();
									if (round(go_Pos.x) == i && round(go_Pos.z) == round(p_Pos.z)) {
										m_Hindrance = true;
										break;
									}
								}
							}
						}
						if (round(p_Pos.x) == round(d_Pos.x)) {
							for (int j = Replacement_Z_Low; j <= Replacement_Z_High; j++) {
								for (auto ogo : ogoPtr->GetGroupVector()) {
									auto go = ogo.lock();
									auto goTrans = go->GetComponent<Transform>();
									auto go_Pos = goTrans->GetPosition();
									if (round(go_Pos.x) == round(p_Pos.x) && round(go_Pos.z) == j) {
										m_Hindrance = true;
										break;
									}
								}
							}
						}
						if (!m_Hindrance) {
							m_PathToPresentBox = true;
							return;
						}
						
					}
					else {
						m_DogOnthePath = false;
					}
					if (rp_Pos.z > d_Pos.z) {
						m_Move_Direction = 0;
					}
					else if (rp_Pos.z < d_Pos.z) {
						m_Move_Direction = 1;
					}
					else if (rp_Pos.x < d_Pos.x) {
						m_Move_Direction = 2;
					}
					else if (rp_Pos.x > d_Pos.x) {
						m_Move_Direction = 3;
					}

					m_Dog_SWalk = true;
					if (!Motion_Change) {
						auto PtrDraw = GetComponent<PNTBoneModelDraw>();
						if (PtrDraw->GetCurrentAnimation() != L"Walk") {
							PtrDraw->ChangeCurrentAnimation(L"Walk");
							Motion_Change = true;
						}
					}
				}
				else {
					if (!(p_Pos.x == d_Pos.x) && !(p_Pos.z == d_Pos.z)) {
					m_PathToPresentBox = false;
					}
					if (p_Pos.z > d_Pos.z) {
						m_Move_Direction = 0;
					}
					else if (p_Pos.z < d_Pos.z) {
						m_Move_Direction = 1;
					}
					if (p_Pos.x < d_Pos.x) {
						m_Move_Direction = 2;
					}
					else if (p_Pos.x > d_Pos.x) {
						m_Move_Direction = 3;
					}
					m_Dog_SWalk = true;
					if (!Motion_Change) {
						auto PtrDraw = GetComponent<PNTBoneModelDraw>();
						if (PtrDraw->GetCurrentAnimation() != L"Walk") {
							PtrDraw->ChangeCurrentAnimation(L"Walk");
							Motion_Change = true;
						}
					}
				}
			}
		}
	}



	void Dog::State_Branch() {
		//犬の位置情報を格納
		auto d_Trans = GetComponent<Transform>();
		auto d_Pos = d_Trans->GetPosition();
		//プレイヤーの情報を呼び出す
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PTrans = PlayerPtr->GetComponent<Transform>();
		auto p_Pos = PTrans->GetPosition();

		auto ElapsedTime = App::GetApp()->GetElapsedTime();

		if(LocatePresent){
			m_PresentCount += ElapsedTime;
			if (m_PresentCount>2.0) {
				LocatePresent = false;
				m_PresentCount = 0;
			}
		}
		else {
			if ((round(sqrt((d_Pos.x - p_Pos.x)*(d_Pos.x - p_Pos.x)) + round((d_Pos.z - p_Pos.z)*(d_Pos.z - p_Pos.z))) == 1)
				&& (d_Pos.x == p_Pos.x || d_Pos.z == p_Pos.z)) {
				GetStateMachine()->ChangeState(D_DefaultState::Instance());
			}
			else {
				GetStateMachine()->ChangeState(ApproachState::Instance());
				//m_Path_History.clear();
				m_phCounter = 0;
			}
		}
	}

	void Dog::state_UIChange() {
		auto Face = GetStage()->GetSharedGameObject<DogSerif>(L"DogSerif");
		//auto d_ui = GetStage()->GetSharedGameObject<WallSprite>(L"State_UI")->GetComponent<PCTSpriteDraw>();
		switch (dog_state)
		{
		//通常ステート
		case 0:
			Face->SetNum(8);
			break;
		//探索ステート
		case 1:
			if (smell_dog) {
				switch (rand() % 2 + 1)
				{
				case 1:
					Face->SetNum(5);
					break;
				case 2:
					Face->SetNum(11);
					break;
				}
			}
			else {
				Face->SetNum(2);
			}
			break;
		//帰ってくるステート
		case 2:
			Face->SetNum(0);
			break;
		}
	}

	//--------------------------------------------------------------------------------------
	//	class D_DefaultState : public ObjState<Dog>;
	//	用途: 通常移動(プレイヤーの後ろについていくとき)
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<D_DefaultState> D_DefaultState::Instance() {
		static shared_ptr<D_DefaultState> instance;
		if (!instance) {
			instance = shared_ptr<D_DefaultState>(new D_DefaultState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void D_DefaultState::Enter(const shared_ptr<Dog>& Obj) {
		Obj->dog_state = 0;
		Obj->state_UIChange();
		Obj->m_PlayerAngleHistory.clear();
		Obj->m_PlayerPosHistory.clear();
		Obj->CurrentDefault = true;
	}
	//ステート実行中に毎ターン呼ばれる関数
	void D_DefaultState::Execute(const shared_ptr<Dog>& Obj) {
		if (Obj->m_Dog_Walk) {
			Obj->Dog_Move_Now(*Obj->m_PlayerAngleHistory.begin());
		}
		else {
			Obj->m_PlayerAngleHistory.clear();
		}
		if (Obj->m_Dog_OrderNow) {
			Obj->GetStateMachine()->ChangeState(SearchState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void D_DefaultState::Exit(const shared_ptr<Dog>& Obj) {
		//何もしない
		Obj->CurrentDefault = false;
	}


	//--------------------------------------------------------------------------------------
	//	class SearchState : public ObjState<Dog>;
	//	用途: 探索状態
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<SearchState> SearchState::Instance() {
		static shared_ptr<SearchState> instance;
		if (!instance) {
			instance = shared_ptr<SearchState>(new SearchState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void SearchState::Enter(const shared_ptr<Dog>& Obj) {
		Obj->dog_state = 1;	
		Obj->state_UIChange();
		if (Obj->m_Dog_Walk) {
			Obj->m_Dog_Walk = false;
		}
		Obj->m_PlayerAngleHistory.clear();
		Obj->m_Path_History.clear();
		Obj->m_Path_History_Revert.clear();
		Obj->m_PathToPresentBox = false;
		Obj->walkanim = true;
	}
	//ステート実行中に毎ターン呼ばれる関数
	void SearchState::Execute(const shared_ptr<Dog>& Obj) {
		if (!Obj->m_Dog_OrderNow) {
			Obj->m_TargetTreasure = false;
			Obj->m_Dog_SWalk = false;
			/*Obj->m_DogOnthePath = false;*/
			Obj->State_Branch();
			//Obj->GetStateMachine()->ChangeState(D_DefaultState::Instance());
		}

		if (Obj->m_Dog_OrderNow) {
			if (!Obj->smell_dog) {
				if (!Obj->m_TargetTreasure) {
					Obj->Search_Treasure();
					Obj->m_phCounter = 0;
				}
				if (!Obj->m_DogOnthePath) {
					Obj->Dog_SearchNearpath();
					//Obj->m_phCounter++;
				}
				else if (Obj->m_DogOnthePath) {
					if (!Obj->m_Dog_SWalk) {
						Obj->DogToPath_MoveAngle();
					}
					if (Obj->m_Dog_SWalk) {
						Obj->Dog_Move_Now(Obj->m_Move_Direction);
					}
				}
			}
			else {
				if (!Obj->m_Dog_SWalk) {
					Obj->m_Move_Direction++;
					if (Obj->m_Move_Direction > 3) {
						Obj->m_Move_Direction = 0;
					}
					Obj->m_Dog_SWalk = true;
				}
				if (Obj->m_Dog_SWalk) {
					Obj->Dog_Move_Now(Obj->m_Move_Direction);
				}
			}
			if (Obj->LocatePresent) {
				Obj->m_Dog_OrderNow = false;
			}
		}


	}
	//ステートにから抜けるときに呼ばれる関数
	void SearchState::Exit(const shared_ptr<Dog>& Obj) {
		Obj->LocatePresent = false;
		Obj->walkanim = false;
	}

	//--------------------------------------------------------------------------------------
	//	class ApproachState : public ObjState<Dog>;
	//	用途: プレイヤーと距離が離れている時の状態
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<ApproachState> ApproachState::Instance() {
		static shared_ptr<ApproachState> instance;
		if (!instance) {
			instance = shared_ptr<ApproachState>(new ApproachState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void ApproachState::Enter(const shared_ptr<Dog>& Obj) {
		Obj->dog_state = 2;
		Obj->state_UIChange();
		Obj->m_DogOnthePath = false;
		Obj->m_Dog_SWalk = false;
		Obj->m_PresentBoxToPlayer = true;
		Obj->walkanim = true;
		//Obj->m_Path_History.clear();
		//Obj->m_Path_History_Revert.clear();
		Obj->m_phCounter = 0;
		Obj->m_phCounter2 = 0;
	}
	//ステート実行中に毎ターン呼ばれる関数
	void ApproachState::Execute(const shared_ptr<Dog>& Obj) {
			if (!Obj->m_DogOnthePath) {
				Obj->Dog_SearchNearpath();
				//Obj->m_phCounter++;
			}
			else if (Obj->m_DogOnthePath) {
				if (!Obj->m_Dog_SWalk) {
					Obj->DogToPath_MoveAngle();
				}
				if (Obj->m_Dog_SWalk) {
					Obj->Dog_Move_Now(Obj->m_Move_Direction);
				}
			}

	}
	//ステートにから抜けるときに呼ばれる関数
	void ApproachState::Exit(const shared_ptr<Dog>& Obj) {
		Obj->m_PresentBoxToPlayer = false;
		Obj->walkanim = false;
	}




	//犬の上に出るスプライト

	//--------------------------------------------------------------------------------------
	//スクエア
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Square::Square(const shared_ptr<Stage>& StagePtr,
		const shared_ptr<GameObject>& DogObject,int i) :
		GameObject(StagePtr),
		m_DogObject(DogObject),
		Allocation(i)
	{}
	Square::~Square() {}

	//初期化
	void Square::OnCreate() {

		auto PtrTransform = GetComponent<Transform>();
		if (!m_DogObject.expired()) {
			auto DogPtr = m_DogObject.lock();
			auto DogTransPtr = DogPtr->GetComponent<Transform>();
			auto Pos = DogTransPtr->GetPosition();
			//Pos.y += 0.8f;
			PtrTransform->SetPosition(Pos);
			PtrTransform->SetScale(0.5f, 0.5f, 0.1f);
			PtrTransform->SetQuaternion(DogTransPtr->GetQuaternion());
			//変更できるスクエアリソースを作成
			//頂点配列
			vector<VertexPositionNormalTexture> vertices;
			//インデックスを作成するための配列
			vector<uint16_t> indices;

			//頂点の型を変えた新しい頂点を作成
			vector<VertexPositionColorTexture> new_vertices;

			auto DrawComp = AddComponent<PCTStaticDraw>();

			//Squareの作成(ヘルパー関数を利用)
			MeshUtill::CreateSquare(1.0f, vertices, indices);
			auto PtrAction = AddComponent<Action>();

			//シーンの情報を取得
			auto ScenePtr = App::GetApp()->GetScene<Scene>();

			//PresentBoxSpriteGroupの中身の数を確認するため
			auto Group = GetStage()->GetSharedObjectGroup(L"PresentBoxSpriteGroup");

			//頂点の座標を格納
			float Coor1 = 0;
			float Coor2 = 0;
			float Coor3 = 0;
			float Coor4 = 0;

			switch (Allocation)
			{
			//犬の通りたい扉に対応したスイッチ
			case 0:
				//左上頂点
				vertices[0].textureCoordinate = Vector2(0, 0);
				//右上頂点
				vertices[1].textureCoordinate = Vector2(1, 0);
				//左下頂点
				vertices[2].textureCoordinate = Vector2(0, 1.0f);
				//右下頂点
				vertices[3].textureCoordinate = Vector2(1, 1.0f);

				for (auto& v : vertices) {
					VertexPositionColorTexture nv;
					nv.position = v.position;
					nv.color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
					nv.textureCoordinate = v.textureCoordinate;
					new_vertices.push_back(nv);
				}
				//新しい頂点を使ってメッシュリソースの作成
				m_SquareMeshResource = MeshResource::CreateMeshResource<VertexPositionColorTexture>(new_vertices, indices, true);

				DrawComp->SetMeshResource(m_SquareMeshResource);


				DrawComp->SetTextureResource(L"SWITCH_TX");
				SetAlphaActive(true);
				SetDrawLayer(10);
				SetDrawActive(false);
				break;

			//プレゼントボックスから宝が出てくる
			case 1:
				switch (ScenePtr->GetStageNo())
				{
				case 1:
					DrawComp->SetTextureResource(L"ITEM_PHOTO_TX");
					break;
				case 2:
					DrawComp->SetTextureResource(L"ITEM_PHOTO_TX");
					break;
				case 3:
					DrawComp->SetTextureResource(L"ITEM_TOY_TX");
					break;
				case 4:
					DrawComp->SetTextureResource(L"ITEM_TOY_TX");
					break;
				case 5:
					DrawComp->SetTextureResource(L"ITEM_TOY_TX");
					break;
				default:
					DrawComp->SetTextureResource(L"ITEM_C_TX");
					break;
				}

				switch (Group->size())
				{
				case 0:
					//x軸
					Coor1 = 0.0f;
					Coor2 = 0.33f;
					//y軸
					Coor3 = 0.0f;
					Coor4 = 0.5f;
					break;
				case 1:
					//x軸
					Coor1 = 0.33f;
					Coor2 = 0.66f;
					//y軸
					Coor3 = 0.0f;
					Coor4 = 0.5f;
					break;
				case 2:
					//x軸
					Coor1 = 0.66f;
					Coor2 = 1.0f;
					//y軸
					Coor3 = 0;
					Coor4 = 0.5f;
					break;
				case 3:
					//x軸
					Coor1 = 0.0f;
					Coor2 = 0.33f;
					//y軸
					Coor3 = 0.5f;
					Coor4 = 1.0f;
					break;
				case 4:
					//x軸
					Coor1 = 0.33f;
					Coor2 = 0.66f;
					//y軸
					Coor3 = 0.5f;
					Coor4 = 1.0f;
					break;
				case 5:
					//x軸
					Coor1 = 0.66f;
					Coor2 = 1.0f;
					//y軸
					Coor3 = 0.5f;
					Coor4 = 1.0f;
					break;
				}
				//左上頂点
				vertices[0].textureCoordinate = Vector2(Coor1, Coor3);
				//右上頂点
				vertices[1].textureCoordinate = Vector2(Coor2, Coor3);
				//左下頂点
				vertices[2].textureCoordinate = Vector2(Coor1, Coor4);
				//右下頂点
				vertices[3].textureCoordinate = Vector2(Coor2, Coor4);

				for (auto& v : vertices) {
					VertexPositionColorTexture nv;
					nv.position = v.position;
					nv.color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
					nv.textureCoordinate = v.textureCoordinate;
					new_vertices.push_back(nv);
				}
				//新しい頂点を使ってメッシュリソースの作成
				m_SquareMeshResource = MeshResource::CreateMeshResource<VertexPositionColorTexture>(new_vertices, indices, true);


				DrawComp->SetMeshResource(m_SquareMeshResource);

				SetAlphaActive(true);
				SetDrawLayer(10);
				SetDrawActive(false);
				
				PtrAction->AddMoveBy(0.3f,Vector3(0, 1, 0));
				PtrAction->SetLooped(false);
				break;
			}
		}

	}


	//変化
	void Square::OnUpdate() {

		if (!m_DogObject.expired()) {
			auto DogPtr = m_DogObject.lock();
			auto DogTransPtr = DogPtr->GetComponent<Transform>();

			auto PtrTransform = GetComponent<Transform>();
			auto Pos = DogTransPtr->GetPosition();
	
			PtrTransform->SetScale(0.5f, 0.5f, 0.1f);

			if (Allocation == 0) {

				//auto PtrCamera = dynamic_pointer_cast<Tutorial>(GetStage())->PtrSubCamera;

				Pos.y += 1.0f;
				PtrTransform->SetPosition(Pos);

				//マルチビューがある場合
				//Quaternion Qt;
				////向きをビルボードにする
				//Qt.Billboard(PtrCamera->GetAt() - PtrCamera->GetEye());

				//PtrTransform->SetQuaternion(Qt);

				auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetStage()->GetView()->GetTargetCamera());

				Quaternion Qt;
				//向きをビルボードにする
				Qt.Billboard(PtrCamera->GetAt() - PtrCamera->GetEye());

				PtrTransform->SetQuaternion(Qt);

			}
			else if (Allocation == 1) {
				auto PtrAction = GetComponent<Action>();
				
				if(GetDrawActive()){
					if (!Fly_Out) {
						PtrAction->Run();
						Fly_Out = true;
					}
				}

				auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetStage()->GetView()->GetTargetCamera());

				Quaternion Qt;
				//向きをビルボードにする
				Qt.Billboard(PtrCamera->GetAt() - PtrCamera->GetEye());

				PtrTransform->SetQuaternion(Qt);

			}
		}

	}

}
//end basecross

