/*!
@file Tutorial.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス
	//--------------------------------------------------------------------------------------
	class TitleStage : public Stage {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//壁模様のスプライト作成
		void CreateWallSprite();
		//スクロールするスプライト作成
		void CreateScrollSprite();
		//スイッチのスプライト作成
		void CreateSwitchSprite();
		//ボタン一発しかならないやつ
		bool IsPushButton;


		//不透明度
		float tou = 0.0;

		//Fadeout(黒)を開始する変数
		bool FO_StartB = false;
		//Fadeout(白)を開始する変数
		bool FO_StartW = false;
		bool FO_StartI = false;
		bool once = false;
		
	public:
		//構築と破棄
		TitleStage() :Stage() {}
		virtual ~TitleStage();
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;

		void SetFOI_Start(bool b) {
			FO_StartI = b;
		};
	};



	//--------------------------------------------------------------------------------------
	//	セレクトステージクラス
	//--------------------------------------------------------------------------------------
	class SelectStage : public Stage {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//壁模様のスプライト作成
		void CreateWallSprite();
		//ボタン一発しかならないやつ
		bool IsPushButton;
		//ステージセレクト用変数
		int StageSelectNo = 0;
		bool OnePushSelect = false;
	public:
		//構築と破棄
		SelectStage() :Stage() {}
		virtual ~SelectStage();
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
	};


	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		void CreatePlate();
		//ステージオブジェクトの作成
		void CreateStageObject();
		//タイマー
		void CreateTimer();
		//アイテムカウンター
		void CreateItemCounter();

		bool TimerStop;

		//ボタン一発しかならないやつ
		bool IsPushButton;
		//タイマーの時間設定
		float GameTimer = 100;

		float Time_Scaling = -1.0f;

		float Timerharf = GameTimer / 2;



		//CSV読み込み 
		void LoadMAPCSV();
		UINT m_StageNum = 1;
		int *m_Stagearr;				//ステージ配列
		Vector2 m_StageSize;			//ステージサイズ
		Vector3 m_PlayerinitPos;		//プレイヤーの初期位置
		//Csv用変数
		size_t line_size;
		size_t col_size;

		//クリアしてから一定時間経過後クリアステージにいく
		int ClearCounter = 0;

		//ワーク用のベクター配列のメンバ変数
		vector< vector <size_t> > m_MapDataVec;

		//FadeOut
		void CreateFade();
	public:
		//構築と破棄
		GameStage() :Stage() {}
		virtual ~GameStage();
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		//アイテムカウンター用の変数
		//ステージ内のアイテムの総数
		int All_ItemCount = 0;
		//現在取得しているアイテムの数
		int Now_ItemCount = 0;
		//置物があるかどうか
		bool Ornament_Decision=false;

		shared_ptr<LookAtCamera3> PtrSubCamera;
	};

	//--------------------------------------------------------------------------------------
	//	クリアステージクラス
	//--------------------------------------------------------------------------------------
	class ClearStage : public Stage {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//壁模様のスプライト作成
		void CreateWallSprite();
		//ボタン一発しかならないやつ
		bool IsPushButton;

		//セレクト用変数
		int StageSelectNo = 0;
		bool OnePushSelect = false;
	public:
		//構築と破棄
		ClearStage() :Stage() {}
		virtual ~ClearStage();
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
	};

	//--------------------------------------------------------------------------------------
	//	ゲームオーバー
	//--------------------------------------------------------------------------------------
	class GameOverStage : public Stage {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//壁模様のスプライト作成
		void CreateWallSprite();
		//ボタン一発しかならないやつ
		bool IsPushButton;

		//セレクト用変数
		int StageSelectNo = 0;
		bool OnePushSelect = false;


	public:
		//構築と破棄
		GameOverStage() :Stage() {}
		virtual ~GameOverStage();
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
	};
	

}
//end basecross

