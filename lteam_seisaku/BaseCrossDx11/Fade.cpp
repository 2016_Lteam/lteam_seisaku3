#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	//	class FadeOut : public GameObject;
	//	用途: フェードアウト用
	//--------------------------------------------------------------------------------------

	//構築と破棄
	FadeOut::FadeOut(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	void FadeOut::CreateResourses() {
		//画像
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Black.png";
		App::GetApp()->RegisterTexture(L"BLACK_TX", strTexture);

		//SE

	}

	//初期化
	void FadeOut::OnCreate() {
		CreateResourses();
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());
		//初期位置などの設定
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(w, h, 0.1f);
		Ptr->SetRotation(0, 0, 0);
		Ptr->SetPosition(0, 0, 0);

		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 0.0f));
		PtrSprite->SetTextureResource(L"BLACK_TX");

		//透明処理
		SetAlphaActive(true);
	}

	void FadeOut::OnUpdate() {
		auto stage = dynamic_pointer_cast<Stage>(GetStage());
		if (FO_StartB == false) {
			if (FO_StartW == true) {
				if (once) {
					once = false;
				}
				GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(1.0f, 1.0f, 1.0f, tou += 0.01f));
				if (tou >= 1.0f) {
					auto ScenePtr = App::GetApp()->GetScene<Scene>();
					PostEvent(0.0f, GetThis<FadeOut>(), ScenePtr, L"ToClearStage");
				}
			}
		}
		if (FO_StartW == false) {
			if (FO_StartB == true) {
				if (once) {
					once = false;
				}
				GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(0.0f, 0.0f, 0.0f, tou += 0.02f));
				if (tou >= 1.0f) {
					auto ScenePtr = App::GetApp()->GetScene<Scene>();
					PostEvent(0.0f, GetThis<FadeOut>(), ScenePtr, L"ToGameOverStage");
				}
			}
		}

		if (FO_StartB_License == true) {
			if (once) {
				once = false;
			}
			GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(0.0f, 0.0f, 0.0f, tou += 0.02f));
			if (tou >= 1.0f) {
				auto ScenePtr = App::GetApp()->GetScene<Scene>();
				PostEvent(0.0f, GetThis<FadeOut>(), ScenePtr, L"ToMenuStage");
			}
		}

		if (FO_StartB == false) {
			if (FO_StartS == true) {
				if (once) {
					once = false;
				}
				GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(0.0f, 0.0f, 0.0f, tou += 0.02f));
				if (tou >= 1.0f) {
					auto ScenePtr = App::GetApp()->GetScene<Scene>();
					PostEvent(0.0f, GetThis<FadeOut>(), ScenePtr, L"ToSelectStage");
				}
			}
		}

		if (FO_StartB == false) {
			if (FO_StartG == true) {
				if (once) {
					once = false;
				}
				GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(0.0f, 0.0f, 0.0f, tou += 0.02f));
				if (tou >= 1.0f) {
					auto ScenePtr = App::GetApp()->GetScene<Scene>();
					if(ScenePtr->GetStageNo() == 0) {
						PostEvent(0.5f, GetThis<FadeOut>(), ScenePtr, L"ToTutorial");
					}
					else {
						PostEvent(0.5f, GetThis<FadeOut>(), ScenePtr, L"ToGameStage");
					}
				}
			}
		}

		if (FO_StartB == false) {
			if (FO_StartT == true) {
				if (once) {
					once = false;
				}
				GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(0.0f, 0.0f, 0.0f, tou += 0.02f));
				if (tou >= 1.0f) {
					auto ScenePtr = App::GetApp()->GetScene<Scene>();
					PostEvent(1.0f, GetThis<FadeOut>(), ScenePtr, L"ToMenuStage");
				}
			}
		}

		if (FO_StartB == false) {
			if (FO_StartA == true) {
				if (once) {
					once = false;
				}
				GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(0.5f, 0.0f, 0.0f, tou += 0.01f));
				if (tou >= 0.3f ) {
					tou = false;
				}
			}
		}

	}

	void FadeOut::OnLastUpdate() {
	}


	//--------------------------------------------------------------------------------------
	//	class FadeOut : public GameObject;
	//	用途: フェードアウト(犬)
	//--------------------------------------------------------------------------------------

	//構築と破棄
	FadeOut2::FadeOut2(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	void FadeOut2::CreateResourses() {
		//画像
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Dog.png";
		App::GetApp()->RegisterTexture(L"DOG_TX", strTexture);
	}

	//初期化
	void FadeOut2::OnCreate() {
		CreateResourses();
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());
		//初期位置などの設定
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(w, h, 0.1f);
		Ptr->SetRotation(0, 0, 0);
		Ptr->SetPosition(0, 0, 0);

		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 0.0f));
		PtrSprite->SetTextureResource(L"DOG_TX");

		//透明処理
		SetAlphaActive(true);
	}

	void FadeOut2::OnUpdate() {
		auto stage = dynamic_pointer_cast<Stage>(GetStage());
			
		if (FO_StartB == false) {
			if (FO_StartD == true) {
				if (once) {
					once = false;
				}
				GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(0.0f, 0.0f, 0.0f, tou += 0.02f));
				if (tou >= 1.0f) {
					auto ScenePtr = App::GetApp()->GetScene<Scene>();
					if (ScenePtr->GetStageNo() == 0) {
						PostEvent(0.5f, GetThis<FadeOut2>(), ScenePtr, L"ToTutorial");
					}
					else {
						PostEvent(0.5f, GetThis<FadeOut2>(), ScenePtr, L"ToGameStage");
					}
				}
			}
		}

	}

	void FadeOut2::OnLastUpdate() {
	}



	//--------------------------------------------------------------------------------------
	//	class FadeOut : public GameObject;
	//	用途: フェードイン用
	//--------------------------------------------------------------------------------------

	//構築と破棄
	FadeIn::FadeIn(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	void FadeIn::CreateResourses() {
		//画像
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Black.png";
		App::GetApp()->RegisterTexture(L"BLACK_TX", strTexture);

	}

	//初期化
	void FadeIn::OnCreate() {
		CreateResourses();
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());
		//初期位置などの設定
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(w, h, 0.1f);
		Ptr->SetRotation(0, 0, 0);
		Ptr->SetPosition(0, 0, 0);

		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"BLACK_TX");

		//透明処理
		SetAlphaActive(true);
	}

	void FadeIn::OnUpdate() {
		auto stage = dynamic_pointer_cast<Stage>(GetStage());
		if (FO_StartB == false) {
			if (FO_StartI == true) {
				if (once) {
					once = false;
				}
				GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(1.0f, 1.0f, 1.0f, tou += -0.02f));

			}
		}	
		if (FO_StartB == false) {
			if (FO_StartW == true) {
				if (once) {
					once = false;
				}
				GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(0.0f, 0.0f, 0.0f, tou += -0.02f));

			}
		}
	}

	void FadeIn::OnLastUpdate() {
	}

	//--------------------------------------------------------------------------------------
	//	class FadeOut : public GameObject;
	//	用途: フェードイン(犬)
	//--------------------------------------------------------------------------------------

	//構築と破棄
	FadeIn2::FadeIn2(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	void FadeIn2::CreateResourses() {
		//画像
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Dog.png";
		App::GetApp()->RegisterTexture(L"DOG_TX", strTexture);

	}

	//初期化
	void FadeIn2::OnCreate() {
		CreateResourses();
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());
		//初期位置などの設定
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(w, h, 0.1f);
		Ptr->SetRotation(0, 0, 0);
		Ptr->SetPosition(0, 0, 0);

		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"DOG_TX");

		//透明処理
		SetAlphaActive(true);
	}

	void FadeIn2::OnUpdate() {
		auto stage = dynamic_pointer_cast<Stage>(GetStage());
		if (FO_StartB == false) {
			if (FO_StartD == true) {
				if (once) {
					once = false;
				}
				GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(0.0f, 0.0f, 0.0f, tou += -0.02f));

			}
		}
	}

	void FadeIn2::OnLastUpdate() {
	}

	//--------------------------------------------------------------------------------------
	//	class FadeOut : public GameObject;
	//	用途: フェードイン(提供)
	//--------------------------------------------------------------------------------------

	//構築と破棄
	FadeIn3::FadeIn3(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	void FadeIn3::CreateResourses() {
		//画像
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Teikyou.png";
		App::GetApp()->RegisterTexture(L"TEIKYOU_TX", strTexture);

	}

	//初期化
	void FadeIn3::OnCreate() {
		CreateResourses();
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());
		//初期位置などの設定
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(w, h, 0.1f);
		Ptr->SetRotation(0, 0, 0);
		Ptr->SetPosition(0, 0, 0);

		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"TEIKYOU_TX");

		//透明処理
		SetAlphaActive(true);
	}

	void FadeIn3::OnUpdate() {
		auto stage = dynamic_pointer_cast<Stage>(GetStage());
		if (FO_StartB == false) {
			if (FO_StartT == true) {
				if (once) {
					once = false;
				}
				GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(0.0f, 0.0f, 0.0f, tou += -0.0001f));
				if (tou <= 0.994f)
					tou += -0.01;
			}
		}
	}

	void FadeIn3::OnLastUpdate() {
	}
}
//endof  basedx11
