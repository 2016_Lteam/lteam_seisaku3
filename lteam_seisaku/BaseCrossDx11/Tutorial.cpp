/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------


	//リソースの作成
	void Tutorial::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		//wstring strTexture = DataDir + L"trace.png";
		//App::GetApp()->RegisterTexture(L"TRACE_TX", strTexture);
		wstring strTexture = DataDir + L"Floor2.png";
		App::GetApp()->RegisterTexture(L"FLOOR_TX", strTexture);
		strTexture = DataDir + L"spark.png";
		App::GetApp()->RegisterTexture(L"SPARK_TX", strTexture);
		strTexture = DataDir + L"number.png";
		App::GetApp()->RegisterTexture(L"NUMBER_TX", strTexture);
		strTexture = DataDir + L"Wall.png";
		App::GetApp()->RegisterTexture(L"WALL_TX", strTexture);
		strTexture = DataDir + L"DogCamera.png";
		App::GetApp()->RegisterTexture(L"DOGCAMERA_TX", strTexture);
		strTexture = DataDir + L"DogColor.png";
		App::GetApp()->RegisterTexture(L"DOG_COLOR_TX", strTexture);
		strTexture = DataDir + L"switch_Door.png";
		App::GetApp()->RegisterTexture(L"SWITCH_TX", strTexture);
		strTexture = DataDir + L"switch_Door2.png";
		App::GetApp()->RegisterTexture(L"SWITCH2_TX", strTexture);
		strTexture = DataDir + L"switch_Door3.png";
		App::GetApp()->RegisterTexture(L"SWITCH3_TX", strTexture);
		strTexture = DataDir + L"switch_Door4.png";
		App::GetApp()->RegisterTexture(L"SWITCH4_TX", strTexture);
		strTexture = DataDir + L"switch_Door5.png";
		App::GetApp()->RegisterTexture(L"SWITCH5_TX", strTexture);
		strTexture = DataDir + L"switch_Door6.png";
		App::GetApp()->RegisterTexture(L"SWITCH6_TX", strTexture);
		strTexture = DataDir + L"BG.png";
		App::GetApp()->RegisterTexture(L"BACKGROUND_TX", strTexture);
		strTexture = DataDir + L"number.png";
		App::GetApp()->RegisterTexture(L"NUMBER_TX", strTexture);
		strTexture = DataDir + L"Time.png";
		App::GetApp()->RegisterTexture(L"TIME_TX", strTexture);
		strTexture = DataDir + L"Slash.png";
		App::GetApp()->RegisterTexture(L"SLASH_TX", strTexture);
		strTexture = DataDir + L"Item_C.png";
		App::GetApp()->RegisterTexture(L"ITEM_C_TX", strTexture);
		strTexture = DataDir + L"Item_Photo.png";
		App::GetApp()->RegisterTexture(L"ITEM_PHOTO_TX", strTexture);
		strTexture = DataDir + L"Black.png";
		App::GetApp()->RegisterTexture(L"BLACK_TX", strTexture);

		//==================================================================
		//・チュートリアルの説明
		//ゲームの説明
		strTexture = DataDir + L"DogFollow.png";
		App::GetApp()->RegisterTexture(L"DOGFOLLOW_TX", strTexture);
		//フレーム
		strTexture = DataDir + L"TutorialFrame.png";
		App::GetApp()->RegisterTexture(L"FRAME_TX", strTexture);
		//ドアの説明
		strTexture = DataDir + L"DoorText.png";
		App::GetApp()->RegisterTexture(L"DOOR_TX", strTexture);
		//ラフレシアの説明
		strTexture = DataDir + L"RafflesiaText.png";
		App::GetApp()->RegisterTexture(L"RAFFLESIA_TX", strTexture);
		//置物の説明
		strTexture = DataDir + L"Ornament.png";
		App::GetApp()->RegisterTexture(L"ORNAMENT_TX", strTexture);
		//犬の探索説明
		strTexture = DataDir + L"Search.png";
		App::GetApp()->RegisterTexture(L"SEARCH_TX", strTexture);
		//制限時間説明
		strTexture = DataDir + L"TreasureChest.png";
		App::GetApp()->RegisterTexture(L"TREASURECHEST_TX", strTexture);
		//==================================================================
		//・左下の犬の顔
		//普通の顔
		strTexture = DataDir + L"UsuallyDog.png";
		App::GetApp()->RegisterTexture(L"UsuallyDog_TX", strTexture);
		strTexture = DataDir + L"FunDog.png";
		App::GetApp()->RegisterTexture(L"FunDog_TX", strTexture);
		strTexture = DataDir + L"BadMoodDog.png";
		App::GetApp()->RegisterTexture(L"BadMoodDog_TX", strTexture);
		//・犬の吹き出し
		strTexture = DataDir + L"OrdinaryBalloon.png";
		App::GetApp()->RegisterTexture(L"OrdinaryBalloon_TX", strTexture);
		strTexture = DataDir + L"JaggedBalloon.png";
		App::GetApp()->RegisterTexture(L"JaggedBalloon_TX", strTexture);
		//・犬のセリフ
		strTexture = DataDir + L"Serif.png";
		App::GetApp()->RegisterTexture(L"Serif_TX", strTexture);
		//ボタンの横の「ボタンを押して」っていう表示
		strTexture = DataDir + L"Button_Push.png";
		App::GetApp()->RegisterTexture(L"Button_Push_TX", strTexture);
		//==================================================================

		strTexture = DataDir + L"Odor.png";
		App::GetApp()->RegisterTexture(L"ODOR_TX", strTexture);

	}



	//ビューとライトの作成
	void Tutorial::CreateViewLight() {

		//auto PtrView = CreateView<MultiView>();
		////メインビューのビューポートとカメラの設定
		//auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera2>();
		//PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		//PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//Viewport MainViewport;
		//MainViewport.Width = static_cast<float>(App::GetApp()->GetGameWidth());
		//MainViewport.Height = static_cast<float>(App::GetApp()->GetGameHeight());
		//MainViewport.TopLeftX = 0;
		//MainViewport.TopLeftY = 0;
		//MainViewport.MinDepth = 0.5f;
		//MainViewport.MaxDepth = 1.0f;
		////比率が変わるのでカメラにビューポートを伝える
		//PtrLookAtCamera->SetViewPort(MainViewport);
		//PtrView->AddView(MainViewport, PtrLookAtCamera);

		////サブ(固定カメラ)
		//PtrSubCamera = ObjectFactory::Create<LookAtCamera3>();
		//PtrSubCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		//PtrSubCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//Viewport SubViewport;
		//SubViewport.Width = static_cast<float>(320.0f);
		//SubViewport.Height = static_cast<float>(256.0f);
		//SubViewport.TopLeftX = 900;
		//SubViewport.TopLeftY = 500;
		//SubViewport.MinDepth = 0.0f;
		//SubViewport.MaxDepth = 0.5f;
		////比率が変わるのでカメラにビューポートを伝える
		//PtrSubCamera->SetViewPort(SubViewport);
		//PtrView->AddView(SubViewport, PtrSubCamera);
		////シングルライトの作成
		//auto PtrSingleLight = CreateLight<SingleLight>();
		////ライトの設定
		//PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);

		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera2>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 15.0f, -25.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);

	}


	//プレートの作成
	void Tutorial::CreatePlate() {
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++) {
				//ステージへのゲームオブジェクトの追加
				auto Ptr = AddGameObject<GameObject>();
				//auto PtrTrans = Ptr->GetComponent<Transform>();
				//Quaternion Qt;
				//Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
				//Matrix4X4 WorldMat;
				//WorldMat.DefTransformation(
				//	Vector3(200.0f, 200.0f, 1.0f),
				//	Qt,
				//	Vector3(0.0f, 0.0f, 0.0f)
				//);
				//PtrTrans->SetScale(20.0f, 20.0f, 1.0f);
				//PtrTrans->SetQuaternion(Qt);
				//PtrTrans->SetPosition(-0.5f, 0.0f, -0.5f);
				auto PtrTrans = Ptr->GetComponent<Transform>();
				Quaternion Qt;
				Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
				Matrix4X4 WorldMat;
				WorldMat.DefTransformation(
					Vector3(10.0f, 10.0f, 1.0f),
					Qt,
					Vector3(0.0f, 0.0f, 0.0f)
				);
				PtrTrans->SetScale(1.0f, 1.0f, 1.0f);
				PtrTrans->SetQuaternion(Qt);
				PtrTrans->SetPosition(i - 10, 0.0f, j - 10);

				//描画コンポーネントの追加
				auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
				//描画コンポーネントに形状（メッシュ）を設定
				DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
				//自分に影が映りこむようにする
				DrawComp->SetOwnShadowActive(true);

				//描画コンポーネントテクスチャの設定
				DrawComp->SetTextureResource(L"FLOOR_TX");

				Ptr->SetDrawLayer(1);
			}
		}

		//背景の描画
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());


		auto bg = AddGameObject<WallSprite>(L"BACKGROUND_TX", false,
			Vector2(w, h), Vector2(0, 0));
		bg->SetDrawLayer(-1);

		////犬カメラの縁
		//bg = AddGameObject<WallSprite>(L"DOGCAMERA_TX", true,
		//	Vector2(496, 496), Vector2(4.0f, -1.0f),true);
		//bg->SetDrawLayer(2);
		//auto bgtrans = bg->GetComponent<Transform>();
		//bgtrans->SetRotation(0, 0, XM_PI/2);

		//SetSharedGameObject(L"DOGCAMERA", bg);


	}

	void Tutorial::CreateStageObject() {

		auto DSG = CreateSharedObjectGroup(L"Dog_SpriteGroup");

		auto ptr2 = AddGameObject<WallSprite>(L"UsuallyDog_TX", true,
			Vector2(320, 320), Vector2(-480, -240));
		ptr2->SetDrawLayer(2);
		SetSharedGameObject(L"DogFace", ptr2);
		DSG->IntoGroup(ptr2);
		ptr2->SetDrawLayer(11);

		ptr2 = AddGameObject<WallSprite>(L"OrdinaryBalloon_TX", true,
			Vector2(320, 320), Vector2(-320, -150));
		ptr2->SetDrawLayer(2);
		SetSharedGameObject(L"DogBalloon", ptr2);
		DSG->IntoGroup(ptr2);
		ptr2->SetDrawLayer(11);


		//扉のスイッチを表示する
		ptr2 = AddGameObject<WallSprite>(L"SWITCH_TX", true,
			Vector2(64, 64), Vector2(-370, -150));
		ptr2->SetDrawActive(false);
		ptr2->SetDrawLayer(2);
		SetSharedGameObject(L"DogSprite", ptr2);
		DSG->IntoGroup(ptr2);
		ptr2->SetDrawLayer(11);

		//「ボタンを押して」
		ptr2 = AddGameObject<WallSprite>(L"Button_Push_TX", true,
			Vector2(192, 64), Vector2(-320, -150));
		ptr2->SetDrawActive(false);
		ptr2->SetDrawLayer(2);
		SetSharedGameObject(L"Button_Push", ptr2);
		DSG->IntoGroup(ptr2);
		ptr2->SetDrawLayer(11);


		auto ptr3 = AddGameObject<DogSerif>(Vector3(-320, -150, 0.0f), Vector3(192, 64, 1.0f));
		ptr3->SetDrawLayer(2);
		SetSharedGameObject(L"DogSerif", ptr3);
		ptr3->SetDrawLayer(11);




		////Csvファイル
		//wstring Filename = App::GetApp()->m_wstrRelativeDataPath + L"Map_Csv\\" + L"data" +L".csv";
		////ローカル上にCSVファイルクラスの作成
		//CsvFile GameStageCsv(Filename);
		//if (!GameStageCsv.ReadCsv()) {
		//	//ファイルが存在しなかった
		//	//初期化失敗
		//	throw BaseException(
		//		L"CSVファイルがありません。",
		//		Filename,
		//		L"選択された場所にはアクセスできません"
		//	);
		//}
		//const int iDataSizeRow = 0;		//データが0行目から始まるようの定数
		//vector< wstring > StageMapVec;	//ワーク用のベクター配列
		//								//iDataSizeRowのデータを抜き取りベクター配列に格納
		//GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);
		//////行、列の取得
		//line_size = static_cast<size_t>(_wtoi(StageMapVec[0].c_str())); //Row  = 行
		//col_size = static_cast<size_t>(_wtoi(StageMapVec[1].c_str())); //Col　= 列
		//															   //マップデータ配列作成
		//m_MapDataVec = vector< vector<size_t> >(line_size, vector<size_t>(col_size));	//列の数と行の数分作成
		//																					//配列の初期化
		//for (size_t i = 0; i < line_size; i++) {
		//	for (size_t j = 0; j < col_size; j++)
		//	{
		//		m_MapDataVec[i][j] = 0;								//size_t型のvector配列を０で初期化			
		//	}
		//}
		////1行目からステージが定義されている
		//const int iDataStartRow = 1;
		////assert(m_sRowSize > 0 && "行が０以下なのでゲームが開始できません");
		//if (line_size > 0) {
		//	for (size_t i = 0; i < line_size; i++) {
		//		GameStageCsv.GetRowVec(i + iDataStartRow, StageMapVec);		//スタート + i　だから最初は 2が入る
		//		for (size_t j = 0; j < col_size; j++) {					//列分ループ処理
		//			const int iNum = _wtoi(StageMapVec[j].c_str());			//列から取得したwstring型をintに変換→格納	
		//		//マップデータ配列に格納
		//			m_MapDataVec[i][j] = iNum;
		//			//配置されるオブジェクトの基準スケール
		//			float ObjectScale = 1.0f;
		//			//基準Scale
		//			Vector3 Scale(ObjectScale, ObjectScale, ObjectScale);
		//			Vector3 Pos(static_cast<float>(j), 0.5, static_cast<float>(i));
		//			switch (iNum) {
		//			case 1:
		//				AddGameObject<FixedBox>(Scale, Vector3(0, 0, 0), Pos);
		//				break;
		//			}
		//		}
		//	}
		//}

		//ゲームプログラミングのファイル
		//// ファイルからマップデータを読み込む	------------------------------------------
		//FILE*	fp;
		////fp = fopen("map.csv", "r");
		//if (!fopen_s(&fp, fileName, "r")) {// 読み取り専用で開く
		//	int a;	// 読み込んだデータを一時的に保存する変数
		//	int c = 0, r = 0;	// マップ用の二次元配列の添え字
		//	char digit[100] = { 0 };
		//	int d_cnt = 0;
		//	do {
		//		a = fgetc(fp);	// ファイルから１文字読み込む
		//		if (isdigit(a)) {	// 読み込んだ文字が整数か判断
		//							//fgetcで読み込んだ文字をdigitに入れていく
		//			digit[d_cnt++] = a;
		//		}
		//		else {
		//			digit[d_cnt] = '\0';
		//			d_cnt = 0;
		//			//digitのデータを数値に変換してマップに入れる 
		//			//map[r][c++] = atoi(digit);
		//			map[r * col + c] = atoi(digit);
		//			c++;
		//			if (c >= col) {
		//				c = 0;
		//				r++;
		//				if (r>row) {
		//					break;
		//				}
		//			}
		//		}
		//	} while (a != EOF);	// ファイルの終了を示す値(EOF, End Of File)を読み込んだら終了
		//	fclose(fp);	// ファイルを閉じる
		//}
		//	------------------------------------------------------------------------------

		int MapSize = 20;

		//ゲームオブジェクトの大きさ、回転、位置
		Vector3 GO_Scale = Vector3(0, 0, 0);
		Vector3 GO_Rot = Vector3(0, 0, 0);
		Vector3 GO_Pos = Vector3(0, 0, 0);

		//プレイヤーと犬以外のオブジェクトを格納するオブジェクトグループ
		auto OGO = CreateSharedObjectGroup(L"Other_GameObject");

		//プレイヤーと犬の情報を格納するオブジェクトグループ
		auto PandD = CreateSharedObjectGroup(L"PlayerAndDog");

		//プレゼントボックスのグループ格納用
		auto PBG = CreateSharedObjectGroup(L"PresentBoxGroup");

		//ドアのグループ格納用
		auto DG = CreateSharedObjectGroup(L"DoorGroup");

		//スイッチのグループ格納用
		auto SG = CreateSharedObjectGroup(L"SwitchGroup");

		//匂いを惑わすオブジェクトグループ格納用
		auto OG = CreateSharedObjectGroup(L"OdorGroup");

		//経路探索用のパス群
		auto Route_search = CreateSharedObjectGroup(L"RoadPathGroup");

		//犬の上に表示されるスプライト
		//auto DogSprite = CreateSharedObjectGroup(L"DogSpriteGroup");

		//プレゼントボックスの上に表示されるスプライト
		auto PresentBoxSprite = CreateSharedObjectGroup(L"PresentBoxSpriteGroup");

		shared_ptr<GameObject> ptr;

		auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetView()->GetTargetCamera());

		//CSVファイルからゲームステージの選択
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath + L"Map_Csv\\";//csvファイルの入っているフォルダを指定
																				 //シーンからゲームセレクトで選んだゲームステージの番号取得
		int i_StageNumber = App::GetApp()->GetScene<Scene>()->GetTutorialNo();

		//ステージ番号からステージの名前を取得
		wstring i_StageName = Util::IntToWStr(i_StageNumber);
		wstring FileName = MediaPath + L"Tutorial_";//
		FileName += i_StageName + L".csv";//Stage番号と拡張子の指定
										  //wstring FileName = MediaPath + L"Stage_1.csv";

										  //ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(FileName);
		//ファイルがあるかどうか
		if (!GameStageCsv.ReadCsv()) {
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません",
				FileName,
				L"選択された場所にdアクセスできません"

			);
		}

		const int iDataSizeRow = 0; //データが0行目から始まるように定数
		vector<wstring>StageMapVec; //ワーク用のベクター配列
		GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);

		//行、列の取得
		auto m_sRowSize = static_cast<size_t>(20); //Row=行
		auto m_sColSize = static_cast<size_t>(20); //Col=列

												   //マップデータの配列作成
		auto map = vector<vector<size_t>>(m_sRowSize, vector<size_t>(m_sColSize)); //列の数と行の数分
																				   //配列の初期化
		for (size_t i = 0; i < m_sRowSize; i++) {
			for (size_t j = 0; j < m_sColSize; j++) {
				map[i][j] = 0;	//0で初期化
			}
		}

		for (size_t i = 0; i < m_sRowSize; i++) {
			GameStageCsv.GetRowVec(i, StageMapVec); //START + iだから最初は２が入る
			for (size_t j = 0; j < m_sColSize; j++) {
				const int iNum = _wtoi(StageMapVec[j].c_str()); //列から取得したwstringをintに
																//マップデータ配列に格納
				map[i][j] = iNum;
			}
		}



		int i, j;

		for (i = 0; i < MapSize; i++) {
			for (j = 0; j < MapSize; j++) {
				GO_Scale = Vector3(1.0f, 1.0f, 1.0f);
				GO_Rot = Vector3(0, 0, 0);
				GO_Pos = Vector3((float)j - MapSize / 2, 0.0f, (float)i - MapSize / 2);
				switch (map[j][i]) {
					//1なら壁を配置する
				case 1:
					GO_Scale = Vector3(1.0f, 2.0f, 1.0f);
					ptr = AddGameObject<FixedBox>(GO_Scale, GO_Rot, GO_Pos);
					OGO->IntoGroup(ptr);

					break;
					//2ならプレイヤーを配置する
				case 2:
					ptr = AddGameObject<Player>(GO_Scale, GO_Rot, GO_Pos);
					SetSharedGameObject(L"Player", ptr);
					Route_search->IntoGroup(AddGameObject<RoadPath>(GO_Pos));
					/*				if (PtrCamera) {
					PtrCamera->SetTargetObject(playerptr);
					}*/
					break;
					//3なら犬を配置する
				case 3:
					ptr = AddGameObject<Dog>(GO_Scale, GO_Rot, GO_Pos);
					SetSharedGameObject(L"Dog", ptr);
					//PtrSubCamera->SetTargetObject(ptr);
					//DogSprite->IntoGroup(AddGameObject<Square>(ptr,0));
					Route_search->IntoGroup(AddGameObject<RoadPath>(GO_Pos));
					break;
					//4ならプレゼントボックスを配置する
				case 4:
					GO_Scale = Vector3(0.6f, 0.6f, 0.6f);
					ptr = AddGameObject<PresentBoxModel>(GO_Scale, GO_Rot, GO_Pos, PBG->size());
					OGO->IntoGroup(ptr);
					PBG->IntoGroup(ptr);
					PresentBoxSprite->IntoGroup(AddGameObject<Square>(ptr, 1));
					break;
					//5ならドアを配置する
				case 5:
					GO_Scale = Vector3(1.0f, 1.0f, 1.0f);
					GO_Rot = Vector3(0, -3.14 / 2, 0);
					DG->IntoGroup(AddGameObject<DoorModel>(GO_Scale, GO_Rot, GO_Pos));
					Route_search->IntoGroup(AddGameObject<RoadPath>(GO_Pos));
					break;
					//6ならスイッチを配置する
				case 6:
					GO_Scale = Vector3(1.0f, 0.01f, 1.0f);
					GO_Rot = Vector3(0, 0, 0);
					SG->IntoGroup(AddGameObject<SwitchModel>(GO_Scale, GO_Rot, GO_Pos));
					Route_search->IntoGroup(AddGameObject<RoadPath>(GO_Pos));
					break;
					//7ならツリーモデルを配置する
				case 7:
					GO_Scale = Vector3(1.0f, 2.0f, 1.0f);
					ptr = AddGameObject<TreeModel>(GO_Scale, GO_Rot, GO_Pos);
					OGO->IntoGroup(ptr);
					break;
				case 8:
					GO_Scale = Vector3(1.0f, 1.0f, 1.0f);
					GO_Rot = Vector3(0, 0, 0);
					OG->IntoGroup(AddGameObject<OdorModel>(GO_Scale, GO_Rot, GO_Pos));
					break;
					//経路探索用パスの配置
				case 99:
					Route_search->IntoGroup(AddGameObject<RoadPath>(GO_Pos));
					break;
				}
			}
		}

		auto OrnamentG = CreateSharedObjectGroup(L"OrnamentGroup");


		//匂いを惑わすオブジェクトから3マス以内、かつ縦か横ドアが
		for (auto o : OG->GetGroupVector()) {
			auto o_ptr = dynamic_pointer_cast<OdorModel>(o.lock());
			auto o_Pos = o_ptr->GetComponent<Transform>()->GetPosition();
			for (auto d : DG->GetGroupVector()) {
				auto d_ptr = dynamic_pointer_cast<DoorModel>(d.lock());
				auto d_Pos = d_ptr->GetComponent<Transform>()->GetPosition();
				if (sqrt((d_Pos.x - o_Pos.x)*(d_Pos.x - o_Pos.x) + (d_Pos.z - o_Pos.z)*(d_Pos.z - o_Pos.z)) <= 3 && (o_Pos.x == d_Pos.x || o_Pos.z == d_Pos.z)) {
					for (auto s : SG->GetGroupVector()) {
						auto s_ptr = dynamic_pointer_cast<SwitchModel>(s.lock());
						auto s_Pos = s_ptr->GetComponent<Transform>()->GetPosition();
						if (d_ptr->m_DoorNum == s_ptr->m_SwitchNum) {
							auto orn = AddGameObject<Ornament>(s_Pos);
							Ornament_Decision = true;
							OrnamentG->IntoGroup(orn);
						}
					}
				}
			}
		}



		//カメラターゲットオブジェクトを配置
		auto ct = AddGameObject<CameraTarget>();
		if (PtrCamera) {
			PtrCamera->SetTargetObject(ct);
		}
		SetSharedGameObject(L"Camera_Target", ct);

	}

	//タイマーの作成
	void Tutorial::CreateTimer() {
		auto ptr2 = AddGameObject<WallSprite>(L"TIME_TX", true,
			Vector2(200, 100), Vector2(-500, 320));
		ptr2->SetDrawLayer(2);
		//ナンバースプライトの１0の位のシェアードゲードオブジェクトを作成
		auto ptr = AddGameObject<NumberSprite>(Vector3(-390, 320, 0), Vector3(100, 100, 1.0f));
		SetSharedGameObject(L"TimeNum_TenPlace", ptr);
		ptr->SetDrawLayer(2);

		//ナンバースプライトの１の位のシェアードゲードオブジェクトを作成
		ptr = AddGameObject<NumberSprite>(Vector3(-320, 320, 0), Vector3(100, 100, 1.0f));
		SetSharedGameObject(L"TimeNum_OnePlace", ptr);
		ptr->SetDrawLayer(2);

		//ナンバースプライトの１00の位のシェアードゲードオブジェクトを作成
		ptr = AddGameObject<NumberSprite>(Vector3(-250, 320, 0), Vector3(100, 100, 1.0f));
		SetSharedGameObject(L"TimeNum_HyakuPlace", ptr);
		ptr->SetDrawLayer(2);
	}

	//アイテムカウンターの作成
	void Tutorial::CreateItemCounter() {
		auto Counter = CreateSharedObjectGroup(L"ItemCounter");
		auto PBG = GetSharedObjectGroup(L"PresentBoxGroup");
		auto ptr = AddGameObject<NumberSprite>(Vector3(570, 530, 0), Vector3(100, 100, 1.0f));
		SetSharedGameObject(L"ItemCounter_All", ptr);
		All_ItemCount = PBG->size();
		ptr->SetNum(All_ItemCount);
		Counter->IntoGroup(ptr);
		ptr->SetDrawLayer(2);
		ptr = AddGameObject<NumberSprite>(Vector3(430, 530, 0), Vector3(100, 100, 1.0f));
		SetSharedGameObject(L"ItemCounter_Recovery", ptr);
		Counter->IntoGroup(ptr);
		ptr->SetDrawLayer(2);
		auto ptr2 = AddGameObject<WallSprite>(L"ITEM_C_TX", true,
			Vector2(128, 128), Vector2(330, 530));
		Counter->IntoGroup(ptr2);
		ptr2->SetDrawLayer(2);
		ptr2 = AddGameObject<WallSprite>(L"SLASH_TX", true,
			Vector2(100, 100), Vector2(500, 530));
		Counter->IntoGroup(ptr2);
		ptr2->SetDrawLayer(2);
	}

	void Tutorial::CreateDescription() {

		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		
		auto ptr2 = AddGameObject<WallSprite>(L"FRAME_TX", true,
			Vector2(640, 256), Vector2(320, -270));
		ptr2->SetDrawLayer(12);

		ptr2 = AddGameObject<WallSprite>(L"DOGFOLLOW_TX", true,
			Vector2(640, 256), Vector2(320, -270));
		ptr2->SetDrawLayer(13);
		SetSharedGameObject(L"DescriptionSprite", ptr2);

		switch (ScenePtr->GetTutorialNo())
		{
		case 0:
			ptr2->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"DOGFOLLOW_TX");
			break;
		case 1:
			ptr2->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"DOOR_TX");
			break;
		case 2:
			ptr2->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"RAFFLESIA_TX");
			break;
		/*case 3:
			ptr2->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"DOGFOLLOW_TX");
			break;*/
		}
	}

	void Tutorial::OnCreate() {
		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//プレートの作成
			CreatePlate();
			//ステージオブジェクトの作成(CSV)
			CreateStageObject();
			//タイマーの作成
			CreateTimer();
			//アイテムカウンターの作成
			CreateItemCounter();

			CreateDescription();

			wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"GameBgm.wav";
			App::GetApp()->RegisterWav(L"GAMEBGM", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Get.wav";
			App::GetApp()->RegisterWav(L"GET", strMusic);

			//オーディオの初期化
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"GAMEBGM");
			m_AudioObjectPtr->Start(L"GAMEBGM", XAUDIO2_LOOP_INFINITE, 0.2f);


		}
		catch (...) {
			throw;
		}
		auto fi2 = AddGameObject<FadeIn2>();
		SetSharedGameObject(L"Fade_In2", fi2);
		fi2->SetDrawLayer(100);
		auto ptr = AddGameObject<FadeOut>();
		SetSharedGameObject(L"Fade_Out", ptr);
		ptr->SetDrawLayer(100);

		auto ptr1 = AddGameObject<FadeOut>();
		SetSharedGameObject(L"Fade_Out2", ptr);
		ptr1->SetDrawLayer(100);

		//フェードアウト
		auto ptr2 = AddGameObject<FadeOut2>();
		SetSharedGameObject(L"Fade_Out3", ptr2);
		ptr2->SetDrawLayer(100);
	}

	void Tutorial::OnUpdate() {
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

		//説明用のスプライトを時間経過で二枚見せる

		auto sp_ptr = GetSharedGameObject<WallSprite>(L"DescriptionSprite");
		if (SpriteCounter % 120 == 0 && SpriteCounter !=0) {
			sp_flg = !sp_flg;
			switch (ScenePtr->GetTutorialNo())
			{
			case 0:
				if (sp_flg) {
					sp_ptr->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"TREASURECHEST_TX");
				}
				else {
					sp_ptr->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"DOGFOLLOW_TX");
				}
				break;
			case 1:
				if (sp_flg) {
					sp_ptr->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"SEARCH_TX");
				}
				else {
					sp_ptr->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"DOOR_TX");
				}
				break;
			case 2:
				if (sp_flg) {
					sp_ptr->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"ORNAMENT_TX");
				}
				else {
					sp_ptr->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"RAFFLESIA_TX");
				}
				break;
			}



		}

		SpriteCounter++;
		if (SpriteCounter > 120) {
			SpriteCounter = 0;
		}




		auto fi2 = GetSharedGameObject<FadeIn2>(L"Fade_In2", false);
		fi2->SetFOD_Start(true);

		auto ptr_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_OnePlace");
		ptr_Num->SetNum(GameTimer / 10);

		ptr_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_TenPlace");
		ptr_Num->SetNum(GameTimer / 100);

		ptr_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_HyakuPlace");
		ptr_Num->SetNum(GameTimer);

		if (GameTimer <= 0 && !IsPushButton) {
			auto ptr = GetSharedGameObject<FadeOut>(L"Fade_Out", false);
			ptr->SetFOB_Start(true);
			//auto ScenePtr = App::GetApp()->GetScene<Scene>();
			//PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameOverStage");
			IsPushButton = true;
		}

		ptr_Num = GetSharedGameObject<NumberSprite>(L"ItemCounter_Recovery");
		ptr_Num->SetNum(Now_ItemCount);

		if (All_ItemCount == Now_ItemCount && !IsPushButton) {
			if (GameTimer && !TimerStop) {
				ScenePtr->SetTutorialNo(ScenePtr->GetTutorialNo() + 1);
				if (ScenePtr->GetTutorialNo() <3) {
					
					TimerStop = true;
					auto ptr = GetSharedGameObject<FadeOut2>(L"Fade_Out3", false);
					ptr->SetFOD_Start(true);
				}
				else {
					auto ptr = GetSharedGameObject<FadeOut>(L"Fade_Out", false);
					ptr->SetFOW_Start(true);
				}
				//PostEvent(1.5f, GetThis<ObjectInterface>(), ScenePtr, L"ToClearStage");
				m_AudioObjectPtr->AddAudioResource(L"GET");
				m_AudioObjectPtr->Start(L"GET", 0.0f, 0.5f);
			}
			IsPushButton = false;
		}
		else {
			auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetView()->GetTargetCamera());
			if (!PtrCamera->m_Camera_RotNow) {
				if (GameTimer > 0) {
					float ElapsedTime = App::GetApp()->GetElapsedTime();
					GameTimer -= ElapsedTime;
				}
			}
		}
		auto DogSprite = GetSharedGameObject<WallSprite>(L"DogSprite");
		//auto DogSprite_Pos = DogSprite->GetComponent<Transform>()->GetPosition();
		auto Button_Push = GetSharedGameObject<WallSprite>(L"Button_Push");
		//auto Button_Push_Pos = Button_Push->GetComponent<Transform>()->GetPosition();
		auto balloon_Pos = GetSharedGameObject<WallSprite>(L"DogBalloon")->GetComponent<Transform>()->GetPosition();

		DogSprite->GetComponent<Transform>()->SetPosition(Vector3(balloon_Pos.x, balloon_Pos.y + 30, balloon_Pos.z));
		Button_Push->GetComponent<Transform>()->SetPosition(Vector3(balloon_Pos.x, balloon_Pos.y - 20, balloon_Pos.z));

		//if (Now_ItemCount >= 1 && Now_ItemCount < 2) {
		//	auto ptr = AddGameObject<WallSprite>(L"TANSAKU_TX", true, Vector2(512, 128), Vector2(20, -250));
		//	ptr->SetDrawLayer(2);

		//}

		/*if (GameTimer <= 10) {
		auto ptr = GetSharedGameObject<FadeOut>(L"Fade_Out", false);
		ptr->SetFOA_Start(true);
		}
		*/

		//時間の数字が大きくなったり小さくなったりする
		if (GameTimer < 11) {

			auto One_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_OnePlace")->GetComponent<Transform>();
			auto Ten_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_TenPlace")->GetComponent<Transform>();
			auto Hyaku_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_HyakuPlace")->GetComponent<Transform>();

			if (One_Num->GetScale().x < 90.0f) {
				Time_Scaling = 1.0f;
			}

			else if (One_Num->GetScale().x > 110.0f) {
				Time_Scaling = -1.0f;
			}
			One_Num->SetScale(Vector3(One_Num->GetScale().x + Time_Scaling, One_Num->GetScale().y + Time_Scaling, One_Num->GetScale().z));
			Ten_Num->SetScale(Vector3(One_Num->GetScale().x + Time_Scaling, One_Num->GetScale().y + Time_Scaling, One_Num->GetScale().z));
			Hyaku_Num->SetScale(Vector3(One_Num->GetScale().x + Time_Scaling, One_Num->GetScale().y + Time_Scaling, One_Num->GetScale().z));

		}


		if (CntlVec[0].bConnected) {
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START && !IsPushButton) {

				//二度と押さないようにする
				auto ptr = GetSharedGameObject<FadeOut>(L"Fade_Out", false);
				ptr->SetFOT_Start(true);
				IsPushButton = true;

			}
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_BACK && !IsPushButton) {

				//二度と押さないようにする
				auto ptr1 = GetSharedGameObject<FadeOut>(L"Fade_Out2", false);
				ptr1->SetFOS_Start(true);
				IsPushButton = true;

			}
		}

	}

	Tutorial::~Tutorial() {
		m_AudioObjectPtr->Stop(L"GAMEBGM");
	}

}