/*!
@file Dog.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"

namespace basecross {



	//--------------------------------------------------------------------------------------
	//	class Dog : public GameObject;
	//	用途: 犬
	//--------------------------------------------------------------------------------------
	class Dog : public GameObject {
		shared_ptr< StateMachine<Dog> >  m_StateMachine;	//ステートマシーン
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;

		//犬の大きさ
		Vector3 m_Scale;
		//犬の回転
		Vector3 m_Rot;
		//犬の位置
		Vector3 m_Pos;

		//犬の座標を格納
		Vector3 m_Dog_BeforePos;
		//Playerの座標を格納
		Vector3 m_Player_BeforePos;

		//移動量を格納する変数
		float m_Move_Distance = 0;

		//検索をかけたプレゼントボックス
		shared_ptr<PresentBoxModel> m_pb_storage;

		//次に向かうパスの情報を格納
		shared_ptr<RoadPath> m_rp_storage;
		

		//犬と検索したパス間が移動不可か判断
		bool m_Hindrance = false;

		//ボタン一発しかならないやつ
		bool IsPushButton;

		//モーション変更用
		bool Motion_Change = false;
		
	public:
		//構築と破棄
		Dog(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);
		virtual ~Dog() {}
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		shared_ptr< StateMachine<Dog> > GetStateMachine() const {
			return m_StateMachine;
		}

		//Playerの前のターンの座標を取得
		void SetPlayer_BeforePos(Vector3 Player_Pos);

		//Playerの前のターンの座標をVector配列に格納(この処理がうまく行けばSetPlayer_BeforePos()は消す)
		void SetPlayer_BeforePosVec(Vector3 Player_Pos);
		//Playerが移動したら犬もその後ろをついていく
		//void Player_FollowMove();

		//更新
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		//ターンの最終更新時
		virtual void OnLastUpdate() override;

		//犬固有関数群
		//一歩をスムーズにするための関数
		void Dog_Move_Now(int MD);

		//一番近くのプレゼントボックスを探す関数(中身ありの物を検索させる予定)
		void Search_Treasure();

		//犬の現在地がどのパスの座標とも一致しなかった場合、一番近くのパスの位置を検索する関数
		void Dog_SearchNearpath();

		//自分とパスの座標情報を取得し移動方向を決める(X軸とY軸を合わせる感じー)
		void DogToPath_MoveAngle();

		//自分の次に進む方向に障害物があるか判断
		void Obstacle_Decision(int angleNo);

		//犬が探索を終えた時にプレイヤーとの距離を確認する(結果によってステートが変化する)
		void State_Branch();

		//犬の状態をUIで出す(dog_stateでUIを分ける(0 = プレイヤー追従、1 = 探索、　2 = 帰還))
		int dog_state = 0;
		void state_UIChange();

		//プレゼントボックスを見つけたら帰ってくる
		bool LocatePresent = false;


		//犬固有変数群
		//犬が現在動いているかどうか
		bool m_Dog_Walk = false;

		//犬が現在動いているか(探索ステート中)
		bool m_Dog_SWalk = false;

		//プレイヤーの移動方向を管理する変数
		int m_Move_Direction = 0;

		//現在デフォルトステートだっていうbool
		bool CurrentDefault = true;

		//匂いで惑わされているか判断
		bool smell_dog = false;


		//プレイヤーのいる方向を格納
		vector<int> m_PlayerAngleHistory;

		//プレイヤーの位置を格納
		vector<Vector3> m_PlayerPosHistory;

		//命令を受けているかどうか
		bool m_Dog_OrderNow = false;

		//プレゼントボックスにをターゲットしているか
		bool m_TargetTreasure = false;

		//現在地がパスの位置かどうか
		bool m_DogOnthePath = false;

		//m_Path_Historyの中身の数を格納(という名目のカウンター)
		int m_phCounter = 0;
		int m_phCounter2 = 0;

		//目標地点をパスからプレゼントボックスに変更するフラグ
		bool m_PathToPresentBox = false;

		//DogToPath_MoveAngle()内で使用する変数、プレゼントボックス用の処理をプレイヤーに変更するため
		bool m_PresentBoxToPlayer = false;

		//プレイヤーの元に帰るときはアニメーションは歩きのまま
		bool walkanim = false;

		//パスの移動履歴を格納
		vector<shared_ptr<RoadPath>> m_Path_History;

		//移動不可能になった時に前の座標に向かうようにする
		vector<shared_ptr<RoadPath>> m_Path_History_Revert;

		//「お宝発見！」を数秒見せるためのカウンター
		float m_PresentCount = 0;

	};

	//--------------------------------------------------------------------------------------
	//	class D_DefaultState : public ObjState<Dog>;
	//	用途: 命令待ち状態
	//--------------------------------------------------------------------------------------
	class D_DefaultState : public ObjState<Dog>
	{
		D_DefaultState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<D_DefaultState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Dog>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Dog>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Dog>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class SearchState : public ObjState<Dog>;
	//	用途: 探索状態
	//--------------------------------------------------------------------------------------
	class SearchState : public ObjState<Dog>
	{
		SearchState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<SearchState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Dog>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Dog>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Dog>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class SearchState : public ObjState<Dog>;
	//	用途: プレイヤーと距離が離れている時の状態
	//--------------------------------------------------------------------------------------
	class ApproachState : public ObjState<Dog>
	{
		ApproachState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<ApproachState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Dog>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Dog>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Dog>& Obj)override;
	};



	//--------------------------------------------------------------------------------------
	//	犬の上に出るスプライト
	//--------------------------------------------------------------------------------------
	class Square : public GameObject {
		weak_ptr<GameObject> m_DogObject;
		//このオブジェクトのみで使用するスクエアメッシュ
		shared_ptr<MeshResource> m_SquareMeshResource;

		int Allocation;

		//ドラクエ的なアイテムがシュッと出てくる感じ
		bool Fly_Out = false;
	public:
		//構築と破棄
		Square(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<GameObject>& DogObject,int i);
		virtual ~Square();
		//初期化
		virtual void OnCreate() override;
		//変化
		virtual void OnUpdate() override;
	};
}
//end basecross

