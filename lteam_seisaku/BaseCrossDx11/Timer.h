#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class NumberSprite : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	class NumberSprite : public GameObject {
		Vector3 m_StartPos;
		Vector3 m_StartScale;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		float m_TotalTime;
		/*Vector3 timer_Pos;
		float timer;*/


	public:
		//構築と破棄
		NumberSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos,const Vector3& StartScale);
		virtual ~NumberSprite();
		//初期化
		virtual void OnCreate() override;
		//変化
		virtual void OnUpdate() override;

		//アクセサー
		void SetNum(float num)
		{
			m_TotalTime = num;
		}
		//アクセサー
		float GetNum()
		{
			return m_TotalTime;
		}

		//アイテムカウントがアイテム取得時にシュッと出てくるようにする
		bool itemcount_ComeOut = false;
		bool spritestop = false;

		int count = 0;

		Vector3 Initial_Pos;
		int MoveSpeed = -20;

		void setitemcount_ComeOut(bool b);
	};

	//--------------------------------------------------------------------------------------
	//	class NumberSprite : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	class SelectNumberSprite : public GameObject {
		Vector3 m_StartPos;
		Vector3 m_StartScale;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		float m_TotalTime;
		/*Vector3 timer_Pos;
		float timer;*/


	public:
		//構築と破棄
		SelectNumberSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const Vector3& StartScale);
		virtual ~SelectNumberSprite();
		//初期化
		virtual void OnCreate() override;
		//変化
		virtual void OnUpdate() override;

		//アクセサー
		void SetNum(float num)
		{
			m_TotalTime = num;
		}
		//アクセサー
		float GetNum()
		{
			return m_TotalTime;
		}
	};

}
//end basedx11