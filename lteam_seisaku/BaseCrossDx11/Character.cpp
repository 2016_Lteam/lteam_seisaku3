/*!
@file Character.cpp
@brief キャラクターなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//class MultiSpark : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiSpark::MultiSpark(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark::~MultiSpark() {}

	//初期化
	void MultiSpark::OnCreate() {
	}


	void MultiSpark::InsertSpark(const Vector3& Pos) {
		auto ParticlePtr = InsertParticle(4);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		ParticlePtr->SetMaxTime(0.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 5.0f,
				rParticleSprite.m_LocalPos.y * 5.0f,
				rParticleSprite.m_LocalPos.z * 5.0f
			);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	//--------------------------------------------------------------------------------------
	///	半透明のスプライト
	//--------------------------------------------------------------------------------------
	TraceSprite::TraceSprite(const shared_ptr<Stage>& StagePtr, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_TotalTime(0)
	{}
	TraceSprite::~TraceSprite() {}
	void TraceSprite::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列
		m_BackupVertices = {
			{ VertexPositionColor(Vector3(-HelfSize, HelfSize, 0),Color4(1.0f,0.0f,0.0f,0.0f)) },
			{ VertexPositionColor(Vector3(HelfSize, HelfSize, 0), Color4(0.0f, 1.0f, 0.0f, 0.0f)) },
			{ VertexPositionColor(Vector3(-HelfSize, -HelfSize, 0), Color4(0.0f, 0.0f, 1.0f, 0.0f)) },
			{ VertexPositionColor(Vector3(HelfSize, -HelfSize, 0), Color4(0.0f, 0.0f, 0, 0.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCSpriteDraw>(m_BackupVertices, indices);
	}
	void TraceSprite::OnUpdate() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_TotalTime += ElapsedTime;
		if (m_TotalTime >= XM_PI) {
			m_TotalTime = 0;
		}
		vector<VertexPositionColor> NewVertices;
		for (size_t i = 0; i < m_BackupVertices.size(); i++) {
			Color4 col = m_BackupVertices[i].color;
			col.w = sin(m_TotalTime);
			auto v = VertexPositionColor(
				m_BackupVertices[i].position,
				col
			);
			NewVertices.push_back(v);
		}
		auto PtrDraw = GetComponent<PCSpriteDraw>();
		PtrDraw->UpdateVertices(NewVertices);

	}


	//--------------------------------------------------------------------------------------
	///	壁模様のスプライト
	//--------------------------------------------------------------------------------------
	WallSprite::WallSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos)
	{}
	WallSprite::WallSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos, const bool& both) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_Both(both)
	{}

	WallSprite::~WallSprite() {}
	void WallSprite::OnCreate() {
		float HelfSize = 0.5f;

		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);

		Initial_Pos = PtrTransform->GetPosition();
		//頂点とインデックスを指定してスプライト作成
		if (m_Both) {
			AddComponent<PCTSpriteDraw>()->Both = true;
		}
		else {
			AddComponent<PCTSpriteDraw>();
		}
		auto PtrDraw = GetComponent<PCTSpriteDraw>();
		PtrDraw->SetWrapSampler(true);
		PtrDraw->SetTextureResource(m_TextureKey);
	}
	void WallSprite::OnUpdate() {
		auto PtrTransform = GetComponent<Transform>();
		auto PtrPos = PtrTransform->GetPosition();
		if (itemcount_ComeOut) {
			if (!spritestop) {
				PtrTransform->SetPosition(PtrPos.x, PtrPos.y + MoveSpeed, PtrPos.z);
				MoveSpeed += 1;
				PtrPos = PtrTransform->GetPosition();
				if (PtrPos.y == Initial_Pos.y - 210) {
					spritestop = true;
				}

				PtrPos = PtrTransform->GetPosition();
				if (Initial_Pos == PtrPos) {
					itemcount_ComeOut = false;
					MoveSpeed = -20;
				}
			}
			else {
				count++;
				if (count >= 60) {
					count = 0;
					spritestop = false;
				}
			}
		}

		if (UD_flg) {
			auto PtrTransform = GetComponent<Transform>();
			UDcount++;
			PtrTransform->SetPosition(Vector3(m_StartPos.x, m_StartPos.y+sin(XM_PI * 2 / 60 * UDcount)*10,0.0f));
			if (UDcount > 90) {
				UD_flg = false;
				UDcount = 0;
				PtrTransform->SetPosition(Vector3(m_StartPos.x, m_StartPos.y, 0.0f));
			}
		}

	}

	void WallSprite::setitemcount_ComeOut(bool b) {
		itemcount_ComeOut = b;
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(Initial_Pos);
	}


	//--------------------------------------------------------------------------------------
	///	スイッチのスプライト
	//--------------------------------------------------------------------------------------
	SwitchSprite::SwitchSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos)
	{}

	SwitchSprite::~SwitchSprite() {}
	void SwitchSprite::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列(縦横5個ずつ表示)
		vector<VertexPositionColorTexture> vertices = {
			{ VertexPositionColorTexture(Vector3(-HelfSize, HelfSize, 0),Color4(1.0f,1.0f,1.0f,1.0f), Vector2(0.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, HelfSize, 0), Color4(0.0f, 1.0f, 1.0f, 1.0f), Vector2(5.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(-HelfSize, -HelfSize, 0), Color4(1.0f, 0.0f, 1.0f, 1.0f), Vector2(0.0f, 5.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, -HelfSize, 0), Color4(0.0f, 0.0f, 0, 1.0f), Vector2(5.0f, 5.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrDraw->SetWrapSampler(true);
		PtrDraw->SetTextureResource(m_TextureKey);
	}



	//--------------------------------------------------------------------------------------
	///	スクロールするスプライト
	//--------------------------------------------------------------------------------------
	ScrollSprite::ScrollSprite(const shared_ptr<Stage>& StagePtr,
		const wstring& TextureKey, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_TotalTime(0)
	{}

	ScrollSprite::~ScrollSprite() {}
	void ScrollSprite::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HelfSize, HelfSize, 0), Vector2(0.0f, 0.0f)) },
			{ VertexPositionTexture(Vector3(HelfSize, HelfSize, 0), Vector2(4.0f, 0.0f)) },
			{ VertexPositionTexture(Vector3(-HelfSize, -HelfSize, 0), Vector2(0.0f, 1.0f)) },
			{ VertexPositionTexture(Vector3(HelfSize, -HelfSize, 0), Vector2(4.0f, 1.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetWrapSampler(true);
		PtrDraw->SetTextureResource(m_TextureKey);
	}
	void ScrollSprite::OnUpdate() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_TotalTime += ElapsedTime;
		if (m_TotalTime > 1.0f) {
			m_TotalTime = 0;
		}
		vector<VertexPositionTexture> NewVertices;
		for (size_t i = 0; i < m_BackupVertices.size(); i++) {
			Vector2 UV = m_BackupVertices[i].textureCoordinate;
			if (UV.x == 0.0f) {
				UV.x = m_TotalTime;
			}
			else if (UV.x == 4.0f) {
				UV.x += m_TotalTime;
			}
			auto v = VertexPositionTexture(
				m_BackupVertices[i].position,
				UV
			);
			NewVertices.push_back(v);
		}
		auto PtrDraw = GetComponent<PTSpriteDraw>();
		PtrDraw->UpdateVertices(NewVertices);
	}


	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FixedBox::FixedBox(const shared_ptr<Stage>& StagePtr, const Vector3& Scale, const Vector3& Rotation, const Vector3& Position) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	FixedBox::~FixedBox() {}

	//初期化
	void FixedBox::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 0.7f, 1.0f),
			Vector3(0.0f, 0, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
		);


		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"FENCE_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);


		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		PtrDraw->SetMeshResource(L"FENCE_MESH");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		SetAlphaActive(true);

		SetDrawLayer(1);
	}

	//--------------------------------------------------------------------------------------
	//	class TreeModel : public GameObject;
	//	用途: ツリーモデル
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TreeModel::TreeModel(const shared_ptr<Stage>& StagePtr, const Vector3& Scale, const Vector3& Rotation, const Vector3& Position) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	TreeModel::~TreeModel() {}

	//初期化
	void TreeModel::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 0.5f, 1.0f),
			Vector3(0.0f, 0, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
		);


		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"TREE_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);


		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		PtrDraw->SetMeshResource(L"TREE_MESH");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		SetAlphaActive(true);

		SetDrawLayer(1);
	}


	//--------------------------------------------------------------------------------------
	//	class PresentBoxModel : public GameObject;
	//	用途: プレゼントボックスモデル
	//--------------------------------------------------------------------------------------
	//構築と破棄
	PresentBoxModel::PresentBoxModel(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const int & boxno
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		boxNo(boxno)
	{
	}
	PresentBoxModel::~PresentBoxModel() {}

	//初期化
	void PresentBoxModel::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(4.0f, 4.0f, 4.0f),
			Vector3(0.0f, 0, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
		);


		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"BOX_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		auto PtrRegid = AddComponent<Rigidbody>();
		//重力を付ける
		auto PtrGravity = AddComponent<Gravity>();


		auto PtrDraw = AddComponent<PNTBoneModelDraw>();
		PtrDraw->SetMeshResource(L"BOX_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->AddAnimation(L"Default", 0, 15, false, 15.0f);
		PtrDraw->ChangeCurrentAnimation(L"Default");


		SetDrawLayer(1);

	}

	void PresentBoxModel::OnUpdate() {

		if (m_OpenBox) {
			auto PtrDraw = GetComponent<PNTBoneModelDraw>();
			float ElapsedTime = App::GetApp()->GetElapsedTime();

			if (PtrDraw->UpdateAnimation(ElapsedTime) && !IsPushButton) {
				auto ps = GetStage()->GetSharedObjectGroup(L"PresentBoxSpriteGroup");
				////二度と押さないようにする
				ps->at(boxNo)->SetDrawActive(true);
				IsPushButton = true;
				auto ScenePtr = App::GetApp()->GetScene<Scene>();

				if (ScenePtr->GetStageNo() == 0) {
					dynamic_pointer_cast<Tutorial>(GetStage())->Now_ItemCount++;
				}
				else {
					dynamic_pointer_cast<GameStage>(GetStage())->Now_ItemCount++;

				}
				auto ItemCount = GetStage()->GetSharedObjectGroup(L"ItemCounter");
				auto Itemcounter_A = GetStage()->GetSharedGameObject<NumberSprite>(L"ItemCounter_All");
				auto Itemcounter_R = GetStage()->GetSharedGameObject<NumberSprite>(L"ItemCounter_Recovery");
				Itemcounter_A->setitemcount_ComeOut(true);
				Itemcounter_R->setitemcount_ComeOut(true);

				for (auto v : ItemCount->GetGroupVector()) {
					auto i = v.lock();
					auto i_ptr = dynamic_pointer_cast<WallSprite>(i);
					if (i != Itemcounter_A && i != Itemcounter_R) {
						i_ptr->setitemcount_ComeOut(true);
					}
				}



			}
		}

	}

	void PresentBoxModel::Decision_OpenBox() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto DogPtr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
		auto P_Pos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto Face = GetStage()->GetSharedGameObject<DogSerif>(L"DogSerif");

		auto myPos = GetComponent<Transform>()->GetPosition();
		//プレゼントボックスの前後左右にプレイヤーか犬がいたら、プレイヤーと犬はアイテムを取得する
		for (int i = 0; i < 2; i++) {
			if (i > 0) {
				P_Pos = DogPtr->GetComponent<Transform>()->GetPosition();
			}
			if (P_Pos.x == myPos.x && P_Pos.z == myPos.z + 1 ||
				P_Pos.x == myPos.x && P_Pos.z == myPos.z - 1 ||
				P_Pos.x == myPos.x + 1 && P_Pos.z == myPos.z ||
				P_Pos.x == myPos.x - 1 && P_Pos.z == myPos.z) {
				m_OpenBox = true;
				if (i > 0 && DogPtr->dog_state==1) {
					Face->SetNum(1);
					DogPtr->LocatePresent = true;
				}
				else {
					Face->SetNum(3);
				}
				break;
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	class DoorModel : public GameObject;
	//	用途: ドアモデル
	//--------------------------------------------------------------------------------------
	//構築と破棄
	DoorModel::DoorModel(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	DoorModel::~DoorModel() {}

	//初期化
	void DoorModel::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//ゲームオブジェクトグループを呼び出す
		auto ogoPtr = GetStage()->GetSharedObjectGroup(L"Other_GameObject");
		//ゲームオブジェクトトグループ内のオブジェクトが上下左右のいずれかにあればそれに対応して
		//ドアの向きを変える

		auto d_Pos = PtrTransform->GetPosition();

		for (auto v : ogoPtr->GetGroupVector()) {
			auto goptr = v.lock();
			auto goTrans = goptr->GetComponent<Transform>();
			auto goPos = goTrans->GetPosition();

			if (((int)sqrt((d_Pos.x - goPos.x)*(d_Pos.x - goPos.x) + (d_Pos.z - goPos.z)*(d_Pos.z - goPos.z)) == 1) && d_Pos.x == goPos.x) {
				doorangle = false;
				break;
			}
			else if (((int)sqrt((d_Pos.x - goPos.x)*(d_Pos.x - goPos.x) + (d_Pos.z - goPos.z)*(d_Pos.z - goPos.z)) == 1) && d_Pos.z == goPos.z) {
				doorangle = true;
				break;
			}
		}

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列

		if (!doorangle) {
			SpanMat.DefTransformation(
				Vector3(1.0f, 1.2f, 1.0f),
				Vector3(0.0f, 0, 0.0f),
				Vector3(0.0f, 0.0f, 0.0f)
			);
		}
		else {
			SpanMat.DefTransformation(
				Vector3(1.0f, 1.2f, 1.0f),
				Vector3(0.0f, XM_PI / 2, 0.0f),
				Vector3(0.0f, 0.0f, 0.0f)
			);
		}

		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"DOOR_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		auto PtrRegid = AddComponent<Rigidbody>();
		//重力を付ける
		auto PtrGravity = AddComponent<Gravity>();


		auto PtrDraw = AddComponent<PNTBoneModelDraw>();
		PtrDraw->SetMeshResource(L"DOOR_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->AddAnimation(L"Open", 0, 25, false, 15.0f);
		PtrDraw->AddAnimation(L"Close", 0, 1, false, 15.0f);
		PtrDraw->ChangeCurrentAnimation(L"Close");


		//Cubeの衝突判定をつける
		//auto CollPtr = AddComponent<CollisionObb>();

		auto DG = GetStage()->GetSharedObjectGroup(L"DoorGroup");
		m_DoorNum = DG->size();

		SetDrawLayer(1);

		wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Tobira.wav";
		App::GetApp()->RegisterWav(L"TOBIRA", strMusic);

	}

	void DoorModel::OnUpdate() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto DogPtr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
		auto CT = GetStage()->GetSharedGameObject<CameraTarget>(L"Camera_Target");
		auto PtrDraw = GetComponent<PNTBoneModelDraw>();
		float ElapsedTime = App::GetApp()->GetElapsedTime();

		if (m_Open) {
			if (PtrDraw->UpdateAnimation(ElapsedTime)) {
				auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetStage()->GetView()->GetTargetCamera());
				if (!ChangePlayerCam) {
					CT->ChangeTarget(PlayerPtr);
					if (anglechange) {
						//PtrCamera->setm_left(true);
						//PtrCamera->setCamela_Movement_DoorOpen(false);
						anglechange = false;
					}
					ChangePlayerCam = true;
				}

			}
		}
		else {
			PtrDraw->UpdateAnimation(ElapsedTime);
		}

		if (m_Open && !m_ChangeAnimation) {
			PtrDraw->ChangeCurrentAnimation(L"Open");
			m_ChangeAnimation = true;
			//オーディオの初期化
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"TOBIRA");
			m_AudioObjectPtr->Start(L"TOBIRA", (size_t)0.0f, 0.5f);
		}
		else if (!m_Open) {
			PtrDraw->ChangeCurrentAnimation(L"Close");
			if (m_ChangeAnimation) {
				CT->ChangeTarget(PlayerPtr);
				m_ChangeAnimation = false;
				ChangePlayerCam = false;
			}
		}

	}

	//--------------------------------------------------------------------------------------
	//	class SwitchModel : public GameObject;
	//	用途: スイッチモデル
	//--------------------------------------------------------------------------------------
	//構築と破棄
	SwitchModel::SwitchModel(const shared_ptr<Stage>& StagePtr, const Vector3& Scale, const Vector3& Rotation, const Vector3& Position) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	SwitchModel::~SwitchModel() {}

	//初期化
	void SwitchModel::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto SG = GetStage()->GetSharedObjectGroup(L"SwitchGroup");
		m_SwitchNum = SG->size();

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		//PtrDraw->SetOwnShadowActive(true);
		switch (m_SwitchNum)
		{
		case 0:
			PtrDraw->SetTextureResource(L"SWITCH_TX");
			break;
		case 1:
			PtrDraw->SetTextureResource(L"SWITCH2_TX");
			break;
		case 2:
			PtrDraw->SetTextureResource(L"SWITCH3_TX");
			break;
		case 3:
			PtrDraw->SetTextureResource(L"SWITCH4_TX");
			break;
		case 4:
			PtrDraw->SetTextureResource(L"SWITCH5_TX");
			break;
		case 5:
			PtrDraw->SetTextureResource(L"SWITCH6_TX");
			break;
		}


		SetAlphaActive(true);



		SetDrawLayer(1);


	}
	void SwitchModel::OnUpdate() {
	}

	void SwitchModel::Decision_OpenDoor() {
		//犬とプレイヤーの位置情報を取得
		auto DogPtr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
		auto D_Pos = DogPtr->GetComponent<Transform>()->GetPosition();
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto P_Pos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		//スイッチ自身の位置情報も用意しておく
		auto m_Pos = GetComponent<Transform>()->GetPosition();
		//同じ数字のドアを格納するための変数
		shared_ptr<DoorModel> DoorPtr;
		auto DG = GetStage()->GetSharedObjectGroup(L"DoorGroup");
		auto CT = GetStage()->GetSharedGameObject<CameraTarget>(L"Camera_Target");
		for (auto v : DG->GetGroupVector()) {
			auto dptr = dynamic_pointer_cast<DoorModel>(v.lock());
			if (m_SwitchNum == dptr->m_DoorNum) {
				DoorPtr = dptr;
				break;
			}
		}
		//スイッチの上にプレイヤーまたは犬が乗っていれば
		auto ornPtr = GetStage()->GetSharedObjectGroup(L"OrnamentGroup");
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		if (ScenePtr->GetStageNo() == 0) {
			if (dynamic_pointer_cast<Tutorial>(GetStage())->Ornament_Decision) {
				for (auto o : ornPtr->GetGroupVector()) {
					auto optr = dynamic_pointer_cast<Ornament>(o.lock());
					auto oTrans = optr->GetComponent<Transform>();
					auto oPos = oTrans->GetPosition();
					if (((m_Pos.x == D_Pos.x) && (m_Pos.z == D_Pos.z)) || ((m_Pos.x == P_Pos.x) && (m_Pos.z == P_Pos.z)) || ((m_Pos.x == oPos.x) && (m_Pos.z == oPos.z))) {
						//対応したドアを開く
						DoorPtr->m_Open = true;
						if (!DoorPtr->m_ChangeAnimation) {
							//CT->m_Target_GO = DoorPtr;
							CT->ChangeTarget(DoorPtr);
							auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetStage()->GetView()->GetTargetCamera());
							PtrCamera->setm_RadYUp(true);
							auto C_Eye = PtrCamera->GetPlayerRot();
							if (DoorPtr->doorangle && C_Eye == 1 || C_Eye == 3) {
								//PtrCamera->setm_right(true);
								DoorPtr->anglechange = true;
							}
							else if (!DoorPtr->doorangle && C_Eye == 0 || C_Eye == 2) {
								//PtrCamera->setm_right(true);
								DoorPtr->anglechange = true;
							}
						}
					}
					else {
						DoorPtr->m_Open = false;
					}
				}
			}
			else {
				if (((m_Pos.x == D_Pos.x) && (m_Pos.z == D_Pos.z)) || ((m_Pos.x == P_Pos.x) && (m_Pos.z == P_Pos.z))) {
					//対応したドアを開く
					DoorPtr->m_Open = true;
					if (!DoorPtr->m_ChangeAnimation) {
						//CT->m_Target_GO = DoorPtr;
						CT->ChangeTarget(DoorPtr);
						auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetStage()->GetView()->GetTargetCamera());
						PtrCamera->setm_RadYUp(true);
						auto C_Eye = PtrCamera->GetPlayerRot();
						if (DoorPtr->doorangle && C_Eye == 1 || C_Eye == 3) {
							//PtrCamera->setm_right(true);
							DoorPtr->anglechange = true;
						}
						else if (!DoorPtr->doorangle && C_Eye == 0 || C_Eye == 2) {
							//PtrCamera->setm_right(true);
							DoorPtr->anglechange = true;
						}
					}
				}
				else {
					DoorPtr->m_Open = false;
				}

			}
		}
		else {
			if (dynamic_pointer_cast<GameStage>(GetStage())->Ornament_Decision) {
				for (auto o : ornPtr->GetGroupVector()) {
					auto optr = dynamic_pointer_cast<Ornament>(o.lock());
					auto oTrans = optr->GetComponent<Transform>();
					auto oPos = oTrans->GetPosition();
					if (((m_Pos.x == D_Pos.x) && (m_Pos.z == D_Pos.z)) || ((m_Pos.x == P_Pos.x) && (m_Pos.z == P_Pos.z)) || ((m_Pos.x == oPos.x) && (m_Pos.z == oPos.z))) {
						//対応したドアを開く
						DoorPtr->m_Open = true;
						if (!DoorPtr->m_ChangeAnimation) {
							//CT->m_Target_GO = DoorPtr;
							CT->ChangeTarget(DoorPtr);
							auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetStage()->GetView()->GetTargetCamera());
							PtrCamera->setm_RadYUp(true);
							auto C_Eye = PtrCamera->GetPlayerRot();
							if (DoorPtr->doorangle && C_Eye == 1 || C_Eye == 3) {
								//PtrCamera->setm_right(true);
								DoorPtr->anglechange = true;
							}
							else if (!DoorPtr->doorangle && C_Eye == 0 || C_Eye == 2) {
								//PtrCamera->setm_right(true);
								DoorPtr->anglechange = true;
							}
						}
					}
					else {
						DoorPtr->m_Open = false;
					}
				}
			}
			else {
				if (((m_Pos.x == D_Pos.x) && (m_Pos.z == D_Pos.z)) || ((m_Pos.x == P_Pos.x) && (m_Pos.z == P_Pos.z))) {
					//対応したドアを開く
					DoorPtr->m_Open = true;
					if (!DoorPtr->m_ChangeAnimation) {
						//CT->m_Target_GO = DoorPtr;
						CT->ChangeTarget(DoorPtr);
						auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetStage()->GetView()->GetTargetCamera());
						PtrCamera->setm_RadYUp(true);
						auto C_Eye = PtrCamera->GetPlayerRot();
						if (DoorPtr->doorangle && C_Eye == 1 || C_Eye == 3) {
							//PtrCamera->setm_right(true);
							DoorPtr->anglechange = true;
						}
						else if (!DoorPtr->doorangle && C_Eye == 0 || C_Eye == 2) {
							//PtrCamera->setm_right(true);
							DoorPtr->anglechange = true;
						}
					}
				}
				else {
					DoorPtr->m_Open = false;
				}

			}
		}
	}



	//--------------------------------------------------------------------------------------
	//	class RoadPath : public GameObject;
	//	用途: 経路探索用のパス
	//--------------------------------------------------------------------------------------
	RoadPath::RoadPath(const shared_ptr<Stage>& StagePtr, const Vector3& Position) :
		GameObject(StagePtr),
		m_Pos(Position)
	{}
	RoadPath::~RoadPath() {}

	void RoadPath::OnCreate() {
		AddComponent<Transform>()->SetPosition(m_Pos);
	}

	//--------------------------------------------------------------------------------------
	//	class CameraTarget : public GameObject;
	//	用途: LookAtCamera用のゲームオブジェクト
	//--------------------------------------------------------------------------------------
	CameraTarget::CameraTarget(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}
	CameraTarget::~CameraTarget() {}

	void CameraTarget::OnCreate() {
		//プレイヤーの位置情報を取得
		m_Target_GO = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto p_pos = m_Target_GO->GetComponent<Transform>()->GetPosition();
		auto p_scale = m_Target_GO->GetComponent<Transform>()->GetScale();
		auto m_Trans = GetComponent<Transform>();

		m_Trans->SetPosition(Vector3(p_pos.x, p_pos.y + p_scale.y, p_pos.z));
	}

	void CameraTarget::OnUpdate() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto DogPtr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
		auto m_Trans = GetComponent<Transform>();
		auto targetpTrans = m_Target_GO->GetComponent<Transform>();
		auto t_pos = targetpTrans->GetPosition();
		auto t_scale = targetpTrans->GetScale();

		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			//Bボタンが押された瞬間ならステージ推移
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_X) {
				if (m_Target_GO == PlayerPtr) {
					//m_Target_GO = DogPtr;
					ChangeTarget(DogPtr);
				}
				else {
					//m_Target_GO = PlayerPtr;
					ChangeTarget(PlayerPtr);

				}
			}
		}
		if (!m_CMove) {
			m_Trans->SetPosition(t_pos.x, 1.0f/* + t_scale.y*/, t_pos.z);
		}
		else {
			ChangeTargetMove();
		}
	}

	void CameraTarget::ChangeTarget(shared_ptr<GameObject> Target_Go) {
		m_Target_GO2 = m_Target_GO;
		m_Target_GO = Target_Go;
		m_CMove = true;
	}

	void CameraTarget::ChangeTargetMove() {
		//CameraObjectの情報を取得
		auto m_Trans = GetComponent<Transform>();
		auto m_Pos = m_Trans->GetPosition();
		//m_Target_GOの情報を取得
		auto targetpTrans = m_Target_GO->GetComponent<Transform>();

		mt_pos = targetpTrans->GetPosition();

		//カメラのターゲット変更時の移動速度の調節
		float Speed_Regulation = 20;

		if (mt_pos.x < m_Pos.x) {
			m_Trans->SetPosition(m_Pos.x - abs(m_Pos.x - mt_pos.x) / Speed_Regulation, m_Pos.y, m_Pos.z);
		}

		else if (mt_pos.x > m_Pos.x) {
			m_Trans->SetPosition(m_Pos.x + abs(m_Pos.x - mt_pos.x) / Speed_Regulation, m_Pos.y, m_Pos.z);
		}
		m_Pos = m_Trans->GetPosition();


		if (mt_pos.z < m_Pos.z) {
			m_Trans->SetPosition(m_Pos.x, m_Pos.y, m_Pos.z - abs(m_Pos.z - mt_pos.z) / Speed_Regulation);
		}
		else if (mt_pos.z > m_Pos.z) {
			m_Trans->SetPosition(m_Pos.x, m_Pos.y, m_Pos.z + abs(m_Pos.z - mt_pos.z) / Speed_Regulation);
		}

		if (abs(m_Pos.x - mt_pos.x) < 0.01 && abs(m_Pos.z - mt_pos.z) < 0.01) {
			m_Trans->SetPosition(mt_pos.x, 1.0f, mt_pos.z);
			m_CMove = false;
			//AssignmentPos = false;
		}
	}







	//--------------------------------------------------------------------------------------
	//	class OdorModel : public GameObject;
	//	用途:ラフレシア
	//--------------------------------------------------------------------------------------
	//構築と破棄
	OdorModel::OdorModel(const shared_ptr<Stage>& StagePtr, const Vector3& Scale, const Vector3& Rotation, const Vector3& Position) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	OdorModel::~OdorModel() {}

	//初期化
	void OdorModel::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, 0, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
		);


		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"RAF_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		auto PtrRegid = AddComponent<Rigidbody>();
		//重力を付ける
		auto PtrGravity = AddComponent<Gravity>();


		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		PtrDraw->SetMeshResource(L"RAF_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);





	}
	void OdorModel::OnUpdate() {

	}

	bool OdorModel::Return_Smell(Vector3 d_Pos) {


		auto PtrGroup = GetStage()->GetSharedObjectGroup(L"Other_GameObject");
		auto PtrDGroup = GetStage()->GetSharedObjectGroup(L"DoorGroup");

		my_Pos = GetComponent<Transform>()->GetPosition();

		if (sqrt((d_Pos.x - my_Pos.x)*(d_Pos.x - my_Pos.x) + (d_Pos.z - my_Pos.z)*(d_Pos.z - my_Pos.z)) <= 3 && (my_Pos.x == d_Pos.x || my_Pos.z == d_Pos.z)) {
			auto max = 0;
			auto min = 0;
			if (my_Pos.x == d_Pos.x) {
				max = (int)round(my_Pos.z);
				min = (int)round(d_Pos.z);
				if (my_Pos.z < d_Pos.z) {
					max = (int)round(d_Pos.z);
					min = (int)round(my_Pos.z);
				}

			}
			else if (my_Pos.z == d_Pos.z) {
				max = (int)round(my_Pos.x);
				min = (int)round(d_Pos.x);
				if (my_Pos.x < d_Pos.x) {
					max = (int)round(d_Pos.x);
					min = (int)round(my_Pos.x);
				}

			}
			for (auto v : PtrGroup->GetGroupVector()) {
				if (!m_Hindrance) {
					auto GroupLock = v.lock();
					auto GroupPos = GroupLock->GetComponent<Transform>()->GetPosition();
					if (my_Pos.x == d_Pos.x) {
						for (int i = min + 1; i < max; i++) {
							if (round(GroupPos.z) == i) {
								Smell = false;
								m_Hindrance = true;
								break;
							}
							Smell = true;
						}
					}
					else if (my_Pos.z == d_Pos.z) {
						for (int i = min + 1; i < max; i++) {
							if (round(GroupPos.x) == i) {
								Smell = false;
								m_Hindrance = true;
								break;
							}
							Smell = true;
						}
					}
				}
				else {
					m_Hindrance = false;
					break;
				}
			}

			for (auto v : PtrDGroup->GetGroupVector()) {
				auto GroupDLock = dynamic_pointer_cast<DoorModel>(v.lock());
				auto GroupDPos = GroupDLock->GetComponent<Transform>()->GetPosition();
				if (my_Pos.x == d_Pos.x) {
					for (int i = min + 1; i < max; i++) {
						if (GroupDPos.z == i) {
							if (GroupDLock->m_Open == false) {
								Smell = false;
								break;
							}
							else {
								Smell = true;
								break;
							}
						}
					}
				}
				else if (my_Pos.z == d_Pos.z) {
					for (int i = min + 1; i < max; i++) {
						if (GroupDPos.x == i) {
							if (GroupDLock->m_Open == false) {
								Smell = false;
								break;
							}
							else {
								Smell = true;
								break;
							}
						}
					}
				}

			}
		}
		else {
			Smell = false;
		}

		return Smell;
	}


	//--------------------------------------------------------------------------------------
	//	class Ornament : public GameObject;
	//	用途: 置物
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Ornament::Ornament(const shared_ptr<Stage>& StagePtr,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Pos(Position)
	{
	}
	Ornament::~Ornament() {}

	//初期化
	void Ornament::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(Vector3(1.0f, 1.0f, 1.0f));
		PtrTransform->SetRotation(Vector3(0, 0, 0));
		PtrTransform->SetPosition(m_Pos);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"PILLAR_MESH");

		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		PtrDraw->SetMeshResource(L"PILLAR_MESH");
		PtrDraw->SetOwnShadowActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class DogSerif : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	DogSerif::DogSerif(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const Vector3& StartScale) :
		GameObject(StagePtr), m_StartPos(StartPos), m_StartScale(StartScale), m_SerifNum(1) {
	}
	DogSerif::~DogSerif() {}

	//初期化
	void DogSerif::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(m_StartScale);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"Serif_TX");
		//透明処理
		SetAlphaActive(true);
		//左上原点
		//スプライトの中のメッシュからバックアップの取得
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		//各数字ごとにUV値を含む頂点データを配列化しておく
		for (size_t i = 0; i < 2; i++) {
			for (size_t j = 0; j < 6; j++) {

				//x軸
				float fromX = 0.5f * i;
				float toX = fromX + 0.5f;

				//y軸
				float fromY = 0.16667f * j;
				float toY = fromY + 0.16667f;

				vector<VertexPositionColorTexture> NumVirtex =
				{
					//左上頂点
					VertexPositionColorTexture(
						SpVertexVec[0].position,
						Color4(1.0f, 1.0f, 1.0f, 1.0f),
						Vector2(fromX, fromY)
					),
					//右上頂点
					VertexPositionColorTexture(
						SpVertexVec[1].position,
						Color4(1.0f, 1.0f, 1.0f, 1.0f),
						Vector2(toX, fromY)
					),
					//左下頂点
					VertexPositionColorTexture(
						SpVertexVec[2].position,
						Color4(1.0f, 1.0f, 1.0f, 1.0f),
						Vector2(fromX, toY)
					),
					//右下頂点
					VertexPositionColorTexture(
						SpVertexVec[3].position,
						Color4(1.0f, 1.0f, 1.0f, 1.0f),
						Vector2(toX, toY)
					),
				};
				m_NumberVertexVec.push_back(NumVirtex);
			}
		}
	}

	void DogSerif::OnUpdate() {
		////前回のターンからの時間
		size_t Num = (size_t)m_SerifNum;
		auto PtrSprite = GetComponent<PCTSpriteDraw>();
		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値が変わる頂点を設定する
		MeshRes->UpdateVirtexBuffer(m_NumberVertexVec[Num]);
		auto balloon_Pos = GetStage()->GetSharedGameObject<WallSprite>(L"DogBalloon")->GetComponent<Transform>()->GetPosition();

		GetComponent<Transform>()->SetPosition(balloon_Pos);
	}


}
//end basecross