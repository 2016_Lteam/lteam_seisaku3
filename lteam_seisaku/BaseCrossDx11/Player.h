/*!
@file Player.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"

namespace basecross {



	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	class Player : public GameObject {
		shared_ptr< StateMachine<Player> >  m_StateMachine;	//ステートマシーン
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;

		//プレイヤーの大きさ
		Vector3 m_Scale;
		//プレイヤーの回転
		Vector3 m_Rot;
		//プレイヤーの位置
		Vector3 m_Pos;

		//Playerの前のターンの座標を格納
		Vector3 m_BeforePos;

		//最初の一歩を踏み出したかフラグ
		bool m_first_Walk = false;

		//カメラの角度を取得したい変数
		float m_C_Eye = 0;

		//プレイヤーが現在動いているかどうかのフラグ
		bool m_Player_Walk = false;

		//移動量を格納する変数
		float m_Move_Distance = 0;

		//アニメーションを一度だけ変更
		bool WalkOrDefault = false;

		//置物があれば置物を押す
		bool OrnamentPush = false;
		shared_ptr<Ornament> Orn_Ptr;

		//Yボタン押してるときはお置物を引っ張る
		bool OrnamentPull = false;

		//Obstacle_Decisionは一度だけ呼ぶ
		bool Obstacle_DecisionOnce = false;

		//匂いを嗅がせるモーション中は操作できないようにする
		bool To_Smell = false;

		//ボタン一発しかならないやつ
		bool IsPushButton;

	public:
		//構築と破棄
		Player(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);
		virtual ~Player() {}
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		shared_ptr< StateMachine<Player> > GetStateMachine() const {
			return m_StateMachine;
		}
		//モーションを実装する関数群
		//移動して向きを移動方向にする
		void MoveMotion();
		//一歩をスムーズにするための関数
		void Player_Move_Now(int MD);
		//プレイヤーが前のターンから移動したか確認する関数
		void Confirmation_Pos();
		//プレイヤーが指示を出す関数
		void PlayerToDog_Order();
		//自分の次に進む方向に障害物があるか判断
		void Obstacle_Decision(int angleNo);

		//更新
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		//ターンの最終更新時
		virtual void OnLastUpdate() override;

		//プレイヤーの移動方向を管理する変数
		int m_Move_Direction = 0;

		//プレイヤーの移動速度を設定
		float Move_Speed = 0.05f;

		//犬の移動する向きを決める変数
		int m_Dog_MoveAngle = 0;

		//現在犬は命令を受けているかのフラグ
		bool m_Dog_OrderNow = false;
		
		//WalkOrDefaultの状態を返す
		bool GetWOD() {
			return WalkOrDefault;
		}
	};

	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	class DefaultState : public ObjState<Player>
	{
		DefaultState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<DefaultState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};



}
//end basecross

