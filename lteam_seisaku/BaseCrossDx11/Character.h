/*!
@file Character.h
@brief キャラクターなど
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//class MultiSpark : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	class MultiSpark : public MultiParticle {
	public:
		//構築と破棄
		MultiSpark(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark();
		//初期化
		virtual void OnCreate() override;
		void InsertSpark(const Vector3& Pos);
	};

	//--------------------------------------------------------------------------------------
	///	半透明のスプライト
	//--------------------------------------------------------------------------------------
	class TraceSprite : public GameObject {
		bool m_Trace;
		Vector2 m_StartScale;
		Vector2 m_StartPos;
		float m_TotalTime;
		//バックアップ頂点データ
		vector<VertexPositionColor> m_BackupVertices;
	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		@param[in]	StagePtr	ステージ
		@param[in]	Trace	透明処理するかどうか
		@param[in]	StartScale	初期スケール
		@param[in]	StartPos	初期位置
		*/
		//--------------------------------------------------------------------------------------
		TraceSprite(const shared_ptr<Stage>& StagePtr, bool Trace,
			const Vector2& StartScale, const Vector2& StartPos);
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~TraceSprite();
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief 更新
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate()override;
	};


	//--------------------------------------------------------------------------------------
	///	壁模様のスプライト
	//--------------------------------------------------------------------------------------
	class WallSprite : public GameObject {
		bool m_Trace;
		Vector2 m_StartScale;
		Vector2 m_StartPos;
		wstring m_TextureKey;
		bool m_Both = false;

		//アイテムカウントがアイテム取得時にシュッと出てくるようにする
		bool itemcount_ComeOut = false;
		bool spritestop = false;

		int count = 0;

		Vector3 Initial_Pos;
		int MoveSpeed = -20;


	public:
		bool UD_flg = false;
		float UDcount = 0;
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		@param[in]	StagePtr	ステージ
		@param[in]	TextureKey	テクスチャキー
		@param[in]	Trace	透明処理するかどうか
		@param[in]	StartScale	初期スケール
		@param[in]	StartPos	初期位置
		*/
		//--------------------------------------------------------------------------------------
		WallSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vector2& StartScale, const Vector2& StartPos);
		WallSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vector2& StartScale, const Vector2& StartPos, const bool& both);
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~WallSprite();
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief 更新
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate()override;

		void SetStartPos() {
			auto m_Trans = GetComponent<Transform>();
			m_Trans->SetPosition(Vector3(m_StartPos.x,m_StartPos.y,0.0f));
		}

		void Up_and_down() {
			auto balloon = GetStage()->GetSharedGameObject<WallSprite>(L"DogBalloon");
			if (!UD_flg) {
				if (this == balloon.get()) {
					UD_flg = true;
					Up_and_down();
				}
			}

		}

		//itemcount_ComeOutのアクセサ
		void setitemcount_ComeOut(bool b);
	};


	//--------------------------------------------------------------------------------------
	///	スイッチのスプライト
	//--------------------------------------------------------------------------------------
	class SwitchSprite : public GameObject {
		bool m_Trace;
		Vector2 m_StartScale;
		Vector2 m_StartPos;
		wstring m_TextureKey;
	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		@param[in]	StagePtr	ステージ
		@param[in]	TextureKey	テクスチャキー
		@param[in]	Trace	透明処理するかどうか
		@param[in]	StartScale	初期スケール
		@param[in]	StartPos	初期位置
		*/
		//--------------------------------------------------------------------------------------
		SwitchSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vector2& StartScale, const Vector2& StartPos);
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~SwitchSprite();
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief 更新
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate()override {}
	};






	//--------------------------------------------------------------------------------------
	///	スクロールするスプライト
	//--------------------------------------------------------------------------------------
	class ScrollSprite : public GameObject {
		bool m_Trace;
		Vector2 m_StartScale;
		Vector2 m_StartPos;
		wstring m_TextureKey;
		float m_TotalTime;
		//バックアップ頂点データ
		vector<VertexPositionTexture> m_BackupVertices;
	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		@param[in]	StagePtr	ステージ
		@param[in]	TextureKey	テクスチャキー
		@param[in]	Trace	透明処理するかどうか
		@param[in]	StartScale	初期スケール
		@param[in]	StartPos	初期位置
		*/
		//--------------------------------------------------------------------------------------
		ScrollSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vector2& StartScale, const Vector2& StartPos);
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~ScrollSprite();
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief 更新
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate()override;
	};






	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	class FixedBox : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		FixedBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~FixedBox();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class TreeModel : public GameObject;
	//	用途: ツリーモデル
	//--------------------------------------------------------------------------------------
	class TreeModel : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		TreeModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~TreeModel();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class PresentBoxModel : public GameObject;
	//	用途: プレゼントボックスモデル
	//--------------------------------------------------------------------------------------
	class PresentBoxModel : public GameObject {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		//ボタン一発しかならないやつ
		bool IsPushButton;
	public:
		//構築と破棄
		PresentBoxModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const int& boxno
		);
		virtual ~PresentBoxModel();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		//操作

		//プレゼントボックスが空いているかどうか
		bool m_OpenBox = false;
		//プレゼントボックスを開くかどうかの判断
		void Decision_OpenBox();

		//プレゼントボックスの番号
		int boxNo;
	};


	//--------------------------------------------------------------------------------------
	//	class DoorModel : public GameObject;
	//	用途: ドアモデル
	//--------------------------------------------------------------------------------------
	class DoorModel : public GameObject {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		DoorModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~DoorModel();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		//ドアが開いているかどうか
		bool m_Open = false;

		//ドアの番号とスイッチの番号を連動させる
		int m_DoorNum = 0;

		//アニメーションをプレイヤーか犬が乗った時一度だけ変更する
		bool m_ChangeAnimation = false;

		//アニメーションが終わった後にプレイヤーにカメラを移す
		bool ChangePlayerCam = false;

		//ドアの方向別にカメラの移動を管理する変数(true:上下(z軸を正面に),false:左右(x軸を正面に))
		bool doorangle = false;
		//ドアの方向によってカメラが回転したなら、元のカメラ角度に戻す
		bool anglechange = false;
	};


	//--------------------------------------------------------------------------------------
	//	class SwitchModel : public GameObject;
	//	用途: スイッチモデル
	//--------------------------------------------------------------------------------------
	class SwitchModel : public GameObject {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		//構築と破棄
		SwitchModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~SwitchModel();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		//ドアの番号とスイッチの番号を連動させる
		int m_SwitchNum = 0;

		//上にプレイヤーか犬が乗っているか判断
		void Decision_OpenDoor();
	};



	//--------------------------------------------------------------------------------------
	//	class RoadPath : public GameObject;
	//	用途: 経路探索用のパス
	//--------------------------------------------------------------------------------------
	class RoadPath :public GameObject {


	public:
		RoadPath(const shared_ptr<Stage>& StagePtr, const Vector3& Position);
		virtual ~RoadPath();
		virtual void OnCreate() override;

		Vector3 m_Pos;
	};

	//--------------------------------------------------------------------------------------
	//	class CameraTarget : public GameObject;
	//	用途: LookAtCamera用のゲームオブジェクト
	//--------------------------------------------------------------------------------------
	class CameraTarget :public GameObject {

	public:
		CameraTarget(const shared_ptr<Stage>& StagePtr);
		virtual ~CameraTarget();
		virtual void OnCreate();
		virtual void OnUpdate();

		//ターゲットするオブジェクトを設定
		shared_ptr<GameObject> m_Target_GO;
		Vector3 mt_pos;
		//１つ前にターゲットしていたオブジェクトを格納
		shared_ptr<GameObject> m_Target_GO2;
		//オブジェクト間のカメラ移動フラグ
		bool m_CMove = false;
		bool AssignmentPos = false;

		//ターゲットを変更する関数
		void ChangeTarget(shared_ptr<GameObject> Target_Go);
		void ChangeTargetMove();

	};

	//--------------------------------------------------------------------------------------
	//	class OdorModel : public GameObject;
	//	用途: ラフレシア
	//--------------------------------------------------------------------------------------
	class OdorModel : public GameObject {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

		bool m_Hindrance = false;
	public:
		//構築と破棄
		OdorModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~OdorModel();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		//犬が影響を受ける範囲にいるか判断
		bool Return_Smell(Vector3 d_Pos);

		Vector3 Distance;
		int m_PosX;
		int m_PosZ;

		Vector3 my_Pos;

		//障害物がなければ犬がにおいにやられる
		bool Smell = false;
	};

	//--------------------------------------------------------------------------------------
	//	class Ornament : public GameObject;
	//	用途: 置物
	//--------------------------------------------------------------------------------------
	class Ornament : public GameObject {
		Vector3 m_Pos;
	public:
		//構築と破棄
		Ornament(const shared_ptr<Stage>& StagePtr,
			const Vector3& Position
		);
		virtual ~Ornament();
		//初期化
		virtual void OnCreate() override;
		//操作
	};


	//--------------------------------------------------------------------------------------
	//	class DogSerif : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	class DogSerif : public GameObject {
		Vector3 m_StartPos;
		Vector3 m_StartScale;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		float m_SerifNum;

		float UDcount = 0;
	public:
		//構築と破棄
		DogSerif(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const Vector3& StartScale);
		virtual ~DogSerif();
		//初期化
		virtual void OnCreate() override;
		//変化
		virtual void OnUpdate() override;

		//アクセサー
		void SetNum(float num)
		{

			//・丸い吹き出し
			//0:「ほめてほめて！」
			//1:「お宝発見！」
			//2:「こっちだ！」
			//3:「探索！Yボタン！」
			//6:「わんわん♪」
			//7:「おなかすいた〜」
			//8：「おさんぽ♪」
			//9:「ちょっと疲れたなぁ」

			//・トゲトゲの吹き出し
			//4:「動けない・・・」
			//5:「くさい！！」
			//10:「助けて！！！」
			//11:「くさい・・・
			//	   もう無理・・・」
			auto balloon = GetStage()->GetSharedGameObject<WallSprite>(L"DogBalloon");
			auto Face = GetStage()->GetSharedGameObject<WallSprite>(L"DogFace");
			switch ((int)round(num)) {
			case 0:
			case 1:
			case 3:
			case 6:
			case 7:
			case 8:
				Face->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"FunDog_TX");
				balloon->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"OrdinaryBalloon_TX");
				break;
			case 2:
			case 9:
				Face->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"UsuallyDog_TX");
				balloon->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"OrdinaryBalloon_TX");
				break;
			case 4:
			case 5:
			case 10:
			case 11:
				Face->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"BadMoodDog_TX");
				balloon->GetComponent<PCTSpriteDraw>()->SetTextureResource(L"JaggedBalloon_TX");
				break;
			}
			balloon->Up_and_down();
			m_SerifNum = num;
		}

		bool UD_flg = false;

		//アクセサー
		float GetNum()
		{
			return m_SerifNum;
		}


		void SetStartPos() {
			auto m_Trans = GetComponent<Transform>();
			m_Trans->SetPosition(Vector3(m_StartPos.x, m_StartPos.y, 0.0f));

		}
	};

}
//end basecross
