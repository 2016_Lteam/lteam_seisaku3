/*!
@file Scene.cpp
@brief シーン実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	void Scene::OnCreate(){
		try {
			wstring DataDir;
			App::GetApp()->GetDataDirectory(DataDir);
			//モデル
			//プレゼントボックスモデル
			auto ModelMesh = MeshResource::CreateBoneModelMesh(DataDir, L"BOX.bmf");
			App::GetApp()->RegisterResource(L"BOX_MESH", ModelMesh);
			//ドアモデル
			ModelMesh = MeshResource::CreateBoneModelMesh(DataDir, L"Door.bmf");
			App::GetApp()->RegisterResource(L"DOOR_MESH", ModelMesh);
			//犬モデル
			ModelMesh = MeshResource::CreateBoneModelMesh(DataDir, L"Dog.bmf");
			App::GetApp()->RegisterResource(L"DOG_MESH", ModelMesh);

			//プレイヤーモデル
			ModelMesh = MeshResource::CreateBoneModelMesh(DataDir, L"Player.bmf");
			App::GetApp()->RegisterResource(L"PLAYER_MESH", ModelMesh);

			//フェンスモデル
			ModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Fence_0.bmf");
			App::GetApp()->RegisterResource(L"FENCE_MESH", ModelMesh);

			//ツリーモデル
			ModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Tree.bmf");
			App::GetApp()->RegisterResource(L"TREE_MESH", ModelMesh);

			//ラフレシアモデル
			//ツリーモデル
			ModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Rafflesia.bmf");
			App::GetApp()->RegisterResource(L"RAF_MESH", ModelMesh);

			//柱モデル
			ModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"pillar.bmf");
			App::GetApp()->RegisterResource(L"PILLAR_MESH", ModelMesh);

			//ゲームクリア
			wstring strTexture = DataDir + L"resultBG.png";
			App::GetApp()->RegisterTexture(L"RESULT_TX", strTexture);
			strTexture = DataDir + L"GameClear.png";
			App::GetApp()->RegisterTexture(L"CLEAR_TX", strTexture);
			strTexture = DataDir + L"NextStage.png";
			App::GetApp()->RegisterTexture(L"NEXTSTAGE_TX", strTexture);
			strTexture = DataDir + L"ToTitle.png";
			App::GetApp()->RegisterTexture(L"TOTITLE_TX", strTexture);
			strTexture = DataDir + L"Phopto.png";
			App::GetApp()->RegisterTexture(L"PHOTO_TX", strTexture);
			strTexture = DataDir + L"Item_Toy.png";
			App::GetApp()->RegisterTexture(L"ITEM_TOY_TX", strTexture);

			//ゲームオーバー
			strTexture = DataDir + L"GameOverBG.png";
			App::GetApp()->RegisterTexture(L"GAMEOVERBG_TX", strTexture);
			strTexture = DataDir + L"gameover.png";
			App::GetApp()->RegisterTexture(L"GAMEOVER_TX", strTexture);
			strTexture = DataDir + L"Retry.png";
			App::GetApp()->RegisterTexture(L"RETRY_TX", strTexture);
			
			//最初のアクティブステージの設定
			ResetActiveStage<TitleStage>();
		}
		catch (...) {
			throw;
		}
	}

	void Scene::OnEvent(const shared_ptr<Event>& event) {
		//ゲームステージ
		if (event->m_MsgStr == L"ToTutorial") {
			//最初のアクティブステージの設定
			ResetActiveStage<Tutorial>();
		}
		//タイトル
		else if (event->m_MsgStr == L"ToMenuStage") {
			//最初のアクティブステージの設定
			ResetActiveStage<TitleStage>();
		}
		else if (event->m_MsgStr == L"ToClearStage") {
			//ゲームクリアステージの設定
			ResetActiveStage<ClearStage>();
		}
		else if (event->m_MsgStr == L"ToSelectStage") {
			//ゲームセレクトステージの設定
			ResetActiveStage<SelectStage>();
		}
		else if (event->m_MsgStr == L"ToGameOverStage") {
			//ゲームオーバーステージの設定
			ResetActiveStage<GameOverStage>();
		}
		else if (event->m_MsgStr == L"ToGameStage") {
			//ゲームステージの設定
			ResetActiveStage<GameStage>();
		}
		
	}



}
//end basecross
