/*!
@file Scene.h
@brief シーン
*/
#pragma once

#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	class Scene : public SceneBase {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		int m_StageNo = 0;
		int TutorialNo = 0;
	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		*/
		//--------------------------------------------------------------------------------------
		Scene() :SceneBase() {}
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~Scene() {}
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;

		virtual void OnEvent(const shared_ptr<Event>& event) override;

		//ステージナンバーを格納する
		void SetStageNo(int stageNo) {
			m_StageNo = stageNo;
			if (m_StageNo > 5) {
				m_StageNo = 5;
			}
		}

		//ステージナンバーを返す
		int GetStageNo() {
			return m_StageNo;
		}

		//チュートリアルナンバーを格納する
		void SetTutorialNo(int tutorialNo) {
			TutorialNo = tutorialNo;
		}

		//チュートリアルナンバーを返す
		int GetTutorialNo() {
			return TutorialNo;
		}
	};
}

//end basecross
