/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Player::Player(const shared_ptr<Stage>& StagePtr, const Vector3& Scale, const Vector3& Rotation, const Vector3& Position) :
		GameObject(StagePtr),
		m_Scale(Scale),	//大きさ
		m_Rot(Rotation),//回転
		m_Pos(Position)	//位置
	{}

	//初期化
	void Player::OnCreate() {
		//初期位置などの設定
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(m_Scale);
		Ptr->SetRotation(m_Rot);
		Ptr->SetPosition(m_Pos);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, 0, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
		);

		////Rigidbodyをつける
		//auto PtrRedid = AddComponent<Rigidbody>();
		////反発係数は0.5（半分）
		//PtrRedid->SetReflection(0.5f);
		////重力をつける
		//auto PtrGravity = AddComponent<Gravity>();

		////最下地点
		//PtrGravity->SetBaseY(0.0f);
		//衝突判定をつける
		//auto PtrCol = AddComponent<CollisionSphere>();
		//横部分のみ反発
		//PtrCol->SetIsHitAction(IsHitAction::AutoOnObjectRepel);

		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"PLAYER_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTBoneModelDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"PLAYER_MESH");
		PtrDraw->AddAnimation(L"Walk", 0, 61, true, 60.0f);
		PtrDraw->AddAnimation(L"Default", 70, 60, true, 30.0f);
		PtrDraw->AddAnimation(L"Smell", 130, 70, false, 30.0f);	
		PtrDraw->AddAnimation(L"Push", 205, 60, true, 60.0f);
		PtrDraw->ChangeCurrentAnimation(L"Walk");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);

		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		//透明処理
		SetAlphaActive(true);
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Player> >(GetThis<Player>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->ChangeState(DefaultState::Instance());
		m_BeforePos = GetComponent<Transform>()->GetPosition();

		SetDrawLayer(1);

		wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Dog.wav";
		App::GetApp()->RegisterWav(L"DOG", strMusic);

	}


	void Player::Confirmation_Pos() {
		//プレイヤーの最初の一歩を取得する
		//if (m_first_Walk) {
			auto dogptr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
			auto dog_pos = dogptr->GetComponent<Transform>()->GetPosition();
			auto P_Pos = GetComponent<Transform>()->GetPosition();
			if (m_BeforePos.x == P_Pos.x && m_BeforePos.z == P_Pos.z) {
			}
			else {
				//dogptr->SetPlayer_BeforePos(m_BeforePos);
				if (dogptr->CurrentDefault) {
					dogptr->SetPlayer_BeforePosVec(m_BeforePos);
				}
			}
			m_BeforePos = P_Pos;
		//}
	}


	//更新
	void Player::OnUpdate() {
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();
		
		auto PtrDraw = GetComponent<PNTBoneModelDraw>();
		float ElapsedTime = App::GetApp()->GetElapsedTime();

		PtrDraw->UpdateAnimation(ElapsedTime);
		if (PtrDraw->UpdateAnimation(ElapsedTime)) {
			To_Smell = false;
			if (!OrnamentPush) {
				//PtrDraw->ChangeCurrentAnimation(L"Default");
				WalkOrDefault = true;
			}
		}
	}

	void Player::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		//スパークの放出
		auto PtrSpark = GetStage()->GetSharedGameObject<MultiSpark>(L"MultiSpark", false);
		if (PtrSpark) {
			PtrSpark->InsertSpark(GetComponent<Transform>()->GetPosition());
		}
	}
	//ターンの最終更新時
	void Player::OnLastUpdate() {
	}

	//モーションを実装する関数群
	//移動して向きを移動方向にする
	void Player::MoveMotion() {
		//カメラが一回転するのに必要な回転量
		//auto One_revolution_Camera = XM_2PI;
		auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetStage()->GetView()->GetTargetCamera());
		auto C_RadXZ = PtrCamera->GetPlayerRot();
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//if (CntlVec[0].fThumbRX != 0) {
			//float ElapsedTime = App::GetApp()->GetElapsedTime();
			//m_C_Eye += CntlVec[0].fThumbRX * ElapsedTime;
			// if (m_C_Eye > One_revolution_Camera || m_C_Eye < -One_revolution_Camera) {
			//	 m_C_Eye = 0;
			// }
		m_C_Eye = C_RadXZ;
		//カメラの回転量を把握するための表示
		//wstring str = L"CAMERAEYE\n :" + Util::FloatToWStr(C_RadXZ, 6, Util::FloatModify::Fixed);
		////文字列をつける
		//auto PtrString = GetComponent<StringSprite>();
		//PtrString->SetText(str);
   //}
   //コントローラーの十字ボタンを変数に代入しておく

		auto Dpad_Key2 = XINPUT_GAMEPAD_DPAD_UP;
		auto Dpad_Key1 = XINPUT_GAMEPAD_DPAD_DOWN;
		auto Dpad_Key4 = XINPUT_GAMEPAD_DPAD_LEFT;
		auto Dpad_Key3 = XINPUT_GAMEPAD_DPAD_RIGHT;

		//キーボードとマウスの取得
		auto Key = App::GetApp()->GetInputDevice().GetKeyState();

		auto key1 = VK_UP;
		auto key2 = VK_DOWN;
		auto key3 = VK_LEFT;
		auto key4 = VK_RIGHT;

		//プレイヤーのトランスフォーム
		auto P_Trans = GetComponent<Transform>();
		auto P_Rot = GetComponent<Transform>()->GetRotation();

		//プレイヤーが移動していなければ
		if (!m_Player_Walk) {
			if (PtrCamera->GetRotCam()) {
				if (m_C_Eye == 0) {
					P_Trans->SetRotation(P_Rot.x, 0, P_Rot.z);
					Dpad_Key1 = XINPUT_GAMEPAD_DPAD_DOWN;
					Dpad_Key2 = XINPUT_GAMEPAD_DPAD_UP;
					Dpad_Key3 = XINPUT_GAMEPAD_DPAD_RIGHT;
					Dpad_Key4 = XINPUT_GAMEPAD_DPAD_LEFT;
				}
				if (m_C_Eye == 1) {
					P_Trans->SetRotation(P_Rot.x, 3.14 / 2, P_Rot.z);
					Dpad_Key1 = XINPUT_GAMEPAD_DPAD_RIGHT;
					Dpad_Key2 = XINPUT_GAMEPAD_DPAD_LEFT;
					Dpad_Key3 = XINPUT_GAMEPAD_DPAD_UP;
					Dpad_Key4 = XINPUT_GAMEPAD_DPAD_DOWN;
				}
				if (m_C_Eye == 2) {
					P_Trans->SetRotation(P_Rot.x, 3.14f, P_Rot.z);
					Dpad_Key1 = XINPUT_GAMEPAD_DPAD_UP;
					Dpad_Key2 = XINPUT_GAMEPAD_DPAD_DOWN;
					Dpad_Key3 = XINPUT_GAMEPAD_DPAD_LEFT;
					Dpad_Key4 = XINPUT_GAMEPAD_DPAD_RIGHT;
				}
				if (m_C_Eye == 3) {
					P_Trans->SetRotation(P_Rot.x, -3.14 / 2, P_Rot.z);
					Dpad_Key1 = XINPUT_GAMEPAD_DPAD_LEFT;
					Dpad_Key2 = XINPUT_GAMEPAD_DPAD_RIGHT;
					Dpad_Key3 = XINPUT_GAMEPAD_DPAD_DOWN;
					Dpad_Key4 = XINPUT_GAMEPAD_DPAD_UP;

				}
				PtrCamera->SetRotCam(false);
			}

			P_Rot = GetComponent<Transform>()->GetRotation();
			P_Trans->SetRotation(0, P_Rot.y, 0);
			auto Dogptr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
			auto d_trans = Dogptr->GetComponent<Transform>();
			auto d_Pos = d_trans->GetPosition();
			auto p_Trans = GetComponent<Transform>();
			auto p_Pos = p_Trans->GetPosition();


			if (CntlVec[0].bConnected) {
				if (!PtrCamera->m_Camera_RotNow) {
					if(!To_Smell){
					if (CntlVec[0].fThumbLY >= 0.75f) {
						if (m_C_Eye == 0) {
							m_Move_Direction = 1;
						}
						else if (m_C_Eye == 2) {
							m_Move_Direction = 0;
						}
						else if (m_C_Eye == 1) {
							m_Move_Direction = 2;
						}
						else if (m_C_Eye == 3) {
							m_Move_Direction = 3;
						}


						if (d_Pos.x < p_Pos.x) {
							m_Dog_MoveAngle = 3;
						}
						if (d_Pos.x > p_Pos.x) {
							m_Dog_MoveAngle = 2;
						}
						if (d_Pos.z > p_Pos.z) {
							m_Dog_MoveAngle = 1;
						}
						if (d_Pos.z < p_Pos.z) {
							m_Dog_MoveAngle = 0;
						}
						m_first_Walk = true;
						m_Player_Walk = true;
						if (WalkOrDefault) {
							WalkOrDefault = false;
						}
					}
					else if (CntlVec[0].fThumbLY <= -0.75f) {
						if (m_C_Eye == 0) {
							m_Move_Direction = 0;
						}
						else if (m_C_Eye == 2) {
							m_Move_Direction = 1;
						}
						else if (m_C_Eye == 1) {
							m_Move_Direction = 3;
						}
						else if (m_C_Eye == 3) {
							m_Move_Direction = 2;
						}


						if (d_Pos.x < p_Pos.x) {
							m_Dog_MoveAngle = 3;
						}
						if (d_Pos.x > p_Pos.x) {
							m_Dog_MoveAngle = 2;
						}
						if (d_Pos.z > p_Pos.z) {
							m_Dog_MoveAngle = 1;
						}
						if (d_Pos.z < p_Pos.z) {
							m_Dog_MoveAngle = 0;
						}
						m_first_Walk = true;
						m_Player_Walk = true;
						if (WalkOrDefault) {
							WalkOrDefault = false;
						}
					}
					else if (CntlVec[0].fThumbLX <= -0.75f) {
						if (m_C_Eye == 0) {
							m_Move_Direction = 3;
						}
						else if (m_C_Eye == 2) {
							m_Move_Direction = 2;
						}
						else if (m_C_Eye == 1) {
							m_Move_Direction = 1;
						}
						else if (m_C_Eye == 3) {
							m_Move_Direction = 0;
						}


						if (d_Pos.x < p_Pos.x) {
							m_Dog_MoveAngle = 3;
						}
						if (d_Pos.x > p_Pos.x) {
							m_Dog_MoveAngle = 2;
						}
						if (d_Pos.z > p_Pos.z) {
							m_Dog_MoveAngle = 1;
						}
						if (d_Pos.z < p_Pos.z) {
							m_Dog_MoveAngle = 0;
						}
						m_first_Walk = true;
						m_Player_Walk = true;
						if (WalkOrDefault) {
							WalkOrDefault = false;
						}
					}
					else if (CntlVec[0].fThumbLX >= 0.75f) {
						if (m_C_Eye == 0) {
							m_Move_Direction = 2;
						}
						else if (m_C_Eye == 2) {
							m_Move_Direction = 3;
						}
						else if (m_C_Eye == 1) {
							m_Move_Direction = 0;
						}
						else if (m_C_Eye == 3) {
							m_Move_Direction = 1;
						}


						if (d_Pos.x < p_Pos.x) {
							m_Dog_MoveAngle = 3;
						}
						if (d_Pos.x > p_Pos.x) {
							m_Dog_MoveAngle = 2;
						}
						if (d_Pos.z > p_Pos.z) {
							m_Dog_MoveAngle = 1;
						}
						if (d_Pos.z < p_Pos.z) {
							m_Dog_MoveAngle = 0;
						}
						m_first_Walk = true;
						m_Player_Walk = true;
						if (WalkOrDefault) {
							WalkOrDefault = false;
						}
					}
					else {
						if (!WalkOrDefault) {
							auto PtrDraw = GetComponent<PNTBoneModelDraw>();
							PtrDraw->ChangeCurrentAnimation(L"Default");
							WalkOrDefault = true;
						}
					}
				}
				}

			}
			//}
		}
		//プレイヤーが移動していたら
		else if (m_Player_Walk) {
			Player_Move_Now(m_Move_Direction);
		}

	}

	void Player::Player_Move_Now(int MD) {
		//ゲームオブジェクトグループを呼び出す
		auto ogoPtr = GetStage()->GetSharedObjectGroup(L"Other_GameObject");
		//ドアグループを呼び出す
		auto dgPtr = GetStage()->GetSharedObjectGroup(L"DoorGroup");
		//プレゼントボックスグループを呼び出す
		auto pbgPtr = GetStage()->GetSharedObjectGroup(L"PresentBoxGroup");
		//スイッチグループを呼び出す
		auto sgPtr = GetStage()->GetSharedObjectGroup(L"SwitchGroup");
		//PlayerのTransformを取得する
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto p_Trans = GetComponent<Transform>();
		auto p_Pos = p_Trans->GetPosition();
		//MDに応じて移動方向を変化させる

		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			if (CntlVec[0].wButtons & XINPUT_GAMEPAD_B) {
				Move_Speed = 0.05f * 2;
			}
			else {
				Move_Speed = 0.05f;
			}
		}

		switch (MD)
		{
		case 0:
				Obstacle_Decision(0);
				p_Trans->SetRotation(0, 3.14f, 0);

			if (m_Player_Walk) {
				if (OrnamentPush) {
					auto o_Pos = Orn_Ptr->GetComponent<Transform>()->GetPosition();
					Orn_Ptr->GetComponent<Transform>()->SetPosition(Vector3(o_Pos.x, o_Pos.y, o_Pos.z + Move_Speed));
					p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z + Move_Speed));
					m_Move_Distance += Move_Speed;
				}
				else if (OrnamentPull) {
					p_Trans->SetRotation(0, 0, 0);
					auto o_Pos = Orn_Ptr->GetComponent<Transform>()->GetPosition();
					Orn_Ptr->GetComponent<Transform>()->SetPosition(Vector3(o_Pos.x, o_Pos.y, o_Pos.z + Move_Speed));
					p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z + Move_Speed));
					m_Move_Distance += Move_Speed;
				}
				else {
					p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z + Move_Speed));
					m_Move_Distance += Move_Speed;
				}
			}
			else {
				Obstacle_DecisionOnce = false;
			}
			break;
		case 1:

				Obstacle_Decision(1);
				p_Trans->SetRotation(0, 0, 0);

			if (m_Player_Walk) {
				if (OrnamentPush) {
					auto o_Pos = Orn_Ptr->GetComponent<Transform>()->GetPosition();
					Orn_Ptr->GetComponent<Transform>()->SetPosition(Vector3(o_Pos.x, o_Pos.y, o_Pos.z - Move_Speed));
					p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z - Move_Speed));
					m_Move_Distance += Move_Speed;
				}
				else if (OrnamentPull) {
					p_Trans->SetRotation(0, 3.14f, 0);
					auto o_Pos = Orn_Ptr->GetComponent<Transform>()->GetPosition();
					Orn_Ptr->GetComponent<Transform>()->SetPosition(Vector3(o_Pos.x, o_Pos.y, o_Pos.z - Move_Speed));
					p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z - Move_Speed));
					m_Move_Distance += Move_Speed;
				}
				else {
					p_Trans->SetPosition(Vector3(p_Pos.x, p_Pos.y, p_Pos.z - Move_Speed));
					m_Move_Distance += Move_Speed;
				}
			}
			else {
				Obstacle_DecisionOnce = false;
			}
			break;
		case 2:
				Obstacle_Decision(2);
				p_Trans->SetRotation(0, 3.14f / 2, 0);
			if (m_Player_Walk) {
				if (OrnamentPush) {

					auto o_Pos = Orn_Ptr->GetComponent<Transform>()->GetPosition();
					Orn_Ptr->GetComponent<Transform>()->SetPosition(Vector3(o_Pos.x - Move_Speed, o_Pos.y, o_Pos.z ));
					p_Trans->SetPosition(Vector3(p_Pos.x - Move_Speed, p_Pos.y, p_Pos.z));
					m_Move_Distance += Move_Speed;
				}
				else if (OrnamentPull) {
					p_Trans->SetRotation(0, -3.14f / 2, 0);
					auto o_Pos = Orn_Ptr->GetComponent<Transform>()->GetPosition();
					Orn_Ptr->GetComponent<Transform>()->SetPosition(Vector3(o_Pos.x - Move_Speed, o_Pos.y, o_Pos.z));
					p_Trans->SetPosition(Vector3(p_Pos.x - Move_Speed, p_Pos.y, p_Pos.z));
					m_Move_Distance += Move_Speed;
				}
				else {
					p_Trans->SetPosition(Vector3(p_Pos.x - Move_Speed, p_Pos.y, p_Pos.z));
					m_Move_Distance += Move_Speed;
				}
			}
			else {
				Obstacle_DecisionOnce = false;
			}
			break;
		case 3:
				Obstacle_Decision(3);
				p_Trans->SetRotation(0, -3.14f / 2, 0);
			if (m_Player_Walk) {
				if (OrnamentPush) {
					
					auto o_Pos = Orn_Ptr->GetComponent<Transform>()->GetPosition();
					Orn_Ptr->GetComponent<Transform>()->SetPosition(Vector3(o_Pos.x + Move_Speed, o_Pos.y, o_Pos.z));
					p_Trans->SetPosition(Vector3(p_Pos.x + Move_Speed, p_Pos.y, p_Pos.z));
					m_Move_Distance += Move_Speed;
				}
				else if (OrnamentPull) {
					p_Trans->SetRotation(0, 3.14f / 2, 0);
					auto o_Pos = Orn_Ptr->GetComponent<Transform>()->GetPosition();
					Orn_Ptr->GetComponent<Transform>()->SetPosition(Vector3(o_Pos.x + Move_Speed, o_Pos.y, o_Pos.z));
					p_Trans->SetPosition(Vector3(p_Pos.x + Move_Speed, p_Pos.y, p_Pos.z));
					m_Move_Distance += Move_Speed;
				}
				else {
					p_Trans->SetPosition(Vector3(p_Pos.x + Move_Speed, p_Pos.y, p_Pos.z));
					m_Move_Distance += Move_Speed;
				}
			}
			else {
				Obstacle_DecisionOnce = false;
			}
			break;
		}
		//m_MoveDistanceにMove_Speedを毎ターン格納する

		//m_Move_Distanceが1.0f(1マスの移動を終えたら)
		if (m_Move_Distance >= 1.0f) {
			m_Move_Distance = 0;
			p_Trans->SetPosition(Vector3(roundf(p_Pos.x), p_Pos.y, roundf(p_Pos.z)));
			for (auto p : pbgPtr->GetGroupVector()) {
				dynamic_pointer_cast<PresentBoxModel>(p.lock())->Decision_OpenBox();
			}
			for (auto s : sgPtr->GetGroupVector()) {
				dynamic_pointer_cast<SwitchModel>(s.lock())->Decision_OpenDoor();
			}
			if (Orn_Ptr!=NULL) {
				auto o_Pos = Orn_Ptr->GetComponent<Transform>()->GetPosition();
				Orn_Ptr->GetComponent<Transform>()->SetPosition(Vector3(round(o_Pos.x), o_Pos.y, round(o_Pos.z)));
			}
			//if (OrnamentPush) {
			//	WalkOrDefault = true;
			//}

			m_Player_Walk = false;
			OrnamentPush = false;
			OrnamentPull = false;
			Obstacle_DecisionOnce = false;

			auto dogptr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
			if (dogptr->m_Dog_OrderNow == false) {
				Confirmation_Pos();
			}
		}
	}

	//自分の次に進む方向に障害物があるか判断
	void Player::Obstacle_Decision(int angleNo) {
		//ゲームオブジェクトグループを呼び出す
		auto ogoPtr = GetStage()->GetSharedObjectGroup(L"Other_GameObject");
		//ドアグループを呼び出す
		auto dgPtr = GetStage()->GetSharedObjectGroup(L"DoorGroup");

		//置物グループを呼び出す
		auto ornPtr = GetStage()->GetSharedObjectGroup(L"OrnamentGroup");
		//PlayerのTransformを取得する
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto p_Trans = GetComponent<Transform>();
		auto p_Pos = p_Trans->GetPosition();

		auto PtrDraw = GetComponent<PNTBoneModelDraw>();

		int x = 0;
		int z = 0;
		switch (angleNo) {
		case 0:
			z = 1;
			break;
		case 1:
			z = -1;
			break;
		case 2:
			x = -1;
			break;
		case 3:
			x = 1;
			break;
		}
		for (auto v : ogoPtr->GetGroupVector()) {
			auto goptr = v.lock();
			auto goTrans = goptr->GetComponent<Transform>();
			auto goPos = goTrans->GetPosition();
			if (goPos.x == p_Pos.x +x&&goPos.z == p_Pos.z + z) {
				m_Player_Walk = false;
				break;
			}
		}
		if (m_Player_Walk) {
			for (auto d : dgPtr->GetGroupVector()) {
				auto dptr = dynamic_pointer_cast<DoorModel>(d.lock());
				auto dTrans = dptr->GetComponent<Transform>();
				auto dPos = dTrans->GetPosition();
				if (dPos.x == p_Pos.x+x&&dPos.z == p_Pos.z + z && dptr->m_Open == false) {
					m_Player_Walk = false;
					break;
				}
			}
			if (!Obstacle_DecisionOnce) {
				for (auto o : ornPtr->GetGroupVector()) {
					auto optr = dynamic_pointer_cast<Ornament>(o.lock());
					auto oTrans = optr->GetComponent<Transform>();
					auto oPos = oTrans->GetPosition();
					if (round(oPos.x) == round(p_Pos.x) + x&& round(oPos.z) == round(p_Pos.z) + z) {
						for (auto v : ogoPtr->GetGroupVector()) {
							auto goptr = v.lock();
							auto goTrans = goptr->GetComponent<Transform>();
							auto goPos = goTrans->GetPosition();
							if (goPos.x == oPos.x + x&&goPos.z == oPos.z + z) {
								m_Player_Walk = false;
								break;
							}
						}
						for (auto d : dgPtr->GetGroupVector()) {
							auto dptr = dynamic_pointer_cast<DoorModel>(d.lock());
							auto dTrans = dptr->GetComponent<Transform>();
							auto dPos = dTrans->GetPosition();
							if (dPos.x == oPos.x + x&&dPos.z == oPos.z + z && dptr->m_Open == false) {
								m_Player_Walk = false;
								break;
							}
						}
						if (m_Player_Walk) {
							Orn_Ptr = optr;
							OrnamentPush = true;
							if (PtrDraw->GetCurrentAnimation() != L"Push"){
							PtrDraw->ChangeCurrentAnimation(L"Push");
						}
							break;
						}
					}
				}


				//障害物を引っ張る
				auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

				if (!OrnamentPush) {
					if (CntlVec[0].bConnected) {
						if (CntlVec[0].wButtons & XINPUT_GAMEPAD_A) {
							for (auto o : ornPtr->GetGroupVector()) {
								auto optr = dynamic_pointer_cast<Ornament>(o.lock());
								auto oTrans = optr->GetComponent<Transform>();
								auto oPos = oTrans->GetPosition();
								if (round(oPos.x) == round(p_Pos.x) - x&& round(oPos.z) == round(p_Pos.z) - z) {
									//for (auto v : ogoPtr->GetGroupVector()) {
									//	auto goptr = v.lock();
									//	auto goTrans = goptr->GetComponent<Transform>();
									//	auto goPos = goTrans->GetPosition();
									//	if (goPos.x == oPos.x - x&&goPos.z == oPos.z - z) {
									//		m_Player_Walk = false;
									//		break;
									//	}
									//}
									//for (auto d : dgPtr->GetGroupVector()) {
									//	auto dptr = dynamic_pointer_cast<DoorModel>(d.lock());
									//	auto dTrans = dptr->GetComponent<Transform>();
									//	auto dPos = dTrans->GetPosition();
									//	if (dPos.x == oPos.x - x&&dPos.z == oPos.z - z && dptr->m_Open == false) {
									//		m_Player_Walk = false;
									//		break;
									//	}
									//}
									if (m_Player_Walk) {
										Orn_Ptr = optr;
										OrnamentPull = true;
										if (PtrDraw->GetCurrentAnimation() != L"Push") {
											PtrDraw->ChangeCurrentAnimation(L"Push");
										}
										break;
									}
								}
							}
						}
					}
				}

				if (!OrnamentPush && !OrnamentPull) {
					if (PtrDraw->GetCurrentAnimation() != L"Walk") {
						PtrDraw->ChangeCurrentAnimation(L"Walk");
					}
				}

			}
			//else {
			//	for (auto o : ornPtr->GetGroupVector()) {
			//		auto optr = dynamic_pointer_cast<Ornament>(o.lock());
			//		auto oTrans = optr->GetComponent<Transform>();
			//		auto oPos = oTrans->GetPosition();
			//		if (round(oPos.x) == round(p_Pos.x) + x&& round(oPos.z) == round(p_Pos.z) + z) {
			//			for (auto v : ogoPtr->GetGroupVector()) {
			//				auto goptr = v.lock();
			//				auto goTrans = goptr->GetComponent<Transform>();
			//				auto goPos = goTrans->GetPosition();
			//				if (goPos.x == oPos.x + x&&goPos.z == oPos.z + z) {
			//					m_Player_Walk = false;
			//					break;
			//				}
			//			}
			//			for (auto d : dgPtr->GetGroupVector()) {
			//				auto dptr = dynamic_pointer_cast<DoorModel>(d.lock());
			//				auto dTrans = dptr->GetComponent<Transform>();
			//				auto dPos = dTrans->GetPosition();
			//				if (dPos.x == oPos.x + x&&dPos.z == oPos.z + z && dptr->m_Open == false) {
			//					m_Player_Walk = false;
			//					break;
			//				}
			//			}
			//		}
			//	}
			//}

		}
		Obstacle_DecisionOnce = true;
	}

	void Player::PlayerToDog_Order() {
		auto ptr_Num = GetStage()->GetSharedGameObject<NumberSprite>(L"ItemCounter_Recovery");

		auto dogptr = GetStage()->GetSharedGameObject<Dog>(L"Dog");
		auto d_Trans = dogptr->GetComponent<Transform>();
		auto d_Pos = d_Trans->GetPosition();

		auto p_Trans = GetComponent<Transform>();
		auto Pos = p_Trans->GetPosition();
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (!dogptr->m_Dog_Walk) {
			if (CntlVec[0].bConnected) {
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_Y)
				{
					if (ptr_Num->GetNum() > 0) {
						if (!dogptr->m_Dog_OrderNow) {
							if (dogptr->dog_state == 0) {
								dogptr->m_Dog_OrderNow = true;
								//m_Dog_OrderNow = true;
								auto PtrDraw = GetComponent<PNTBoneModelDraw>();
								//犬のいる方向に向く
								if (round(d_Pos.x) > round(Pos.x)) {
									p_Trans->SetRotation(0, -3.14f / 2, 0);
								}
								else if(round(d_Pos.x) < round(Pos.x)){
									p_Trans->SetRotation(0, 3.14f / 2, 0);

								}
								if (round(d_Pos.z) > round(Pos.z)) {
									p_Trans->SetRotation(0, 3.14f, 0);

								}
								else if(round(d_Pos.z) < round(Pos.z)){
									p_Trans->SetRotation(0, 0, 0);

								}
								PtrDraw->ChangeCurrentAnimation(L"Smell");
								To_Smell = true;
								//オーディオの初期化
								m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
								m_AudioObjectPtr->AddAudioResource(L"DOG");
								m_AudioObjectPtr->Start(L"DOG", 0.1f);
							}
						}
						else {
							dogptr->m_Dog_OrderNow = false;
							//m_Dog_OrderNow = false;
						}
					}
				}
			}
		}
	}


	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<DefaultState> DefaultState::Instance() {
		static shared_ptr<DefaultState> instance;
		if (!instance) {
			instance = shared_ptr<DefaultState>(new DefaultState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void DefaultState::Enter(const shared_ptr<Player>& Obj) {
		
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void DefaultState::Execute(const shared_ptr<Player>& Obj) {
		Obj->MoveMotion();
			Obj->PlayerToDog_Order();
	}
	//ステートにから抜けるときに呼ばれる関数
	void DefaultState::Exit(const shared_ptr<Player>& Obj) {
		//何もしない
	}



}
//end basecross

