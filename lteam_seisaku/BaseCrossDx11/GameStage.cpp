/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	void TitleStage::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"trace.png";
		strTexture = DataDir + L"Title.png";
		App::GetApp()->RegisterTexture(L"TITLE_TX", strTexture);
		strTexture = DataDir + L"TitleLogo.png";
		App::GetApp()->RegisterTexture(L"TITLELOGO_TX", strTexture);
		strTexture = DataDir + L"Press.png";
		App::GetApp()->RegisterTexture(L"PRESS_TX", strTexture);
		strTexture = DataDir + L"Tansaku.png";
		App::GetApp()->RegisterTexture(L"TANSAKU_TX", strTexture);
	}

	//ビューとライトの作成
	void TitleStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}

	//タイトルのスプライト作成
	void TitleStage::CreateWallSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<WallSprite>(L"TITLE_TX", false,
			Vector2(w, h), Vector2(0, 0));

		AddGameObject<WallSprite>(L"TITLELOGO_TX", true,
			Vector2(1024, 384), Vector2(0, 150));

		auto ptr = AddGameObject<WallSprite>(L"PRESS_TX", true,
		Vector2(640, 128), Vector2(20, -220));
		SetSharedGameObject(L"Press_Button",ptr);
	}

	//スイッチスプライト作成
	void TitleStage::CreateSwitchSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth()) / 2.0f;
		float h = static_cast<float>(App::GetApp()->GetGameHeight()) / 2.0f;

		AddGameObject<WallSprite>(L"SWITCH_TX", false,
			Vector2(w, h), Vector2(0, 0));
	}


	//スクロールするスプライト作成
	void TitleStage::CreateScrollSprite() {
		/*AddGameObject<ScrollSprite>(L"TRACE_TX", true,
		Vector2(512.0f, 128.0f), Vector2(0, 0));*/
	}


	void TitleStage::OnCreate() {
		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//壁模様のスプライト作成
			CreateWallSprite();
			//スクロールするスプライト作成
			CreateScrollSprite();
			////スイッチのスプライト作成
			//CreateSwitchSprite();
			wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Title.wav";
			App::GetApp()->RegisterWav(L"TITLE", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"TitleButton.wav";
			App::GetApp()->RegisterWav(L"TB", strMusic);

			//オーディオの初期化
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"TITLE");
			m_AudioObjectPtr->Start(L"TITLE", XAUDIO2_LOOP_INFINITE, 0.2f);
		}
		catch (...) {
			throw;
		}
		auto ptr = AddGameObject<FadeOut>();
		SetSharedGameObject(L"Fade_Out", ptr);
		ptr->SetDrawLayer(2);
		//フェードイン
		auto fi = AddGameObject<FadeIn3>();
		SetSharedGameObject(L"Fade_In3", fi);
		fi->SetDrawLayer(2);
	}

	//更新
	void TitleStage::OnUpdate() {
		auto fi = GetSharedGameObject<FadeIn3>(L"Fade_In3", false);
		fi->SetFOT_Start(true);
		
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (fi->GetComponent<PCTSpriteDraw>()->GetEmissive().w >0) {
			if (CntlVec[0].bConnected) {
				//Bボタンが押された瞬間ならステージ推移
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
					fi->SetinvisibleZero();
				}
			}
		}
		else {
			if (CntlVec[0].bConnected) {
				//Bボタンが押された瞬間ならステージ推移
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !IsPushButton) {
					//auto ScenePtr = App::GetApp()->GetScene<Scene>();
					m_AudioObjectPtr->AddAudioResource(L"TB");
					m_AudioObjectPtr->Start(L"TB", 0.0f, 0.5f);
					//PostEvent(1.0f, GetThis<ObjectInterface>(), ScenePtr, L"TosStage");
					auto ptr = GetSharedGameObject<FadeOut>(L"Fade_Out", false);
					ptr->SetFOS_Start(true);
					//二度と押さないようにする
					IsPushButton = true;
				}
			}
		}

		auto pbptr = GetSharedGameObject<WallSprite>(L"Press_Button");
		auto ElappsedTime = App::GetApp()->GetElapsedTime();
		tou += ElappsedTime * 3;

		pbptr->GetComponent<PCTSpriteDraw>()->SetDiffuse(Color4(1, 1, 1, 1-sin(tou)));
	}

	TitleStage :: ~TitleStage() {
		//オーディオSTOP
		m_AudioObjectPtr->Stop(L"TITLE");
	}




	//--------------------------------------------------------------------------------------
	//	セレクトステージクラス実体
	//--------------------------------------------------------------------------------------
	void SelectStage::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"Stageselect.png";
		App::GetApp()->RegisterTexture(L"SELECTSTAGE_TX", strTexture);
		strTexture = DataDir + L"select.png";
		App::GetApp()->RegisterTexture(L"SELECT_TX", strTexture);
		strTexture = DataDir + L"Stage.png";
		App::GetApp()->RegisterTexture(L"STAGE_TX", strTexture);
		strTexture = DataDir + L"Stageselectnumber.png";
		App::GetApp()->RegisterTexture(L"SELECT_NUMBER_TX", strTexture);
		strTexture = DataDir + L"tutorial.png";
		App::GetApp()->RegisterTexture(L"TUTORIAL_TX", strTexture);
		strTexture = DataDir + L"icon001.png";
		App::GetApp()->RegisterTexture(L"ICON01_TX", strTexture);
		strTexture = DataDir + L"icon002.png";
		App::GetApp()->RegisterTexture(L"ICON02_TX", strTexture);
	}

	//ビューとライトの作成
	void SelectStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}

	//セレクトのスプライト作成
	void SelectStage::CreateWallSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<WallSprite>(L"SELECTSTAGE_TX", false,
			Vector2(w, h), Vector2(0, 0));

		AddGameObject<WallSprite>(L"SELECT_TX", true,
			Vector2(640, 128), Vector2(0, 300));

		auto sp = CreateSharedObjectGroup(L"SelectPlateGroup");
		auto spn = CreateSharedObjectGroup(L"SelectPlateNumberGroup");
		auto GandD = CreateSharedObjectGroup(L"GirlAndDog");

		//女の子と犬のイラストを左右に配置する
		auto s = AddGameObject<WallSprite>(L"ICON01_TX", true,
			Vector2(192, 192), Vector2(-200, 150));
		GandD->IntoGroup(s);

		s = AddGameObject<WallSprite>(L"ICON02_TX", true,
			Vector2(192, 192), Vector2(250, 150));
		GandD->IntoGroup(s);
		
		//ステージ + 数字の組み合わせを表示する
		s = AddGameObject<WallSprite>(L"TUTORIAL_TX", true,
			Vector2(320, 128), Vector2(10, 200));
		auto s_Pos = s->GetComponent<Transform>()->GetPosition();
		auto ptr = AddGameObject<SelectNumberSprite>(Vector3(s_Pos.x, s_Pos.y, 0), Vector3(128, 128, 1.0f));
		ptr->SetNum(0);
		ptr->SetDrawActive(false);
		spn->IntoGroup(ptr);
		sp->IntoGroup(s);

		s = AddGameObject<WallSprite>(L"STAGE_TX", true,
			Vector2(256, 128), Vector2(-50, 90));
		s_Pos = s->GetComponent<Transform>()->GetPosition();
		ptr = AddGameObject<SelectNumberSprite>(Vector3(s_Pos.x + 200, s_Pos.y, 0), Vector3(128, 128, 1.0f));
		ptr->SetNum(1);
		spn->IntoGroup(ptr);
		sp->IntoGroup(s);

		s = AddGameObject<WallSprite>(L"STAGE_TX", true,
			Vector2(256, 128), Vector2(-50, -20));
		s_Pos = s->GetComponent<Transform>()->GetPosition();
		ptr = AddGameObject<SelectNumberSprite>(Vector3(s_Pos.x + 200, s_Pos.y, 0), Vector3(128, 128, 1.0f));
		ptr->SetNum(2);
		spn->IntoGroup(ptr);
		sp->IntoGroup(s);

		s = AddGameObject<WallSprite>(L"STAGE_TX", true,
			Vector2(256, 128), Vector2(-50, -130));
		s_Pos = s->GetComponent<Transform>()->GetPosition();
		ptr = AddGameObject<SelectNumberSprite>(Vector3(s_Pos.x + 200, s_Pos.y, 0), Vector3(128, 128, 1.0f));
		ptr->SetNum(3);
		spn->IntoGroup(ptr);
		sp->IntoGroup(s);

		s = AddGameObject<WallSprite>(L"STAGE_TX", true,
			Vector2(256, 128), Vector2(-50, -240));
		s_Pos = s->GetComponent<Transform>()->GetPosition();
		ptr = AddGameObject<SelectNumberSprite>(Vector3(s_Pos.x + 200, s_Pos.y, 0), Vector3(128, 128, 1.0f));
		ptr->SetNum(4);
		spn->IntoGroup(ptr);
		sp->IntoGroup(s);

		//s = AddGameObject<WallSprite>(L"STAGE_TX", true,
		//	Vector2(256, 128), Vector2(-50, -350));
		//s_Pos = s->GetComponent<Transform>()->GetPosition();
		//ptr = AddGameObject<SelectNumberSprite>(Vector3(s_Pos.x + 200, s_Pos.y, 0), Vector3(128, 128, 1.0f));
		//ptr->SetNum(5);
		//spn->IntoGroup(ptr);
		//sp->IntoGroup(s);

	}


	void SelectStage::OnCreate() {
		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//壁模様のスプライト作成
			CreateWallSprite();
			wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"selectBG.wav";
			App::GetApp()->RegisterWav(L"SELECTBG", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Dog01.wav";
			App::GetApp()->RegisterWav(L"DOG01", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Select.wav";
			App::GetApp()->RegisterWav(L"SELECT", strMusic);
			//オーディオの初期化
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"SELECTBG");
			m_AudioObjectPtr->Start(L"SELECTBG", XAUDIO2_LOOP_INFINITE, 0.3f);
		}
		catch (...) {
			throw;
		}
		//フェードアウト
		auto ptr2 = AddGameObject<FadeOut2>();
		SetSharedGameObject(L"Fade_Out", ptr2);
		ptr2->SetDrawLayer(2);
		//フェードイン
		auto fi = AddGameObject<FadeIn>();
		SetSharedGameObject(L"Fade_In", fi);
		fi->SetDrawLayer(2);

		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		ScenePtr->SetTutorialNo(0);
	}

	//更新
	void SelectStage::OnUpdate() {
		auto fi = GetSharedGameObject<FadeIn>(L"Fade_In", false);
		fi->SetFOW_Start(true);

		auto sp = GetSharedObjectGroup(L"SelectPlateGroup");
		auto spn = GetSharedObjectGroup(L"SelectPlateNumberGroup");
		auto GandD = GetSharedObjectGroup(L"GirlAndDog");
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			for (auto i = 0; i < sp->size(); i++)
			{
				auto spn_Pos = spn->at(i)->GetComponent<Transform>()->GetPosition();
				if (i == StageSelectNo) {
					//sp->at(i)->GetComponent<Transform>()->SetScale(Vector3(256*1.3f, 128 * 1.3f, 1.0f));
					//spn->at(i)->GetComponent<Transform>()->SetScale(Vector3(128 * 1.3f, 128 * 1.3f, 1.0f));
					//spn->at(i)->GetComponent<Transform>()->SetPosition(Vector3(200, spn_Pos.y, spn_Pos.z));
					for (auto v : GandD->GetGroupVector()) {
						auto GD_Trans = v.lock()->GetComponent<Transform>();
						auto GD_Pos = GD_Trans->GetPosition();

						GD_Trans->SetPosition(Vector3(GD_Pos.x, spn_Pos.y, GD_Pos.z));
					}
					continue;
				}
				if (i == 0) {
					sp->at(i)->GetComponent<Transform>()->SetScale(Vector3(320, 128, 1.0f));
				}
				else {
					sp->at(i)->GetComponent<Transform>()->SetScale(Vector3(256, 128, 1.0f));
					spn->at(i)->GetComponent<Transform>()->SetScale(Vector3(128, 128, 1.0f));
					spn->at(i)->GetComponent<Transform>()->SetPosition(Vector3(150, spn_Pos.y, spn_Pos.z));
				}
			}

			
			if (CntlVec[0].fThumbLY >= 0.7f) {
				if (!OnePushSelect) {
					StageSelectNo--;
					if (StageSelectNo < 0) {
						StageSelectNo = sp->size() - 1;
					}
					OnePushSelect = true;
					//オーディオの初期化
					m_AudioObjectPtr->AddAudioResource(L"SELECT");
					m_AudioObjectPtr->Start(L"SELECT", 0,0.3f);
				}
						
			}
				else if (CntlVec[0].fThumbLY <= -0.7f) {
					if (!OnePushSelect) {
						StageSelectNo++;
						if (StageSelectNo > sp->size()-1) {
							StageSelectNo = 0;
						}
						OnePushSelect = true;
						//オーディオの初期化
						m_AudioObjectPtr->AddAudioResource(L"SELECT");
						m_AudioObjectPtr->Start(L"SELECT",0, 0.3f);
					}
								
				}
				else {
					OnePushSelect = false;
				}

			//Bボタンが押された瞬間ならステージ推移
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !IsPushButton) {
				auto ptr = GetSharedGameObject<FadeOut2>(L"Fade_Out", false);
				ptr->SetFOD_Start(true);
				//auto ScenePtr = App::GetApp()->GetScene<Scene>();
				switch (StageSelectNo)
				{
				case 0:
					App::GetApp()->GetScene<Scene>()->SetStageNo(0);
					break;
				case 1:
					App::GetApp()->GetScene<Scene>()->SetStageNo(1);
					break;
				case 2:
					App::GetApp()->GetScene<Scene>()->SetStageNo(2);
					break;
				case 3:
					App::GetApp()->GetScene<Scene>()->SetStageNo(3);
					break;
				case 4:
					App::GetApp()->GetScene<Scene>()->SetStageNo(4);
					break;
				case 5:
					App::GetApp()->GetScene<Scene>()->SetStageNo(5);
					break;
				}
				//二度と押さないようにする
				IsPushButton = true;
				//オーディオの初期化
				m_AudioObjectPtr->AddAudioResource(L"DOG01");
				m_AudioObjectPtr->Start(L"DOG01", 0.5f);
			}
		}
	}

	SelectStage :: ~SelectStage() {
		//オーディオSTOP
		m_AudioObjectPtr->Stop(L"SELECTBG");
	}


	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------


	//リソースの作成
	void GameStage::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		//wstring strTexture = DataDir + L"trace.png";
		//App::GetApp()->RegisterTexture(L"TRACE_TX", strTexture);
		wstring strTexture = DataDir + L"Floor2.png";
		App::GetApp()->RegisterTexture(L"FLOOR_TX", strTexture);
		strTexture = DataDir + L"spark.png";
		App::GetApp()->RegisterTexture(L"SPARK_TX", strTexture);
		strTexture = DataDir + L"number.png";
		App::GetApp()->RegisterTexture(L"NUMBER_TX", strTexture);
		strTexture = DataDir + L"Wall.png";
		App::GetApp()->RegisterTexture(L"WALL_TX", strTexture);
		strTexture = DataDir + L"DogCamera.png";
		App::GetApp()->RegisterTexture(L"DOGCAMERA_TX", strTexture);
		strTexture = DataDir + L"DogColor.png";
		App::GetApp()->RegisterTexture(L"DOG_COLOR_TX", strTexture);
		strTexture = DataDir + L"switch_Door.png";
		App::GetApp()->RegisterTexture(L"SWITCH_TX", strTexture);
		strTexture = DataDir + L"switch_Door2.png";
		App::GetApp()->RegisterTexture(L"SWITCH2_TX", strTexture);
		strTexture = DataDir + L"switch_Door3.png";
		App::GetApp()->RegisterTexture(L"SWITCH3_TX", strTexture);
		strTexture = DataDir + L"switch_Door4.png";
		App::GetApp()->RegisterTexture(L"SWITCH4_TX", strTexture);
		strTexture = DataDir + L"switch_Door5.png";
		App::GetApp()->RegisterTexture(L"SWITCH5_TX", strTexture);
		strTexture = DataDir + L"switch_Door6.png";
		App::GetApp()->RegisterTexture(L"SWITCH6_TX", strTexture);
		strTexture = DataDir + L"BG.png";
		App::GetApp()->RegisterTexture(L"BACKGROUND_TX", strTexture);
		strTexture = DataDir + L"number.png";
		App::GetApp()->RegisterTexture(L"NUMBER_TX", strTexture);
		strTexture = DataDir + L"Time.png";
		App::GetApp()->RegisterTexture(L"TIME_TX", strTexture);
		strTexture = DataDir + L"Slash.png";
		App::GetApp()->RegisterTexture(L"SLASH_TX", strTexture);
		strTexture = DataDir + L"Item_C.png";
		App::GetApp()->RegisterTexture(L"ITEM_C_TX", strTexture);
		strTexture = DataDir + L"Item_Photo.png";
		App::GetApp()->RegisterTexture(L"ITEM_PHOTO_TX", strTexture); 
		strTexture = DataDir + L"Black.png";
		App::GetApp()->RegisterTexture(L"BLACK_TX", strTexture);

		//==================================================================
		//チュートリアルの説明
		strTexture = DataDir + L"DogFollow.png";
		App::GetApp()->RegisterTexture(L"DOGFOLLOW_TX", strTexture);

		//==================================================================
		//・左下の犬の顔
		//普通の顔
		strTexture = DataDir + L"UsuallyDog.png";
		App::GetApp()->RegisterTexture(L"UsuallyDog_TX", strTexture);
		strTexture = DataDir + L"FunDog.png";
		App::GetApp()->RegisterTexture(L"FunDog_TX", strTexture);
		strTexture = DataDir + L"BadMoodDog.png";
		App::GetApp()->RegisterTexture(L"BadMoodDog_TX", strTexture);
		//・犬の吹き出し
		strTexture = DataDir + L"OrdinaryBalloon.png";
		App::GetApp()->RegisterTexture(L"OrdinaryBalloon_TX", strTexture);
		strTexture = DataDir + L"JaggedBalloon.png";
		App::GetApp()->RegisterTexture(L"JaggedBalloon_TX", strTexture);
		//・犬のセリフ
		strTexture = DataDir + L"Serif.png";
		App::GetApp()->RegisterTexture(L"Serif_TX", strTexture);
		//ボタンの横の「ボタンを押して」っていう表示
		strTexture = DataDir + L"Button_Push.png";
		App::GetApp()->RegisterTexture(L"Button_Push_TX", strTexture);
		//==================================================================

		strTexture = DataDir + L"Odor.png";
		App::GetApp()->RegisterTexture(L"ODOR_TX", strTexture);

	}



	//ビューとライトの作成
	void GameStage::CreateViewLight() {

		//auto PtrView = CreateView<MultiView>();
		////メインビューのビューポートとカメラの設定
		//auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera2>();
		//PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		//PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//Viewport MainViewport;
		//MainViewport.Width = static_cast<float>(App::GetApp()->GetGameWidth());
		//MainViewport.Height = static_cast<float>(App::GetApp()->GetGameHeight());
		//MainViewport.TopLeftX = 0;
		//MainViewport.TopLeftY = 0;
		//MainViewport.MinDepth = 0.5f;
		//MainViewport.MaxDepth = 1.0f;
		////比率が変わるのでカメラにビューポートを伝える
		//PtrLookAtCamera->SetViewPort(MainViewport);
		//PtrView->AddView(MainViewport, PtrLookAtCamera);

		////サブ(固定カメラ)
		//PtrSubCamera = ObjectFactory::Create<LookAtCamera3>();
		//PtrSubCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		//PtrSubCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//Viewport SubViewport;
		//SubViewport.Width = static_cast<float>(320.0f);
		//SubViewport.Height = static_cast<float>(256.0f);
		//SubViewport.TopLeftX = 900;
		//SubViewport.TopLeftY = 500;
		//SubViewport.MinDepth = 0.0f;
		//SubViewport.MaxDepth = 0.5f;
		////比率が変わるのでカメラにビューポートを伝える
		//PtrSubCamera->SetViewPort(SubViewport);
		//PtrView->AddView(SubViewport, PtrSubCamera);
		////シングルライトの作成
		//auto PtrSingleLight = CreateLight<SingleLight>();
		////ライトの設定
		//PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);

		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera2>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 15.0f, -25.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);

	}


	//プレートの作成
	void GameStage::CreatePlate() {
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++) {
				//ステージへのゲームオブジェクトの追加
				auto Ptr = AddGameObject<GameObject>();
				//auto PtrTrans = Ptr->GetComponent<Transform>();
				//Quaternion Qt;
				//Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
				//Matrix4X4 WorldMat;
				//WorldMat.DefTransformation(
				//	Vector3(200.0f, 200.0f, 1.0f),
				//	Qt,
				//	Vector3(0.0f, 0.0f, 0.0f)
				//);
				//PtrTrans->SetScale(20.0f, 20.0f, 1.0f);
				//PtrTrans->SetQuaternion(Qt);
				//PtrTrans->SetPosition(-0.5f, 0.0f, -0.5f);
				auto PtrTrans = Ptr->GetComponent<Transform>();
				Quaternion Qt;
				Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
				Matrix4X4 WorldMat;
				WorldMat.DefTransformation(
					Vector3(10.0f, 10.0f, 1.0f),
					Qt,
					Vector3(0.0f, 0.0f, 0.0f)
				);
				PtrTrans->SetScale(1.0f, 1.0f, 1.0f);
				PtrTrans->SetQuaternion(Qt);
				PtrTrans->SetPosition(i - 10, 0.0f, j-10);

				//描画コンポーネントの追加
				auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
				//描画コンポーネントに形状（メッシュ）を設定
				DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
				//自分に影が映りこむようにする
				DrawComp->SetOwnShadowActive(true);

				//描画コンポーネントテクスチャの設定
				DrawComp->SetTextureResource(L"FLOOR_TX");

				Ptr->SetDrawLayer(1);
			}
		}

		//背景の描画
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());


		auto bg = AddGameObject<WallSprite>(L"BACKGROUND_TX", false,
			Vector2(w, h), Vector2(0, 0));
		bg->SetDrawLayer(-1);

		////犬カメラの縁
		//bg = AddGameObject<WallSprite>(L"DOGCAMERA_TX", true,
		//	Vector2(496, 496), Vector2(4.0f, -1.0f),true);
		//bg->SetDrawLayer(2);
		//auto bgtrans = bg->GetComponent<Transform>();
		//bgtrans->SetRotation(0, 0, XM_PI/2);

		//SetSharedGameObject(L"DOGCAMERA", bg);

		
	}

	void GameStage::CreateStageObject() {

		auto DSG = CreateSharedObjectGroup(L"Dog_SpriteGroup");

		auto ptr2 = AddGameObject<WallSprite>(L"UsuallyDog_TX", true,
			Vector2(320, 320), Vector2(-480, -240));
		ptr2->SetDrawLayer(2);
		SetSharedGameObject(L"DogFace", ptr2);
		DSG->IntoGroup(ptr2);
		ptr2->SetDrawLayer(11);

		ptr2 = AddGameObject<WallSprite>(L"OrdinaryBalloon_TX", true,
			Vector2(320, 320), Vector2(-320, -150));
		ptr2->SetDrawLayer(2);
		SetSharedGameObject(L"DogBalloon", ptr2);
		DSG->IntoGroup(ptr2);
		ptr2->SetDrawLayer(11);


		//扉のスイッチを表示する
		ptr2 = AddGameObject<WallSprite>(L"SWITCH_TX", true,
			Vector2(64, 64), Vector2(-370, -150));
		ptr2->SetDrawActive(false);
		ptr2->SetDrawLayer(2);
		SetSharedGameObject(L"DogSprite", ptr2);
		DSG->IntoGroup(ptr2);
		ptr2->SetDrawLayer(11);

		//「ボタンを押して」
		ptr2 = AddGameObject<WallSprite>(L"Button_Push_TX", true,
			Vector2(192, 64), Vector2(-320, -150));
		ptr2->SetDrawActive(false);
		ptr2->SetDrawLayer(2);
		SetSharedGameObject(L"Button_Push", ptr2);
		DSG->IntoGroup(ptr2);
		ptr2->SetDrawLayer(11);


		auto ptr3 = AddGameObject<DogSerif>(Vector3(-320, -150, 0.0f), Vector3(192, 64, 1.0f));
		ptr3->SetDrawLayer(2);
		SetSharedGameObject(L"DogSerif", ptr3);
		ptr3->SetDrawLayer(11);



		
		////Csvファイル
		//wstring Filename = App::GetApp()->m_wstrRelativeDataPath + L"Map_Csv\\" + L"data" +L".csv";
		////ローカル上にCSVファイルクラスの作成
		//CsvFile GameStageCsv(Filename);
		//if (!GameStageCsv.ReadCsv()) {
		//	//ファイルが存在しなかった
		//	//初期化失敗
		//	throw BaseException(
		//		L"CSVファイルがありません。",
		//		Filename,
		//		L"選択された場所にはアクセスできません"
		//	);
		//}
		//const int iDataSizeRow = 0;		//データが0行目から始まるようの定数
		//vector< wstring > StageMapVec;	//ワーク用のベクター配列
		//								//iDataSizeRowのデータを抜き取りベクター配列に格納
		//GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);
		//////行、列の取得
		//line_size = static_cast<size_t>(_wtoi(StageMapVec[0].c_str())); //Row  = 行
		//col_size = static_cast<size_t>(_wtoi(StageMapVec[1].c_str())); //Col　= 列
		//															   //マップデータ配列作成
		//m_MapDataVec = vector< vector<size_t> >(line_size, vector<size_t>(col_size));	//列の数と行の数分作成
		//																					//配列の初期化
		//for (size_t i = 0; i < line_size; i++) {
		//	for (size_t j = 0; j < col_size; j++)
		//	{
		//		m_MapDataVec[i][j] = 0;								//size_t型のvector配列を０で初期化			
		//	}
		//}
		////1行目からステージが定義されている
		//const int iDataStartRow = 1;
		////assert(m_sRowSize > 0 && "行が０以下なのでゲームが開始できません");
		//if (line_size > 0) {
		//	for (size_t i = 0; i < line_size; i++) {
		//		GameStageCsv.GetRowVec(i + iDataStartRow, StageMapVec);		//スタート + i　だから最初は 2が入る
		//		for (size_t j = 0; j < col_size; j++) {					//列分ループ処理
		//			const int iNum = _wtoi(StageMapVec[j].c_str());			//列から取得したwstring型をintに変換→格納	
		//		//マップデータ配列に格納
		//			m_MapDataVec[i][j] = iNum;
		//			//配置されるオブジェクトの基準スケール
		//			float ObjectScale = 1.0f;
		//			//基準Scale
		//			Vector3 Scale(ObjectScale, ObjectScale, ObjectScale);
		//			Vector3 Pos(static_cast<float>(j), 0.5, static_cast<float>(i));
		//			switch (iNum) {
		//			case 1:
		//				AddGameObject<FixedBox>(Scale, Vector3(0, 0, 0), Pos);
		//				break;
		//			}
		//		}
		//	}
		//}

		//ゲームプログラミングのファイル
		//// ファイルからマップデータを読み込む	------------------------------------------
		//FILE*	fp;
		////fp = fopen("map.csv", "r");
		//if (!fopen_s(&fp, fileName, "r")) {// 読み取り専用で開く
		//	int a;	// 読み込んだデータを一時的に保存する変数
		//	int c = 0, r = 0;	// マップ用の二次元配列の添え字
		//	char digit[100] = { 0 };
		//	int d_cnt = 0;
		//	do {
		//		a = fgetc(fp);	// ファイルから１文字読み込む
		//		if (isdigit(a)) {	// 読み込んだ文字が整数か判断
		//							//fgetcで読み込んだ文字をdigitに入れていく
		//			digit[d_cnt++] = a;
		//		}
		//		else {
		//			digit[d_cnt] = '\0';
		//			d_cnt = 0;
		//			//digitのデータを数値に変換してマップに入れる 
		//			//map[r][c++] = atoi(digit);
		//			map[r * col + c] = atoi(digit);
		//			c++;
		//			if (c >= col) {
		//				c = 0;
		//				r++;
		//				if (r>row) {
		//					break;
		//				}
		//			}
		//		}
		//	} while (a != EOF);	// ファイルの終了を示す値(EOF, End Of File)を読み込んだら終了
		//	fclose(fp);	// ファイルを閉じる
		//}
		//	------------------------------------------------------------------------------

		int MapSize = 20;

		//ゲームオブジェクトの大きさ、回転、位置
		Vector3 GO_Scale = Vector3(0, 0, 0);
		Vector3 GO_Rot = Vector3(0, 0, 0);
		Vector3 GO_Pos = Vector3(0, 0, 0);

		//プレイヤーと犬以外のオブジェクトを格納するオブジェクトグループ
		auto OGO = CreateSharedObjectGroup(L"Other_GameObject");

		//プレイヤーと犬の情報を格納するオブジェクトグループ
		auto PandD = CreateSharedObjectGroup(L"PlayerAndDog");

		//プレゼントボックスのグループ格納用
		auto PBG = CreateSharedObjectGroup(L"PresentBoxGroup");

		//ドアのグループ格納用
		auto DG = CreateSharedObjectGroup(L"DoorGroup");

		//スイッチのグループ格納用
		auto SG = CreateSharedObjectGroup(L"SwitchGroup");

		//匂いを惑わすオブジェクトグループ格納用
		auto OG = CreateSharedObjectGroup(L"OdorGroup");

		//経路探索用のパス群
		auto Route_search = CreateSharedObjectGroup(L"RoadPathGroup");

		//犬の上に表示されるスプライト
		//auto DogSprite = CreateSharedObjectGroup(L"DogSpriteGroup");

		//プレゼントボックスの上に表示されるスプライト
		auto PresentBoxSprite = CreateSharedObjectGroup(L"PresentBoxSpriteGroup");

		shared_ptr<GameObject> ptr;

		auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetView()->GetTargetCamera());

		//CSVファイルからゲームステージの選択
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath + L"Map_Csv\\";//csvファイルの入っているフォルダを指定
																				   //シーンからゲームセレクトで選んだゲームステージの番号取得
		int i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNo();

		//ステージ番号からステージの名前を取得
		wstring i_StageName = Util::IntToWStr(i_StageNumber);
		wstring FileName = MediaPath + L"Map_";//
		FileName += i_StageName + L".csv";//Stage番号と拡張子の指定
										  //wstring FileName = MediaPath + L"Stage_1.csv";

										  //ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(FileName);
		//ファイルがあるかどうか
		if (!GameStageCsv.ReadCsv()) {
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません",
				FileName,
				L"選択された場所にdアクセスできません"

			);
		}

		const int iDataSizeRow = 0; //データが0行目から始まるように定数
		vector<wstring>StageMapVec; //ワーク用のベクター配列
		GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);

		//行、列の取得
		auto m_sRowSize = static_cast<size_t>(20); //Row=行
		auto m_sColSize = static_cast<size_t>(20); //Col=列

																		 //マップデータの配列作成
		auto map = vector<vector<size_t>>(m_sRowSize, vector<size_t>(m_sColSize)); //列の数と行の数分
																						//配列の初期化
		for (size_t i = 0; i < m_sRowSize; i++) {
			for (size_t j = 0; j < m_sColSize; j++) {
				map[i][j] = 0;	//0で初期化
			}
		}

		for (size_t i = 0; i < m_sRowSize; i++) {
			GameStageCsv.GetRowVec(i, StageMapVec); //START + iだから最初は２が入る
			for (size_t j = 0; j < m_sColSize; j++) {
				const int iNum = _wtoi(StageMapVec[j].c_str()); //列から取得したwstringをintに
				//マップデータ配列に格納
				map[i][j] = iNum;
			}
		}
		

		//const int map[20][20] = {
		//	//  0	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19
		//	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,
		//	1,	4,	0,	0,	0,	0,	0,	0,	1,	1,	1,	1,	1,	1,	1,	7,	0,	0,	4,	1,
		//	1,	0,	0,	0,	0,	0,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
		//	1,	0,	0,	1,	1,	1,	0,	0,	1,	7,	1,	7,	1,	7,	1,	7,	0,	0,	0,	1,
		//	1,	5,	1,	1,	1,	1,	0,	0,	1,	1,	0,	0,	0,	0,	0,	0,	0,	99,	0,	1,
		//	1,	0,	0,	7,	1,	1,	1,	1,	1,	7,	0,	6,	0,	0,	0,	0,	0,	0,	0,	1,
		//	1,	0,	0,	1,	1,	1,	7,	1,	7,	1,	7,	1,	7,	1,	7,	1,	0,	0,	0,	1,
		//	1,	0,	0,	7,	1,	7,	0,	0,	1,	1,	1,	1,	1,	1,	1,	7,	0,	0,	0,	1,
		//	1,	0,	0,	1,	1,	1,	0,	0,	7,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
		//	1,	0,	0,	7,	1,	7,	0,	0,	1,	1,	1,	1,	1,	1,	1,	7,	0,	0,	0,	1,
		//	1,	0,	0,	1,	7,	1,	0,	0,	7,	1,	7,	1,	7,	1,	7,	1,	0,	0,	0,	1,
		//	1,	99,	0,	0,	0,	0,	99, 0,	0,	0,	0,	0,	0,	0,	0,	0,	2,	99,	0,	1,
		//	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	6,	3,	0,	0,	1,
		//	1,	0,	0,	1,	1,	1,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
		//	1,	0,	0,	1,	1,	1,	8,	0,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
		//	1,	0,	0,	1,	1,	1,	5,	1,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	1,
		//	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,
		//	1,	99,	0,	0,	0,	0,	99,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	99,	0,	1,
		//	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,
		//	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,
		//};

		int i, j;

		for (i = 0; i < MapSize; i++) {
			for (j = 0; j < MapSize; j++) {
				GO_Scale = Vector3(1.0f, 1.0f, 1.0f);
				GO_Rot = Vector3(0, 0, 0);
				GO_Pos = Vector3((float)j - MapSize / 2, 0.0f, (float)i - MapSize / 2);
				switch (map[j][i]) {
					//1なら壁を配置する
				case 1:
					GO_Scale = Vector3(1.0f, 2.0f, 1.0f);
					ptr = AddGameObject<FixedBox>(GO_Scale, GO_Rot, GO_Pos);
					OGO->IntoGroup(ptr);

					break;
					//2ならプレイヤーを配置する
				case 2:
					ptr = AddGameObject<Player>(GO_Scale,GO_Rot ,GO_Pos);
					SetSharedGameObject(L"Player", ptr);
					Route_search->IntoGroup(AddGameObject<RoadPath>(GO_Pos));
	/*				if (PtrCamera) {
						PtrCamera->SetTargetObject(playerptr);
					}*/
					break;
					//3なら犬を配置する
				case 3:
					ptr = AddGameObject<Dog>(GO_Scale, GO_Rot, GO_Pos);
					SetSharedGameObject(L"Dog", ptr);
					//PtrSubCamera->SetTargetObject(ptr);
					//DogSprite->IntoGroup(AddGameObject<Square>(ptr,0));
					Route_search->IntoGroup(AddGameObject<RoadPath>(GO_Pos));
					break;
					//4ならプレゼントボックスを配置する
				case 4:
					GO_Scale = Vector3(0.6f, 0.6f, 0.6f);
					ptr = AddGameObject<PresentBoxModel>(GO_Scale, GO_Rot, GO_Pos, PBG->size());
					OGO->IntoGroup(ptr);
					PBG->IntoGroup(ptr);
					PresentBoxSprite->IntoGroup(AddGameObject<Square>(ptr, 1));
					break;
					//5ならドアを配置する
				case 5:
					GO_Scale = Vector3(1.0f, 1.0f, 1.0f);
					GO_Rot = Vector3(0, -3.14/2, 0);
					DG->IntoGroup(AddGameObject<DoorModel>(GO_Scale, GO_Rot, GO_Pos));
					Route_search->IntoGroup(AddGameObject<RoadPath>(GO_Pos));
					break;
					//6ならスイッチを配置する
				case 6:
					GO_Scale = Vector3(1.0f, 0.01f, 1.0f);
					GO_Rot = Vector3(0, 0, 0);
					SG->IntoGroup(AddGameObject<SwitchModel>(GO_Scale, GO_Rot, GO_Pos));
					Route_search->IntoGroup(AddGameObject<RoadPath>(GO_Pos));
					break;
					//7ならツリーモデルを配置する
				case 7:
					GO_Scale = Vector3(1.0f, 2.0f, 1.0f);
					ptr = AddGameObject<TreeModel>(GO_Scale, GO_Rot, GO_Pos);
					OGO->IntoGroup(ptr);
					break;
				case 8:
					GO_Scale = Vector3(1.0f, 1.0f, 1.0f);
					GO_Rot = Vector3(0, 0, 0);
					OG->IntoGroup(AddGameObject<OdorModel>(GO_Scale, GO_Rot, GO_Pos));
					break;
					//経路探索用パスの配置
				case 99:
					Route_search->IntoGroup(AddGameObject<RoadPath>(GO_Pos));
					break;
				}
			}
		}

		auto OrnamentG = CreateSharedObjectGroup(L"OrnamentGroup");


		//匂いを惑わすオブジェクトから3マス以内、かつ縦か横ドアが
		for (auto o : OG->GetGroupVector()) {
			auto o_ptr = dynamic_pointer_cast<OdorModel>(o.lock());
			auto o_Pos = o_ptr->GetComponent<Transform>()->GetPosition();
			for (auto d : DG->GetGroupVector()) {
				auto d_ptr = dynamic_pointer_cast<DoorModel>(d.lock());
				auto d_Pos = d_ptr->GetComponent<Transform>()->GetPosition();
				if (sqrt((d_Pos.x - o_Pos.x)*(d_Pos.x - o_Pos.x) + (d_Pos.z - o_Pos.z)*(d_Pos.z - o_Pos.z)) <= 3 && (o_Pos.x == d_Pos.x || o_Pos.z == d_Pos.z)){
					for (auto s :SG->GetGroupVector()) {
						auto s_ptr = dynamic_pointer_cast<SwitchModel>(s.lock());
						auto s_Pos = s_ptr->GetComponent<Transform>()->GetPosition();
						if (d_ptr->m_DoorNum == s_ptr->m_SwitchNum) {
							auto orn = AddGameObject<Ornament>(s_Pos);
							Ornament_Decision = true;
							OrnamentG->IntoGroup(orn);
						}
					}
				}
			}
		}



		//カメラターゲットオブジェクトを配置
		auto ct = AddGameObject<CameraTarget>();
		if (PtrCamera) {
			PtrCamera->SetTargetObject(ct);
		}
		SetSharedGameObject(L"Camera_Target",ct);

	}

	//タイマーの作成
	void GameStage::CreateTimer() {
		auto ptr2 = AddGameObject<WallSprite>(L"TIME_TX", true,
			Vector2(200, 100), Vector2(-500, 320));
		ptr2->SetDrawLayer(2);
		//ナンバースプライトの１0の位のシェアードゲードオブジェクトを作成
		auto ptr = AddGameObject<NumberSprite>(Vector3(-390, 320, 0), Vector3(100, 100, 1.0f));
		SetSharedGameObject(L"TimeNum_TenPlace", ptr);
		ptr->SetDrawLayer(2);

		//ナンバースプライトの１の位のシェアードゲードオブジェクトを作成
		ptr = AddGameObject<NumberSprite>(Vector3(-320, 320, 0), Vector3(100, 100, 1.0f));
		SetSharedGameObject(L"TimeNum_OnePlace", ptr);
		ptr->SetDrawLayer(2);

		//ナンバースプライトの１00の位のシェアードゲードオブジェクトを作成
		ptr = AddGameObject<NumberSprite>(Vector3(-250, 320, 0), Vector3(100, 100, 1.0f));
		SetSharedGameObject(L"TimeNum_HyakuPlace", ptr);
		ptr->SetDrawLayer(2);
	}
	
	//アイテムカウンターの作成
	void GameStage::CreateItemCounter() {
		auto Counter = CreateSharedObjectGroup(L"ItemCounter");
		auto PBG = GetSharedObjectGroup(L"PresentBoxGroup");	
		auto ptr = AddGameObject<NumberSprite>(Vector3(570, 530, 0), Vector3(100, 100, 1.0f));
		SetSharedGameObject(L"ItemCounter_All", ptr);
		All_ItemCount = PBG->size();
		ptr->SetNum(All_ItemCount);
		Counter->IntoGroup(ptr);
		ptr->SetDrawLayer(2);
		ptr = AddGameObject<NumberSprite>(Vector3(430, 530, 0), Vector3(100, 100, 1.0f));
		SetSharedGameObject(L"ItemCounter_Recovery", ptr);
		Counter->IntoGroup(ptr);
		ptr->SetDrawLayer(2);
		auto ptr2 = AddGameObject<WallSprite>(L"ITEM_C_TX", true,
			Vector2(128, 128), Vector2(330, 530));
		Counter->IntoGroup(ptr2);
		ptr2->SetDrawLayer(2);
		ptr2 = AddGameObject<WallSprite>(L"SLASH_TX", true,
			Vector2(100, 100), Vector2(500, 530));
		Counter->IntoGroup(ptr2);
		ptr2->SetDrawLayer(2);
	}


	void GameStage::OnCreate() {
		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//プレートの作成
			CreatePlate();
			//ステージオブジェクトの作成(CSV)
			CreateStageObject();
			//タイマーの作成
			CreateTimer();
			//アイテムカウンターの作成
			CreateItemCounter();

			wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"GameBgm.wav";
			App::GetApp()->RegisterWav(L"GAMEBGM", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Get.wav";
			App::GetApp()->RegisterWav(L"GET", strMusic);

			//オーディオの初期化
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"GAMEBGM");
			m_AudioObjectPtr->Start(L"GAMEBGM", XAUDIO2_LOOP_INFINITE, 0.2f);


		}
		catch (...) {
			throw;
		}
		auto fi2 = AddGameObject<FadeIn2>();
		SetSharedGameObject(L"Fade_In2", fi2);
		fi2->SetDrawLayer(100);
		auto ptr = AddGameObject<FadeOut>();
		SetSharedGameObject(L"Fade_Out", ptr);
		ptr->SetDrawLayer(100);

		auto ptr1 = AddGameObject<FadeOut>();
		SetSharedGameObject(L"Fade_Out2", ptr);
		ptr1->SetDrawLayer(100);
	}

	void GameStage::OnUpdate() {

		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

		//auto bg = GetSharedGameObject<WallSprite>(L"DOGCAMERA");
		//auto bgtrans = bg->GetComponent<Transform>();
		//auto bgpos = bgtrans->GetPosition();
		//if (CntlVec[0].bConnected) {
		//	if (CntlVec[0].fThumbLX >= 0.3f) {
		//		bgtrans->SetPosition(Vector3(bgpos.x + 10.0f, bgpos.y, bgpos.z));
		//	}
		//	if (CntlVec[0].fThumbLX <= -0.3f) {
		//		bgtrans->SetPosition(Vector3(bgpos.x - 10.0f, bgpos.y, bgpos.z));
		//	}
		//	bgpos = bg->GetComponent<Transform>()->GetPosition();
		//	if (CntlVec[0].fThumbLY >= 0.3f) {;
		//	bgtrans->SetPosition(Vector3(bgpos.x , bgpos.y+ 10.0f, bgpos.z));
		//	}
		//	if (CntlVec[0].fThumbLY <= -0.3f) {
		//		bgtrans->SetPosition(Vector3(bgpos.x , bgpos.y- 10.0f, bgpos.z));
		//	}
		//}
		auto fi2 = GetSharedGameObject<FadeIn2>(L"Fade_In2", false);
		fi2->SetFOD_Start(true);

		auto ptr_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_OnePlace");
		ptr_Num->SetNum(GameTimer/10);

		ptr_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_TenPlace");
		ptr_Num->SetNum(GameTimer / 100);

		ptr_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_HyakuPlace");
		ptr_Num->SetNum(GameTimer);

		if (GameTimer <= 0 && !IsPushButton) {
			auto ptr = GetSharedGameObject<FadeOut>(L"Fade_Out", false);
			ptr->SetFOB_Start(true);
			//auto ScenePtr = App::GetApp()->GetScene<Scene>();
			//PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameOverStage");
			IsPushButton = true;
		}

		ptr_Num = GetSharedGameObject<NumberSprite>(L"ItemCounter_Recovery");
		ptr_Num->SetNum(Now_ItemCount);

		if (All_ItemCount == Now_ItemCount) {
			if (!IsPushButton) {
				if (GameTimer && !TimerStop) {
					TimerStop = true;
					//auto ScenePtr = App::GetApp()->GetScene<Scene>();
					//PostEvent(1.5f, GetThis<ObjectInterface>(), ScenePtr, L"ToClearStage");
					m_AudioObjectPtr->AddAudioResource(L"GET");
					m_AudioObjectPtr->Start(L"GET", 0.0f, 0.5f);
				}
				IsPushButton = false;
			}
			if (ClearCounter<=120) {
				ClearCounter++;
			}
			if (ClearCounter >=120) {
				auto ptr = GetSharedGameObject<FadeOut>(L"Fade_Out", false);
				ptr->SetFOW_Start(true);
			}

		}
		else {
			auto PtrCamera = dynamic_pointer_cast<LookAtCamera2>(GetView()->GetTargetCamera());
			if (!PtrCamera->m_Camera_RotNow) {
				if (GameTimer > 0) {
					float ElapsedTime = App::GetApp()->GetElapsedTime();
					GameTimer -= ElapsedTime;
				}
			}
		}
		auto DogSprite = GetSharedGameObject<WallSprite>(L"DogSprite");
		//auto DogSprite_Pos = DogSprite->GetComponent<Transform>()->GetPosition();
		auto Button_Push = GetSharedGameObject<WallSprite>(L"Button_Push");
		//auto Button_Push_Pos = Button_Push->GetComponent<Transform>()->GetPosition();
		auto balloon_Pos = GetSharedGameObject<WallSprite>(L"DogBalloon")->GetComponent<Transform>()->GetPosition();

		DogSprite->GetComponent<Transform>()->SetPosition(Vector3(balloon_Pos.x,balloon_Pos.y+30, balloon_Pos.z));
		Button_Push->GetComponent<Transform>()->SetPosition(Vector3(balloon_Pos.x, balloon_Pos.y-20, balloon_Pos.z));

		//if (Now_ItemCount >= 1 && Now_ItemCount < 2) {
		//	auto ptr = AddGameObject<WallSprite>(L"TANSAKU_TX", true, Vector2(512, 128), Vector2(20, -250));
		//	ptr->SetDrawLayer(2);

		//}

		/*if (GameTimer <= 10) {
			auto ptr = GetSharedGameObject<FadeOut>(L"Fade_Out", false);
			ptr->SetFOA_Start(true);
		}
*/

		//時間の数字が大きくなったり小さくなったりする
		if (GameTimer < 11) {
				
			auto One_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_OnePlace")->GetComponent<Transform>();
			auto Ten_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_TenPlace")->GetComponent<Transform>();
			auto Hyaku_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_HyakuPlace")->GetComponent<Transform>();

			if (One_Num->GetScale().x < 90.0f) {
				Time_Scaling = 1.0f;
			}

			else if (One_Num->GetScale().x > 110.0f) {
				Time_Scaling = -1.0f;
			}
			One_Num->SetScale(Vector3(One_Num->GetScale().x + Time_Scaling, One_Num->GetScale().y + Time_Scaling, One_Num->GetScale().z));
			Ten_Num->SetScale(Vector3(One_Num->GetScale().x + Time_Scaling, One_Num->GetScale().y + Time_Scaling, One_Num->GetScale().z));
			Hyaku_Num->SetScale(Vector3(One_Num->GetScale().x + Time_Scaling, One_Num->GetScale().y + Time_Scaling, One_Num->GetScale().z));

		}


		if (CntlVec[0].bConnected) {
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START && !IsPushButton) {
				
				//二度と押さないようにする
				auto ptr = GetSharedGameObject<FadeOut>(L"Fade_Out", false);
				ptr->SetFOT_Start(true);
				IsPushButton = true;

			}
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_BACK && !IsPushButton) {

				//二度と押さないようにする
				auto ptr1 = GetSharedGameObject<FadeOut>(L"Fade_Out2", false);
				ptr1->SetFOS_Start(true);
				IsPushButton = true;

			}
		}

	}

	GameStage::~GameStage() {
		m_AudioObjectPtr->Stop(L"GAMEBGM");
	}

	//--------------------------------------------------------------------------------------
	//	クリアステージクラス実体
	//--------------------------------------------------------------------------------------

	//ビューとライトの作成
	void ClearStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}

	//クリアのスプライト作成
	void ClearStage::CreateWallSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<WallSprite>(L"RESULT_TX", false,
			Vector2(w, h), Vector2(0, 0));

		AddGameObject<WallSprite>(L"CLEAR_TX", true,
			Vector2(768, 256), Vector2(0, 260));


		auto sp = CreateSharedObjectGroup(L"SelectPlateGroup");
		
		auto s = AddGameObject<WallSprite>(L"NEXTSTAGE_TX", true,
			Vector2(640, 128), Vector2(-250, -350));
		sp->IntoGroup(s);

		s = AddGameObject<WallSprite>(L"TOTITLE_TX", true,
			Vector2(640, 128), Vector2(300, -350));
		sp->IntoGroup(s);

		//シーンの情報を取得
		auto ScenePtr = App::GetApp()->GetScene<Scene>();

		//クリア時写真を表示する
		switch (ScenePtr->GetStageNo())
		{
		case 1:
			s = AddGameObject<WallSprite>(L"PHOTO_TX", true,
				Vector2(384, 256), Vector2(0, 30));			
			break;
		case 2:
			s = AddGameObject<WallSprite>(L"PHOTO_TX", true,
				Vector2(384, 256), Vector2(0, 30));			
			break;
		case 3:
			s = AddGameObject<WallSprite>(L"ITEM_TOY_TX", true,
				Vector2(384, 256), Vector2(0, 30));			
			break;
		case 4:
			s = AddGameObject<WallSprite>(L"ITEM_TOY_TX", true,
				Vector2(384, 256), Vector2(0, 30));			
			break;
		case 5:
			s = AddGameObject<WallSprite>(L"ITEM_TOY_TX", true,
				Vector2(384, 256), Vector2(0, 30));			
			break;
		default:
			s = AddGameObject<WallSprite>(L"PHOTO_TX", true,
				Vector2(384, 256), Vector2(0, 30));
			break;
		}
		auto PtrAction = s->AddComponent<Action>();
		PtrAction->AddMoveBy(1.0f, Vector3(0, 20, 0));
		PtrAction->AddMoveBy(1.0f, Vector3(0, -20, 0));
		PtrAction->SetLooped(true);
		PtrAction->Run();
	}


	void ClearStage::OnCreate() {
		try {
			//リソースの作成
			//CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//壁模様のスプライト作成
			CreateWallSprite();

			wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"1.wav";
			App::GetApp()->RegisterWav(L"11", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"TitleButton.wav";
			App::GetApp()->RegisterWav(L"TB", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Select.wav";
			App::GetApp()->RegisterWav(L"SELECT", strMusic);

			//オーディオの初期化
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"11");
			m_AudioObjectPtr->Start(L"11", XAUDIO2_LOOP_INFINITE, 0.3f);


		}
		catch (...) {
			throw;
		}
		//フェードアウト
		auto ptr = AddGameObject<FadeOut>();
		SetSharedGameObject(L"Fade_Out", ptr);
		ptr->SetDrawLayer(2);
		//フェードイン
		auto fi = AddGameObject<FadeIn>();
		SetSharedGameObject(L"Fade_In", fi);
		fi->SetDrawLayer(2);

		//フェードイン
		auto fo2 = AddGameObject<FadeOut2>();
		SetSharedGameObject(L"Fade_Out2", fo2);
		fo2->SetDrawLayer(2);
	}

	//更新
	void ClearStage::OnUpdate() {
		auto sp = GetSharedObjectGroup(L"SelectPlateGroup");

		auto fi = GetSharedGameObject<FadeIn>(L"Fade_In", false);
		fi->SetFOI_Start(true);


		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			//Bボタンが押された瞬間ならステージ推移
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !IsPushButton) {
				auto ScenePtr = App::GetApp()->GetScene<Scene>();
				//PostEvent(1.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToMenuStage");
				//二度と押さないようにする
				auto ptr = GetSharedGameObject<FadeOut>(L"Fade_Out", false);
				auto ptr2 = GetSharedGameObject<FadeOut2>(L"Fade_Out2", false);

				switch (StageSelectNo) {
				case 0:
					ScenePtr->SetStageNo(ScenePtr->GetStageNo() + 1);

					ptr2->SetFOD_Start(true);
					IsPushButton = true;

					m_AudioObjectPtr->AddAudioResource(L"TB");
					m_AudioObjectPtr->Start(L"TB", 0.0f, 0.3f);
					break;
				case 1:
					
					ptr->SetFOT_Start(true);
					IsPushButton = true;

					m_AudioObjectPtr->AddAudioResource(L"TB");
					m_AudioObjectPtr->Start(L"TB", 0.0f, 0.3f);
					break;
				}
			}



			if (CntlVec[0].fThumbLX >= 0.7f) {
				if (!OnePushSelect) {
					StageSelectNo--;
					if (StageSelectNo < 0) {
						StageSelectNo = sp->size() - 1;
					}
					OnePushSelect = true;
					//オーディオの初期化
					m_AudioObjectPtr->AddAudioResource(L"SELECT");
					m_AudioObjectPtr->Start(L"SELECT", 0,0.3f);
				}
			}
			else if (CntlVec[0].fThumbLX <= -0.7f) {
				if (!OnePushSelect) {
					StageSelectNo++;
					if (StageSelectNo > sp->size() - 1) {
						StageSelectNo = 0;
					}
					OnePushSelect = true;
					//オーディオの初期化
					m_AudioObjectPtr->AddAudioResource(L"SELECT");
					m_AudioObjectPtr->Start(L"SELECT",0, 0.3f);
				}
			}
			else {
				OnePushSelect = false;
			}

			for (auto i = 0; i < sp->size(); i++)
			{
				if (i == StageSelectNo) {
					sp->at(i)->GetComponent<Transform>()->SetScale(Vector3(640 * 1.2f, 128 * 1.2f, 1.0f));
					continue;
				}
				sp->at(i)->GetComponent<Transform>()->SetScale(Vector3(640, 128, 1.0f));
			}
		}
	}

	ClearStage::~ClearStage() {
		m_AudioObjectPtr->Stop(L"11");
	}



	//--------------------------------------------------------------------------------------
	// ゲームオーバーステージクラス実体
	//--------------------------------------------------------------------------------------

	//ビューとライトの作成
	void GameOverStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}

	//ゲームオーバーのスプライト作成
	void GameOverStage::CreateWallSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<WallSprite>(L"GAMEOVERBG_TX", false,
			Vector2(w, h), Vector2(0, 0));

		AddGameObject<WallSprite>(L"GAMEOVER_TX", true,
			Vector2(1280,256 ), Vector2(0, 150));

		auto sp = CreateSharedObjectGroup(L"SelectPlateGroup");

		auto s = AddGameObject<WallSprite>(L"RETRY_TX", true,
			Vector2(512, 128), Vector2(-400, -300));
		sp->IntoGroup(s);

		s = AddGameObject<WallSprite>(L"TOTITLE_TX", true,
			Vector2(640, 128), Vector2(400, -300));
		sp->IntoGroup(s);

	}


	void GameOverStage::OnCreate() {
		try {
			//ビューとライトの作成
			CreateViewLight();
			//壁模様のスプライト作成
			CreateWallSprite();

			wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"GameOver.wav";
			App::GetApp()->RegisterWav(L"GO", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"TitleButton.wav";
			App::GetApp()->RegisterWav(L"TB", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Select.wav";
			App::GetApp()->RegisterWav(L"SELECT", strMusic);

			//オーディオの初期化
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"GO");
			m_AudioObjectPtr->Start(L"GO", XAUDIO2_LOOP_INFINITE, 0.3f);
		}
		catch (...) {
			throw;
		}
		//フェードアウト
		auto ptr2 = AddGameObject<FadeOut2>();
		SetSharedGameObject(L"Fade_Out2", ptr2);
		ptr2->SetDrawLayer(2);

		auto ptr = AddGameObject<FadeOut>();
		SetSharedGameObject(L"Fade_Out", ptr);
		ptr->SetDrawLayer(2);

		auto fi = AddGameObject<FadeIn>();
		SetSharedGameObject(L"Fade_In", fi);
		fi->SetDrawLayer(2);
	}

	//更新
	void GameOverStage::OnUpdate() {

		auto sp = GetSharedObjectGroup(L"SelectPlateGroup");

		auto fi = GetSharedGameObject<FadeIn>(L"Fade_In", false);
		fi->SetFOW_Start(true);

		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			//Bボタンが押された瞬間ならステージ推移
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !IsPushButton) {
				/*auto ScenePtr = App::GetApp()->GetScene<Scene>();
				PostEvent(1.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToMenuStage");*/
				//二度と押さないようにする
				if (StageSelectNo == 0) {
					auto ptr = GetSharedGameObject<FadeOut2>(L"Fade_Out2", false);
					ptr->SetFOD_Start(true);
					PostEvent(1.0f, GetThis<ObjectInterface>(), ptr, L"ToGameStage");
				}
				else {
					auto ptr = GetSharedGameObject<FadeOut>(L"Fade_Out", false);
					ptr->SetFOT_Start(true);
				}
				m_AudioObjectPtr->AddAudioResource(L"TB");
				m_AudioObjectPtr->Start(L"TB", 0.0f, 0.3f);
				IsPushButton = true;
			}

			if (CntlVec[0].fThumbLX >= 0.7f) {
				if (!OnePushSelect) {
					StageSelectNo--;
					if (StageSelectNo < 0) {
						StageSelectNo = sp->size() - 1;
					}
					OnePushSelect = true;
					//オーディオの初期化
					m_AudioObjectPtr->AddAudioResource(L"SELECT");
					m_AudioObjectPtr->Start(L"SELECT", 0,0.3f);
				}
			}
			else if (CntlVec[0].fThumbLX <= -0.7f) {
				if (!OnePushSelect) {
					StageSelectNo++;
					if (StageSelectNo > sp->size() - 1) {
						StageSelectNo = 0;
					}
					OnePushSelect = true;
					//オーディオの初期化
					m_AudioObjectPtr->AddAudioResource(L"SELECT");
					m_AudioObjectPtr->Start(L"SELECT", 0,0.3f);
				}
			}
			else {
				OnePushSelect = false;
			}

			for (auto i = 0; i < sp->size(); i++)
			{
				if (i == StageSelectNo) {
					if (i == 0) {
						sp->at(i)->GetComponent<Transform>()->SetScale(Vector3(512 * 1.2f, 128 * 1.2f, 1.0f));
					}
					else {
						sp->at(i)->GetComponent<Transform>()->SetScale(Vector3(640 * 1.2f, 128 * 1.2f, 1.0f));
					}
					continue;
				}
				if (i ==0) {
					sp->at(i)->GetComponent<Transform>()->SetScale(Vector3(512, 128, 1.0f));

				}
				else {
					sp->at(i)->GetComponent<Transform>()->SetScale(Vector3(640, 128, 1.0f));
				}
			}
		}
	}
	GameOverStage::~GameOverStage() {
		m_AudioObjectPtr->Stop(L"GO");
	}
}
//end basecross
