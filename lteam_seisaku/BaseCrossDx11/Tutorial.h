/*!
@file Tutorial.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class Tutorial : public Stage {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		void CreatePlate();
		//ステージオブジェクトの作成
		void CreateStageObject();
		//タイマー
		void CreateTimer();
		//アイテムカウンター
		void CreateItemCounter();

		void CreateDescription();

		bool TimerStop;

		//ボタン一発しかならないやつ
		bool IsPushButton;
		//タイマーの時間設定
		float GameTimer = 100;

		float Time_Scaling = -1.0f;

		float Timerharf = GameTimer / 2;

		//説明用のスプライトを時間経過で変更する
		int SpriteCounter = 0;
		bool sp_flg = false;



		//CSV読み込み 
		void LoadMAPCSV();
		UINT m_StageNum = 1;
		int *m_Stagearr;				//ステージ配列
		Vector2 m_StageSize;			//ステージサイズ
		Vector3 m_PlayerinitPos;		//プレイヤーの初期位置
										//Csv用変数
		size_t line_size;
		size_t col_size;


		//ワーク用のベクター配列のメンバ変数
		vector< vector <size_t> > m_MapDataVec;

		//FadeOut
		void CreateFade();
	public:
		//構築と破棄
		Tutorial() :Stage() {}
		virtual ~Tutorial();
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		//アイテムカウンター用の変数
		//ステージ内のアイテムの総数
		int All_ItemCount = 0;
		//現在取得しているアイテムの数
		int Now_ItemCount = 0;
		//置物があるかどうか
		bool Ornament_Decision = false;

		shared_ptr<LookAtCamera3> PtrSubCamera;
	};
}



