#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	//	class NumberSprite : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	NumberSprite::NumberSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const Vector3& StartScale) :
		GameObject(StagePtr), m_StartPos(StartPos), m_StartScale(StartScale), m_TotalTime(0) {
	}
	NumberSprite::~NumberSprite() {}

	//初期化
	void NumberSprite::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(m_StartScale);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		Initial_Pos = PtrTransform->GetPosition();
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"NUMBER_TX");
		//透明処理
		SetAlphaActive(true);
		//左上原点
		//スプライトの中のメッシュからバックアップの取得
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		//各数字ごとにUV値を含む頂点データを配列化しておく
		for (size_t i = 0; i < 10; i++) {

			float from = ((float)i) / 10.0f;
			float to = from + (1.0f / 10.0f);
			vector<VertexPositionColorTexture> NumVirtex =
			{
				//左上頂点
				VertexPositionColorTexture(
					SpVertexVec[0].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(from, 0)
				),
				//右上頂点
				VertexPositionColorTexture(
					SpVertexVec[1].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(to, 0)
				),
				//左下頂点
				VertexPositionColorTexture(
					SpVertexVec[2].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(from, 1.0f)
				),
				//右下頂点
				VertexPositionColorTexture(
					SpVertexVec[3].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(to, 1.0f)
				),
			};
			m_NumberVertexVec.push_back(NumVirtex);
		}
	}

	void NumberSprite::OnUpdate() {
		////前回のターンからの時間
		size_t Num = (size_t)m_TotalTime;
		Num = Num % 10;
		auto PtrSprite = GetComponent<PCTSpriteDraw>();
		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値が変わる頂点を設定する
		MeshRes->UpdateVirtexBuffer(m_NumberVertexVec[Num]);

		auto PtrTransform = GetComponent<Transform>();
		auto PtrPos = PtrTransform->GetPosition();
		if (itemcount_ComeOut) {
			if (!spritestop) {
				PtrTransform->SetPosition(PtrPos.x, PtrPos.y + MoveSpeed, PtrPos.z);
				MoveSpeed += 1;
				PtrPos = PtrTransform->GetPosition();
				if (PtrPos.y == Initial_Pos.y - 210) {
					spritestop = true;
				}

				PtrPos = PtrTransform->GetPosition();
				if (Initial_Pos == PtrPos) {
					itemcount_ComeOut = false;
					MoveSpeed = -20;
				}
			}
			else {
				count++;
				if (count >= 60) {
					count = 0;
					spritestop = false;
				}
			}
		}
	}

	void NumberSprite::setitemcount_ComeOut(bool b) {
		itemcount_ComeOut = b;
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(Initial_Pos);
	}

	//--------------------------------------------------------------------------------------
	//	class NumberSprite : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	SelectNumberSprite::SelectNumberSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const Vector3& StartScale) :
		GameObject(StagePtr), m_StartPos(StartPos), m_StartScale(StartScale), m_TotalTime(0) {
	}
	SelectNumberSprite::~SelectNumberSprite() {}

	//初期化
	void SelectNumberSprite::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(m_StartScale);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"SELECT_NUMBER_TX");
		//透明処理
		SetAlphaActive(true);
		//左上原点
		//スプライトの中のメッシュからバックアップの取得
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		//各数字ごとにUV値を含む頂点データを配列化しておく
		for (size_t i = 0; i < 10; i++) {

			float from = ((float)i) / 10.0f;
			float to = from + (1.0f / 10.0f);
			vector<VertexPositionColorTexture> NumVirtex =
			{
				//左上頂点
				VertexPositionColorTexture(
					SpVertexVec[0].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(from, 0)
				),
				//右上頂点
				VertexPositionColorTexture(
					SpVertexVec[1].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(to, 0)
				),
				//左下頂点
				VertexPositionColorTexture(
					SpVertexVec[2].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(from, 1.0f)
				),
				//右下頂点
				VertexPositionColorTexture(
					SpVertexVec[3].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(to, 1.0f)
				),
			};
			m_NumberVertexVec.push_back(NumVirtex);
		}
	}

	void SelectNumberSprite::OnUpdate() {
		////前回のターンからの時間
		size_t Num = (size_t)m_TotalTime;
		Num = Num % 10;
		auto PtrSprite = GetComponent<PCTSpriteDraw>();
		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値が変わる頂点を設定する
		MeshRes->UpdateVirtexBuffer(m_NumberVertexVec[Num]);


	}

}
//endof  basedx11