#pragma once

#include "stdafx.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	//	class FadeOut : public GameObject;
	//	用途: フェードアウト用
	//--------------------------------------------------------------------------------------
	class FadeOut : public GameObject {
		//リソースの作成
		void CreateResourses();

		//不透明度
		float tou = 0.0;

		//Fadeout(黒)を開始する変数
		bool FO_StartB = false;
		//Fadeout(白)を開始する変数
		bool FO_StartW = false;
		//Fadeout(黒)を開始する変数(License)
		bool FO_StartB_License = false;

		//Fadeout(セレクトに移動)を開始する変数
		bool FO_StartS = false;
		//Fadeout(ゲームステージに移動)を開始する変数
		bool FO_StartG = false;
		//Fadeout(タイトルに移動)を開始する変数
		bool FO_StartT = false;

		bool FO_StartI = false;
		bool FO_StartA = false;
        //単発用
		bool IsPushButton;

		bool once = false;
	public:
		//構築と破棄
		FadeOut(const shared_ptr<Stage>& StagePtr);
		virtual ~FadeOut() {}
		//初期化
		virtual void OnCreate() override;

		//更新
		virtual void OnUpdate() override;
		virtual void OnLastUpdate() override;

		void SetFOB_Start(bool b) {
			FO_StartB = b;
		};

		void SetFOW_Start(bool b) {
			FO_StartW = b;
		};

		void SetFOS_Start(bool b) {
			FO_StartS = b;
		};

		void SetFOG_Start(bool b) {
			FO_StartG = b;
		};
		void SetFOT_Start(bool b) {
			FO_StartT = b;
		};

		void SetFOB_License_Start(bool b) {
			FO_StartB_License = b;
		};

		void SetFOI_Start(bool b) {
			FO_StartI = b;
		};

		void SetFOA_Start(bool b) {
			FO_StartA = b;
		};
		
	};


	//--------------------------------------------------------------------------------------
	//	class FadeOut : public GameObject;
	//	用途: フェードアウト犬用
	//--------------------------------------------------------------------------------------
	class FadeOut2 : public GameObject {
		//リソースの作成
		void CreateResourses();

		//不透明度
		float tou = 0.0;

		//Fadeout(黒)を開始する変数
		bool FO_StartB = false;
		//Fadeout(白)を開始する変数
		bool FO_StartW = false;
		//Fadeout(黒)を開始する変数(License)
		bool FO_StartB_License = false;

		bool FO_StartD = false;

		bool once = false;
	public:
		//構築と破棄
		FadeOut2(const shared_ptr<Stage>& StagePtr);
		virtual ~FadeOut2() {}
		//初期化
		virtual void OnCreate() override;

		//更新
		virtual void OnUpdate() override;
		virtual void OnLastUpdate() override;

		void SetFOB_Start(bool b) {
			FO_StartB = b;
		};

		void SetFOW_Start(bool b) {
			FO_StartW = b;
		};
		void SetFOB_License_Start(bool b) {
			FO_StartB_License = b;
		};

		void SetFOD_Start(bool b) {
			FO_StartD = b;
		};
	};

	//--------------------------------------------------------------------------------------
	//	class FadeOut : public GameObject;
	//	用途: フェードイン用
	//--------------------------------------------------------------------------------------
	class FadeIn : public GameObject {
		//リソースの作成
		void CreateResourses();

		//不透明度
		float tou = 0.0;

		//Fadeout(黒)を開始する変数
		bool FO_StartB = false;
		//Fadeout(白)を開始する変数
		bool FO_StartW = false;
		//Fadeout(黒)を開始する変数(License)
		bool FO_StartB_License = false;

		bool FO_StartI = false;
	
		

		bool once = false;
	public:
		//構築と破棄
		FadeIn(const shared_ptr<Stage>& StagePtr);
		virtual ~FadeIn() {}
		//初期化
		virtual void OnCreate() override;

		//更新
		virtual void OnUpdate() override;
		virtual void OnLastUpdate() override;

		void SetFOB_Start(bool b) {
			FO_StartB = b;
		};

		void SetFOW_Start(bool b) {
			FO_StartW = b;
		};		

		void SetFOB_License_Start(bool b) {
			FO_StartB_License = b;
		};

		void SetFOI_Start(bool b) {
			FO_StartI = b;
		};

	};

	//--------------------------------------------------------------------------------------
	//	class FadeOut : public GameObject;
	//	用途: フェードイン(犬)用
	//--------------------------------------------------------------------------------------
	class FadeIn2 : public GameObject {
		//リソースの作成
		void CreateResourses();

		//不透明度
		float tou = 0.0;

		//Fadeout(黒)を開始する変数
		bool FO_StartB = false;
		//Fadeout(白)を開始する変数
		bool FO_StartW = false;
		//Fadeout(黒)を開始する変数(License)
		bool FO_StartB_License = false;

		bool FO_StartD = false;

		bool once = false;
	public:
		//構築と破棄
		FadeIn2(const shared_ptr<Stage>& StagePtr);
		virtual ~FadeIn2() {}
		//初期化
		virtual void OnCreate() override;

		//更新
		virtual void OnUpdate() override;
		virtual void OnLastUpdate() override;

		void SetFOB_Start(bool b) {
			FO_StartB = b;
		};

		void SetFOW_Start(bool b) {
			FO_StartW = b;
		};

		void SetFOB_License_Start(bool b) {
			FO_StartB_License = b;
		};

		void SetFOD_Start(bool b) {
			FO_StartD = b;
		};
	};

	//--------------------------------------------------------------------------------------
	//	class FadeOut : public GameObject;
	//	用途: フェードイン(提供)用
	//--------------------------------------------------------------------------------------
	class FadeIn3 : public GameObject {
		//リソースの作成
		void CreateResourses();

		//不透明度
		float tou = 1.0;

		//Fadeout(黒)を開始する変数
		bool FO_StartB = false;
		//Fadeout(白)を開始する変数
		bool FO_StartW = false;
		//Fadeout(黒)を開始する変数(License)
		bool FO_StartB_License = false;

		bool FO_StartT = false;

		bool once = false;
	public:
		//構築と破棄
		FadeIn3(const shared_ptr<Stage>& StagePtr);
		virtual ~FadeIn3() {}
		//初期化
		virtual void OnCreate() override;

		//更新
		virtual void OnUpdate() override;
		virtual void OnLastUpdate() override;

		void SetFOB_Start(bool b) {
			FO_StartB = b;
		};

		void SetFOW_Start(bool b) {
			FO_StartW = b;
		};

		void SetFOB_License_Start(bool b) {
			FO_StartB_License = b;
		};

		void SetFOT_Start(bool b) {
			FO_StartT = b;
		};

		float Getinvisible() {
			return tou;
		};
		void SetinvisibleZero() {
			tou = 0;
		};
	};
}
//endof  basedx11